/**
 * Control interfaces
 *
 * Authors:
 *   Nat Friedman <nat@nat.org>
 *   Miguel de Icaza <miguel@gnu.org>
 */

module GNOME {

interface ControlFrame : GNOME::Unknown {

	/**
	 * activated:
	 * @state: TRUE if the associated Control has been activated,
	 * FALSE if it just became inactive.
	 */
	void activated (in boolean state);

	/**
	 * get_ambient_properties:
	 * 
	 * Returns: A PropertyBag containing the ambient properties
	 * for this container.
	 */
	PropertyBag get_ambient_properties ();

	/** 
	 * queue_resize:
	 *
	 * Tell the container that the Control would like to be
	 * resized.  It should follow-up by calling
	 * Control:size_request()
	 *
	 */
	void queue_resize ();

	/**
	 * activate_uri:
	 * @uri: The uri we would like to get loaded.
	 *
	 * This is used by the containee when it needs
	 * to ask the container to perform some action
	 * with a given URI.
	 *
	 * The user has requested that the uri be loaded
	 */
	void activate_uri (in string uri, in boolean relative);

	/**
	 * get_ui_handler:
	 *
	 * Returns: The GNOME::UIHandler interface to be used for
	 * menu/toolbar item merging.
	 */
	UIHandler get_ui_handler ();

	/**
	 * deactivate_and_undo:
	 *
	 * This is called by the active object if it has just been activated
	 * and the undo operation has been called
	 */
	void deactivate_and_undo ();

};

interface Control : GNOME::Unknown {
	typedef string windowid;

	/**
	 * activate:
	 *
	 * Activates or deactivates this Control.
	 */
	void activate (in boolean activate);

	/**
	 * reactivate_and_undo:
	 * 
	 * Activates the object and undoes the last operation.
	 */
	void reactivate_and_undo ();

	/**
	 * set_frame:
	 * @frame: A GNOME_ControlFrame.
	 *
	 * Gives the Control a handle to its ControlFrame.
	 */
	void set_frame (in ControlFrame frame);

	/**
	 * set_window:
	 * @id: An X Window ID.
	 *
	 * Sets the window ID that should be used for the plug
	 * of this window.
	 */
	void set_window (in windowid id);

	/**
	 * get_property_bag:
	 *
	 * Returns: A PropertyBag containing this Control's properties.
	 */
	PropertyBag get_property_bag ();

	/**
	 * size_allocate:
	 * @width: width given to the control
	 * @height: height given to the control
	 *
	 * Informs the Control of the size assigned by its container application
	 */
	void size_allocate (in short width, in short height);

	/**
	 * size_request:
	 * @desired_width: In this value you should return the desired width you want.
	 * @desired_height: In this value you should return the desired height you want.
	 *
	 */
	void size_request (out short desired_width, out short desired_height);

};

};
