/*
 * gnome-stream-efs.c: libefs based Stream implementation
 *
 * Author:
 *   Dietmar Maurer (dm@vlsivie.tuwien.ac.at)
 *
 */

#include <config.h>
#include <storage-modules/gnome-stream-efs.h>

static GnomeStream *
create_stream_efs_server (const GnomeStreamEFS *stream_efs)
{
	GnomeObject *object = GNOME_OBJECT(stream_efs);
	POA_GNOME_Stream *servant;
	GNOME_Stream corba_stream;
	CORBA_Environment ev;

	servant = (POA_GNOME_Stream *) g_new0 (GnomeObjectServant, 1);
	servant->vepv = &gnome_stream_vepv;

	CORBA_exception_init (&ev);
	POA_GNOME_Stream__init ((PortableServer_Servant) servant, &ev);
	if (ev._major != CORBA_NO_EXCEPTION){
		g_free (servant);
		CORBA_exception_free (&ev);
		return NULL;
	}
	CORBA_exception_free (&ev);

	corba_stream = gnome_object_activate_servant(object, servant);
	return GNOME_STREAM(gnome_object_construct(GNOME_OBJECT(stream_efs), 
						   corba_stream));
}

static void
gnome_stream_efs_destroy (GtkObject *object)
{
	GnomeStreamEFS *stream_efs = GNOME_STREAM_EFS (object);

	if (stream_efs->file) efs_file_close (stream_efs->file);
	if (stream_efs->storage) 
		gtk_object_unref (GTK_OBJECT (stream_efs->storage));    
}

static CORBA_long
real_write (GnomeStream *stream, const GNOME_Stream_iobuf *buffer,
	    CORBA_Environment *ev)
{
	GnomeStreamEFS *stream_efs = GNOME_STREAM_EFS (stream);

	if (efs_file_write(stream_efs->file, buffer->_buffer, 
			   buffer->_length) == -1) return 0;

	return buffer->_length;
}

static CORBA_long
real_read (GnomeStream *stream, CORBA_long count,
	   GNOME_Stream_iobuf ** buffer,
	   CORBA_Environment *ev)
{
	GnomeStreamEFS *stream_efs = GNOME_STREAM_EFS (stream);
	CORBA_octet *data;
	CORBA_long bytes;

	*buffer = GNOME_Stream_iobuf__alloc ();
	CORBA_sequence_set_release (*buffer, TRUE);
	data = CORBA_sequence_CORBA_octet_allocbuf (count);

	bytes = efs_file_read(stream_efs->file, data, count);

	(*buffer)->_buffer = data;
	(*buffer)->_length = bytes;

	return bytes;
}

static CORBA_long
real_seek (GnomeStream *stream, CORBA_long offset, GNOME_Stream_SeekType whence,
	   CORBA_Environment *ev)
{
	GnomeStreamEFS *stream_efs = GNOME_STREAM_EFS (stream);
	int fw;

	if (whence == GNOME_Stream_SEEK_CUR)
		fw = SEEK_CUR;
	else if (whence == GNOME_Stream_SEEK_END)
		fw = SEEK_END;
	else
		fw = SEEK_SET;

	
	return efs_file_seek (stream_efs->file, offset, fw);
}

static void
real_truncate (GnomeStream *stream, const CORBA_long new_size, 
	       CORBA_Environment *ev)
{
	GnomeStreamEFS *stream_efs = GNOME_STREAM_EFS (stream);

	if (efs_file_trunc (stream_efs->file, new_size)) {
		g_warning ("Signal exception!");
	}
}

static void
gnome_stream_efs_class_init (GnomeStreamEFSClass *class)
{
	GtkObjectClass *object_class = (GtkObjectClass *) class;
	GnomeStreamClass *sclass = GNOME_STREAM_CLASS (class);
	
	sclass->write    = real_write;
	sclass->read     = real_read;
	sclass->seek     = real_seek;
	sclass->truncate = real_truncate;

	object_class->destroy = gnome_stream_efs_destroy;
}

GtkType
gnome_stream_efs_get_type (void)
{
	static GtkType type = 0;

	if (!type){
		GtkTypeInfo info = {
			"IDL:GNOME/StreamEFS:1.0",
			sizeof (GnomeStreamEFS),
			sizeof (GnomeStreamEFSClass),
			(GtkClassInitFunc) gnome_stream_efs_class_init,
			(GtkObjectInitFunc) NULL,
			NULL, /* reserved 1 */
			NULL, /* reserved 2 */
			(GtkClassInitFunc) NULL
		};

		type = gtk_type_unique (gnome_stream_get_type (), &info);
	}
  
	return type;
}

static GnomeStream *
gnome_stream_efs_open_create (GnomeStorageEFS *storage_efs,
			      const CORBA_char *path,
			      GNOME_Storage_OpenMode mode,
			      gboolean create)
{
	GnomeStreamEFS *stream_efs;
	gint flags;

	g_return_val_if_fail(storage_efs != NULL, NULL);
	g_return_val_if_fail (path != NULL, NULL);

	if (!(stream_efs = gtk_type_new (gnome_stream_efs_get_type ()))) 
		return NULL;

	flags = EFS_READ;
	if (mode&GNOME_Storage_WRITE) flags |= EFS_WRITE;
	if (create) flags |= EFS_CREATE;
  
	if (!(stream_efs->file=efs_file_open(storage_efs->dir, path, flags))) {
		gtk_object_destroy (GTK_OBJECT (stream_efs));
		return NULL;
	}
	
	stream_efs->storage = storage_efs;
	gtk_object_ref(GTK_OBJECT(storage_efs));
	create_stream_efs_server(stream_efs);

	return GNOME_STREAM (stream_efs);
}

GnomeStream *
gnome_stream_efs_open (GnomeStorageEFS *storage, const CORBA_char *path, 
			GNOME_Storage_OpenMode mode)
{
	return gnome_stream_efs_open_create(storage, path, mode, FALSE);
}

GnomeStream *
gnome_stream_efs_create (GnomeStorageEFS *storage, const CORBA_char *path)
{
	return gnome_stream_efs_open_create(storage, path, 0, TRUE);
}



