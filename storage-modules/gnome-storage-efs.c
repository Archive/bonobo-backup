/*
 * gnome-storage-efs.c: libefs based Storage implementation
 *
 * Author:
 *   Dietmar Maurer (dm@vlsivie.tuwien.ac.at)
 *
 *
 * Problems:
 *
 * do we really need open and create functions? maybe we can use open and
 * a special flag O_CREATE (like in stdio)
 *
 * storage_copy_to: ??? 
 *      idl: copy_to (in string path, in Storage dest, is string new_path)
 *
 * list_contents: bad interface! redesign necessary?
 *
 */

#include <config.h>
#include <efs.h>
#include <storage-modules/gnome-storage-efs.h>
#include <storage-modules/gnome-stream-efs.h>

/*
 * Creates and activates the corba server 
 */
static GnomeStorage *
create_storage_efs_server (const GnomeStorageEFS *storage_efs)
{
	GnomeObject *object = GNOME_OBJECT(storage_efs);
	POA_GNOME_Storage *servant;
	GNOME_Storage corba_storage;
	CORBA_Environment ev;

	servant = (POA_GNOME_Storage *) g_new0 (GnomeObjectServant, 1);
	servant->vepv = &gnome_storage_vepv;

	CORBA_exception_init (&ev);
	POA_GNOME_Storage__init ((PortableServer_Servant) servant, 
				 &ev);
	if (ev._major != CORBA_NO_EXCEPTION){
		g_free (servant);
		CORBA_exception_free (&ev);
		return NULL;
	}
	CORBA_exception_free (&ev);

	corba_storage = gnome_object_activate_servant(object, servant);
	return gnome_storage_construct(GNOME_STORAGE(storage_efs), 
				       corba_storage);
}

static void
gnome_storage_efs_destroy (GtkObject *object)
{
	GnomeStorageEFS *storage_efs = GNOME_STORAGE_EFS (object);

	
	if (storage_efs->owner) {
		if (storage_efs->dir) efs_dir_close(storage_efs->dir);
		gtk_object_unref (GTK_OBJECT (storage_efs->owner));    
	} else {
		if (storage_efs->dir) efs_close (storage_efs->dir);
	}
}


static GnomeStream *
real_create_stream (GnomeStorage *storage, const CORBA_char *path, 
		    CORBA_Environment *ev)
{
	GnomeStorageEFS *storage_efs = GNOME_STORAGE_EFS (storage);

	return gnome_stream_efs_create (storage_efs, path);
}

static GnomeStream *
real_open_stream (GnomeStorage *storage, const CORBA_char *path, 
		  GNOME_Storage_OpenMode mode, CORBA_Environment *ev)
{
	GnomeStorageEFS *storage_efs = GNOME_STORAGE_EFS (storage);
	
	return gnome_stream_efs_open (storage_efs, path, mode);
}


static GnomeStorage *
real_create_storage (GnomeStorage *storage, const CORBA_char *path, 
		     CORBA_Environment *ev)
{
	GnomeStorageEFS *storage_efs = GNOME_STORAGE_EFS (storage);
	GnomeStorageEFS *sefs;
	EFSDir *dir;

	if (!(dir=efs_dir_open(storage_efs->dir, path, EFS_CREATE|EFS_EXCL))) {
		CORBA_exception_set(ev, CORBA_USER_EXCEPTION, 
				    ex_GNOME_Storage_NameExists, NULL);
		return NULL;
	}
	
	sefs = gtk_type_new (gnome_storage_efs_get_type ());
	sefs->dir = dir;
	sefs->owner = storage;
	gtk_object_ref(GTK_OBJECT(storage));
	create_storage_efs_server(sefs);

	return GNOME_STORAGE(sefs);
}

static GnomeStorage *
real_open_storage (GnomeStorage *storage, const CORBA_char *path, 
		   CORBA_Environment *ev)
{
	GnomeStorageEFS *storage_efs = GNOME_STORAGE_EFS (storage);
	GnomeStorageEFS *sefs;
	EFSDir *dir;

	if (!(dir=efs_dir_open(storage_efs->dir, path, 0))) {
		CORBA_exception_set(ev, CORBA_USER_EXCEPTION, 
				    ex_GNOME_Storage_NotFound, NULL);
		return NULL;
	}

	sefs = gtk_type_new (gnome_storage_efs_get_type ());
	sefs->dir = dir;
	sefs->owner = storage;
	gtk_object_ref(GTK_OBJECT(storage));
	create_storage_efs_server(sefs);

	return GNOME_STORAGE(sefs);
}

static void
real_copy_to (GnomeStorage *storage, GNOME_Storage target, 
	      CORBA_Environment *ev)
{	
	g_warning ("Not yet implemented");
}

static void
real_rename (GnomeStorage *storage, const CORBA_char *path, 
	     const CORBA_char *new_path, CORBA_Environment *ev)
{
	GnomeStorageEFS *storage_efs = GNOME_STORAGE_EFS (storage);

	if (!efs_rename(storage_efs->dir, path, new_path)) {
		CORBA_exception_set(ev, CORBA_USER_EXCEPTION, 
				    ex_GNOME_Storage_NotFound, NULL);
	}
}

static void
real_commit (GnomeStorage *storage, CORBA_Environment *ev)
{
	GnomeStorageEFS *storage_efs = GNOME_STORAGE_EFS (storage);

	if (storage_efs->owner) return;

	efs_commit(storage_efs->dir);
}


static GNOME_Storage_directory_list *
real_list_contents (GnomeStorage *storage, const CORBA_char *path, 
		    CORBA_Environment *ev)
{
	GnomeStorageEFS *storage_efs = GNOME_STORAGE_EFS (storage);
	GNOME_Storage_directory_list *list;
	EFSDirEntry *de;
	EFSDir *dir;
	int i;
	char **buf = NULL;
	char *n1;

	list =  GNOME_Storage_directory_list__alloc();
	CORBA_sequence_set_release (list, TRUE); 
	buf = CORBA_sequence_CORBA_string_allocbuf(100); /*fixme: max 100*/

	if ((dir = efs_dir_open(storage_efs->dir, path, 0))) {
		i = 0;
		while ((de = efs_dir_read(dir))) {
			n1 = CORBA_string_alloc(strlen(de->name));
			strcpy(n1,de->name);
			buf[i]=n1;
			i++;
		}
		list->_length = i;
		list->_buffer = buf;
		efs_dir_close (dir);
		return list;
	} else {
		CORBA_exception_set(ev, CORBA_USER_EXCEPTION, 
				    ex_GNOME_Storage_NotFound, NULL);
		return NULL;
	}
}


static void
real_erase (GnomeStorage *storage, const CORBA_char *path,
	    CORBA_Environment *ev)
{
	GnomeStorageEFS *storage_efs = GNOME_STORAGE_EFS (storage);

	if (!efs_erase (storage_efs->dir, path)) {
		CORBA_exception_set(ev, CORBA_USER_EXCEPTION, 
				    ex_GNOME_Storage_NotFound, NULL);
	}
}

static void
gnome_storage_efs_class_init (GnomeStorageEFSClass *class)
{
	GtkObjectClass *object_class = (GtkObjectClass *) class;
	GnomeStorageClass *sclass = GNOME_STORAGE_CLASS (class);

	sclass->create_stream  = real_create_stream;
	sclass->open_stream    = real_open_stream;

	sclass->create_storage = real_create_storage;
	sclass->open_storage   = real_open_storage;
	sclass->copy_to        = real_copy_to;
	sclass->rename         = real_rename;
	sclass->commit         = real_commit;
	sclass->list_contents  = real_list_contents;
	sclass->erase          = real_erase;

	object_class->destroy = gnome_storage_efs_destroy;
}

GtkType
gnome_storage_efs_get_type (void)
{
	static GtkType type = 0;
  
	if (!type){
		GtkTypeInfo info = {
			"IDL:GNOME/StorageEFS:1.0",
			sizeof (GnomeStorageEFS),
			sizeof (GnomeStorageEFSClass),
			(GtkClassInitFunc) gnome_storage_efs_class_init,
			(GtkObjectInitFunc) NULL,
			NULL, /* reserved 1 */
			NULL, /* reserved 2 */
			(GtkClassInitFunc) NULL
		};

		type = gtk_type_unique (gnome_storage_get_type (), &info);
	}

	return type;
}

/** 
 * gnome_storage_efs_open:
 * @path: path to a file that represents the storage
 * @flags: flags
 * @mode: mode
 *
 * Opens a GnomeStorage object in @path. The storage opened is an activated
 * CORBA server for this storage.
 *
 * Returns the GnomeStorage GTK object.
 */
GnomeStorage *
gnome_storage_efs_open (const gchar *path, gint flags, gint mode)
{
	GnomeStorageEFS *sefs;

	sefs = gtk_type_new (gnome_storage_efs_get_type ());

	if (!(sefs->dir = efs_open(path, flags, mode))) {
		gnome_object_destroy (GNOME_OBJECT (sefs));
		return NULL;
	}
    
	if (!create_storage_efs_server(sefs)) {
		gnome_object_destroy (GNOME_OBJECT (sefs));
		return NULL;
	}

	return GNOME_STORAGE(sefs);
}

/*
 * Shared library entry point
 */
GnomeStorage *
gnome_storage_driver_open (const gchar *path, gint flags, gint mode)
{
	gint efs_flags = 0;

	if (flags & GNOME_SS_READ)
		efs_flags |= EFS_READ;
	if (flags & GNOME_SS_WRITE)
		efs_flags |= EFS_WRITE;
	if (flags & GNOME_SS_RDWR)
		efs_flags |= EFS_RDWR;
	if (flags & GNOME_SS_CREATE)
		efs_flags |= EFS_CREATE;
	if (flags & GNOME_SS_EXCL)
		efs_flags |= EFS_EXCL;
	
	return gnome_storage_efs_open (path, efs_flags, mode);
}

