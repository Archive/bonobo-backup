#ifndef _GNOME_STREAM_EFS_H_
#define _GNOME_STREAM_EFS_H_

#include <bonobo/gnome-stream.h>
#include <storage-modules/gnome-storage-efs.h>

BEGIN_GNOME_DECLS

#define GNOME_STREAM_EFS_TYPE        (gnome_stream_efs_get_type ())
#define GNOME_STREAM_EFS(o)          (GTK_CHECK_CAST ((o), GNOME_STREAM_EFS_TYPE, GnomeStreamEFS))
#define GNOME_STREAM_EFS_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), GNOME_STREAM_EFS_TYPE, GnomeStreamEFSClass))
#define GNOME_IS_STREAM_EFS(o)       (GTK_CHECK_TYPE ((o), GNOME_STREAM_EFS_TYPE))
#define GNOME_IS_STREAM_EFS_CLASS(k) (GTK_CHECK_CLASS_TYPE ((k), GNOME_STREAM_EFS_TYPE))

typedef struct {
	GnomeStream stream;
	GnomeStorageEFS *storage;
	EFSFile *file;
} GnomeStreamEFS;

typedef struct {
	GnomeStreamClass parent_class;
} GnomeStreamEFSClass;

GtkType         gnome_stream_efs_get_type     (void);
GnomeStream    *gnome_stream_efs_open         (GnomeStorageEFS *storage,
					       const CORBA_char *path, 
					       GNOME_Storage_OpenMode mode);
GnomeStream    *gnome_stream_efs_create       (GnomeStorageEFS *storage,
					       const CORBA_char *path);
	
END_GNOME_DECLS

#endif /* _GNOME_STREAM_EFS_H_ */
