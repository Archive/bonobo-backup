#ifndef _GNOME_STORAGE_EFS_H_
#define _GNOME_STORAGE_EFS_H_

#include <bonobo/gnome-storage.h>
#include <efs.h>

BEGIN_GNOME_DECLS

#define GNOME_STORAGE_EFS_TYPE        (gnome_storage_efs_get_type ())
#define GNOME_STORAGE_EFS(o)          (GTK_CHECK_CAST ((o), GNOME_STORAGE_EFS_TYPE, GnomeStorageEFS))
#define GNOME_STORAGE_EFS_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), GNOME_STORAGE_EFS_TYPE, GnomeStorageEFSClass))
#define GNOME_IS_STORAGE_EFS(o)       (GTK_CHECK_TYPE ((o), GNOME_STORAGE_EFS_TYPE))
#define GNOME_IS_STORAGE_EFS_CLASS(k) (GTK_CHECK_CLASS_TYPE ((k), GNOME_STORAGE_EFS_TYPE))

typedef struct {
        GnomeStorage storage;
	GnomeStorage *owner;
	EFSDir *dir;
} GnomeStorageEFS;

typedef struct {
	GnomeStorageClass parent_class;
} GnomeStorageEFSClass;

GtkType       gnome_storage_efs_get_type  (void);

GnomeStorage *gnome_storage_efs_open      (const gchar *path, gint flags, 
					   gint mode);


END_GNOME_DECLS

#endif /* _GNOME_STORAGE_EFS_H_ */
