#ifndef __BONOBO_CLOCK_CONTROL_H__
#define __BONOBO_CLOCK_CONTROL_H__

#include <bonobo/gnome-control.h>

void             bonobo_clock_factory_init (void);

#endif /* __BONOBO_CLOCK_CONTROL_H__ */
