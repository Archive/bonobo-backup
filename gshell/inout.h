/*
 * Author:
 *   Dietmar Maurer (dietmar@maurer-it.com)
 *
 * Copyright 1999 Maurer IT Systemlösungen (http://www.maurer-it.com)
 */

#ifndef _GSHELL_INOUT_H_
#define _GSHELL_INOUT_H_

#include "gshell.h"

void  file_menu_open_cb    (GnomeUIHandler *uih, void *data, char *path);
void  file_menu_save_as_cb (GnomeUIHandler *uih, void *data, char *path);
void  file_menu_save_cb    (GnomeUIHandler *uih, void *data, char *path);

#endif
