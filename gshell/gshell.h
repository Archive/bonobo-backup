/*
 * Author:
 *   Dietmar Maurer (dietmar@maurer-it.com)
 *
 * Copyright 1999 Maurer IT Systemlösungen (http://www.maurer-it.com)
 */

#ifndef _GSHELL_H_
#define _GSHELL_H_

#include <config.h>
#include <gnome.h>
#include <bonobo/gnome-bonobo.h>

typedef struct {
	GnomeUIHandler  *uih;
	GtkWidget	*app;
	GtkWidget	*vbox;
	GnomeViewFrame *active_view_frame;
	GList           *view_list;
} Frame;

typedef struct {
	GnomeClientSite   *client_site;
	GnomeObjectClient *server;
	gchar *name;
	gint menu_id;
} Buffer;

typedef struct {
	GnomeContainer  *container;
	GList *frame_list;
	GList *buffer_list;
	GtkFileSelection *fs;
} Application;

extern Application app;

void file_menu_kill_cb          (GnomeUIHandler *uih, void *data, char *path);
void view_menu_new_window_cb    (GnomeUIHandler *uih, void *data, char *path);
void view_menu_delete_window_cb (GnomeUIHandler *uih, void *data, char *path);
void view_menu_split_window_cb  (GnomeUIHandler *uih, void *data, char *path);
void view_menu_one_window_cb    (GnomeUIHandler *uih, void *data, char *path);
void file_menu_exit_cb          (GnomeUIHandler *uih, void *data, char *path);
void edit_menu_preferences_cb   (GnomeUIHandler *uih, void *data, char *path);
void help_menu_about_cb         (GnomeUIHandler *uih, void *data, char *path);
void help_menu_help_gnome_cb    (GnomeUIHandler *uih, void *data, char *path);

Frame          *get_active_frame           (GtkWidget *widget);
GnomeViewFrame *get_active_view_frame      (Frame *frame);
Buffer         *buffer_create              (const char *component_goad_id);
gint            view_remove                (Frame *frame, 
					    GnomeViewFrame *view_frame);
void            buffer_add_view            (Buffer *buffer, 
					    Frame *frame, 
					    gint pos);
gboolean        gnome_object_has_interface (GnomeObject *obj, 
					    char *interface);
void            set_buffer_cb              (GnomeUIHandler *uih, 
					    void *buffer,  
					    const char *path);



#endif
