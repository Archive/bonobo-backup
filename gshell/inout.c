/*
 * input/output routines
 *
 * based on sample-container from  Nat Friedman (nat@nat.org)
 *
 * Author:
 *   Dietmar Maurer (dietmar@maurer-it.com)
 *
 * Copyright 1999 Maurer IT Systemlösungen (http://www.maurer-it.com)
 */

#include "inout.h"
#include "menu.h"

static void
buffer_save_pf (Buffer *buffer, gchar *filename)
{
	GNOME_PersistFile persist;
	CORBA_Environment ev;

	g_return_if_fail (buffer != NULL);
	g_return_if_fail (filename != NULL);
	
	CORBA_exception_init (&ev);

	persist = GNOME_Unknown_query_interface (
	        gnome_object_corba_objref (GNOME_OBJECT (buffer->server)),
		"IDL:GNOME/PersistFile:1.0",
		&ev);
	
	if (ev._major != CORBA_NO_EXCEPTION || persist == CORBA_OBJECT_NIL) {
		g_warning ("component doesn't support PersistFile!");
		CORBA_exception_free (&ev);
		return;
	}

	/* fixme: only save when dirty - this is currently not 
	   supported by most components */
	/*
	if (!GNOME_PersistFile_is_dirty (persist, &ev)) {
	        g_warning ("No changes need to be saved");
		GNOME_Unknown_unref (persist, &ev);
		CORBA_exception_free (&ev);
		return;
	}
	*/

	GNOME_PersistFile_save (persist, (CORBA_char *) filename, &ev);

	if (ev._major != CORBA_NO_EXCEPTION) {
		gnome_warning_dialog (_("An exception occured while trying "
					"to save data from the component with "
					"PersistFile"));
	}
	if (ev._major != CORBA_SYSTEM_EXCEPTION)
		CORBA_Object_release (persist, &ev);

	GNOME_Unknown_unref (persist, &ev);
	CORBA_exception_free (&ev);
}

static void
buffer_load_pf (Buffer *buffer, gchar *filename)
{
	GNOME_PersistFile persist;
	CORBA_Environment ev;

	g_return_if_fail (buffer != NULL);
	g_return_if_fail (filename != NULL);
	
	CORBA_exception_init (&ev);

	persist = GNOME_Unknown_query_interface (
	        gnome_object_corba_objref (GNOME_OBJECT (buffer->server)),
		"IDL:GNOME/PersistFile:1.0",
		&ev);

	if (ev._major != CORBA_NO_EXCEPTION || persist == CORBA_OBJECT_NIL) {
		g_warning ("component doesn't support PersistFile!");
		CORBA_exception_free (&ev);
		return;
	}

	GNOME_PersistFile_load (persist, (CORBA_char *) filename, &ev);

	if (ev._major != CORBA_NO_EXCEPTION) {
		gnome_warning_dialog (_("An exception occured while trying "
					"to load data into the component with "
					"PersistFile"));
	}
	if (ev._major != CORBA_SYSTEM_EXCEPTION)
		CORBA_Object_release (persist, &ev);

	GNOME_Unknown_unref (persist, &ev);	
	CORBA_exception_free (&ev);
}

static void
buffer_save_ps (Buffer *buffer, gchar *filename)
{
	GNOME_PersistStream persist;
	CORBA_Environment ev;
	GnomeStream *stream;

	g_return_if_fail (buffer != NULL);
	g_return_if_fail (filename != NULL);
	
	if (!(stream = gnome_stream_fs_open (filename, GNOME_Storage_WRITE))) {
		char *error_msg;
		error_msg = g_strdup_printf (_("Could not save file %s"), 
					     filename);
		gnome_warning_dialog (error_msg);
		g_free (error_msg);
		return;
	}

	CORBA_exception_init (&ev);

	persist = GNOME_Unknown_query_interface (
	        gnome_object_corba_objref (GNOME_OBJECT (buffer->server)),
		"IDL:GNOME/PersistStream:1.0",
		&ev);
	
	if (ev._major != CORBA_NO_EXCEPTION || persist == CORBA_OBJECT_NIL) {
		g_warning ("component doesn't support PersistStream!");
		CORBA_exception_free (&ev);
		return;
	}

	/* fixme: only save when dirty - this is currently not 
	   supported by most components */
	/*
	if (!GNOME_PersistStream_is_dirty (persist, &ev)) {
		g_warning ("No changes need to be saved");
		GNOME_Unknown_unref (persist, &ev);
		CORBA_exception_free (&ev);
		return;
	}
	*/

	GNOME_PersistStream_save (persist,
	     (GNOME_Stream) gnome_object_corba_objref (GNOME_OBJECT (stream)),
				  &ev);

	if (ev._major != CORBA_NO_EXCEPTION) {
		gnome_warning_dialog (_("An exception occured while trying "
					"to save data from the component with "
					"PersistStream"));
	}
	if (ev._major != CORBA_SYSTEM_EXCEPTION)
		CORBA_Object_release (persist, &ev);

	GNOME_Unknown_unref (persist, &ev);
	CORBA_exception_free (&ev);
	gnome_object_unref (GNOME_OBJECT(stream));
}

static void
buffer_load_ps (Buffer *buffer, gchar *filename)
{
	GNOME_PersistStream persist;
	CORBA_Environment ev;
	GnomeStream *stream;

	g_return_if_fail (buffer != NULL);
	g_return_if_fail (filename != NULL);
	
	if (!(stream = gnome_stream_fs_open (filename, GNOME_Storage_READ))) {
		char *error_msg;
		error_msg = g_strdup_printf (_("Could not open file %s"), 
					     filename);
		gnome_warning_dialog (error_msg);
		g_free (error_msg);
		return;
	}

	CORBA_exception_init (&ev);

	persist = GNOME_Unknown_query_interface (
	        gnome_object_corba_objref (GNOME_OBJECT (buffer->server)),
		"IDL:GNOME/PersistStream:1.0",
		&ev);

	if (ev._major != CORBA_NO_EXCEPTION || persist == CORBA_OBJECT_NIL) {
		g_warning ("component doesn't support PersistStream!");
		CORBA_exception_free (&ev);
		return;
	}

	GNOME_PersistStream_load (persist,
	   (GNOME_Stream) gnome_object_corba_objref (GNOME_OBJECT (stream)),
				  &ev);

	if (ev._major != CORBA_NO_EXCEPTION) {
		gnome_warning_dialog (_("An exception occured while trying "
					"to load data into the component with "
					"PersistStream"));
	}
	if (ev._major != CORBA_SYSTEM_EXCEPTION)
		CORBA_Object_release (persist, &ev);

	GNOME_Unknown_unref (persist, &ev);
	CORBA_exception_free (&ev);
	gnome_object_unref (GNOME_OBJECT(stream));
}

static void
file_save_as_cb (GtkWidget *widget, GnomeUIHandler *uih)
{
	Buffer *buffer;
	Frame *frame;
	GnomeViewFrame *view_frame;
	gchar *name;

	g_return_if_fail(uih != NULL);
	g_return_if_fail(GNOME_IS_UI_HANDLER(uih));

	name = strdup(gtk_file_selection_get_filename(app.fs));
	gtk_widget_destroy (GTK_WIDGET(app.fs));

	if (!(frame = get_active_frame (GTK_WIDGET(uih->top->app)))) {
		g_warning ("unable to get active frame");
		g_free (name);
		return;
	}

	if (!(view_frame = get_active_view_frame (frame))) {
		g_warning ("unable to get active view frame");
		g_free (name);
		return;
	}

	if (!(buffer = gtk_object_get_data(GTK_OBJECT(view_frame),"buffer"))) {
		g_warning ("unable to get active buffer");
		g_free (name);
		return;
	}

	if (gnome_object_has_interface(GNOME_OBJECT (buffer->server), 
				       "IDL:GNOME/PersistFile:1.0")) {
		buffer_save_pf (buffer, name);
	} else if (gnome_object_has_interface(GNOME_OBJECT (buffer->server), 
					      "IDL:GNOME/PersistStream:1.0")) {
		buffer_save_ps (buffer, name);
	} else {
		g_warning ("no storage interface found");
	}

	g_free (name);
}

static void
file_open_cb (GtkWidget *widget, GnomeUIHandler *uih)
{
	gchar *name;
	const gchar *mime;
	const gchar *bid;
	static gint menu_id = 0;
	Frame *frame;
	Buffer *buffer;
	GnomeViewFrame *view_frame;
	gint pos = -1;

	g_return_if_fail(uih != NULL);
	g_return_if_fail(GNOME_IS_UI_HANDLER(uih));

	if (!(frame = get_active_frame (GTK_WIDGET(uih->top->app)))) {
		g_warning ("unable to get active frame");
		return;
	}
	
	name = strdup(gtk_file_selection_get_filename(app.fs));
	gtk_widget_destroy (GTK_WIDGET(app.fs));
	app.fs = NULL;
	if (!name) return;

	if (!g_file_test(name, G_FILE_TEST_ISFILE)) return;
	
	if (!(mime = gnome_mime_type_of_file (name))) {
		g_warning ("unable to detect mime type");
		return;
	}

	if (!(bid = gnome_mime_get_value (mime, "bonobo-goad-id"))) {
		g_warning ("unable to detect the bonobo-goad-id");
		printf ("for mime type %s\n",mime);
		return;
	}

	if (!(buffer = buffer_create (bid))) {
		g_warning ("unable to create the bonobo component");
		return;
	}
	
	buffer->name = name;
	buffer->menu_id = menu_id++;

	if ((view_frame = get_active_view_frame (frame))) {
		pos = view_remove (frame, view_frame);
	}

	if (gnome_object_has_interface(GNOME_OBJECT (buffer->server), 
				       "IDL:GNOME/PersistFile:1.0")) {
		buffer_load_pf (buffer, name);
	} else if (gnome_object_has_interface(GNOME_OBJECT (buffer->server), 
					      "IDL:GNOME/PersistStream:1.0")) {
		buffer_load_ps (buffer, name);
	} else {
		g_warning ("no storage interface found");
	}

	buffer_add_view (buffer, frame, pos);
	update_buffer_menu ();
}

void  
file_menu_open_cb (GnomeUIHandler *uih, void *data, char *path)
{
	g_return_if_fail(uih != NULL);
	g_return_if_fail(GNOME_IS_UI_HANDLER(uih));
	
	app.fs=GTK_FILE_SELECTION(gtk_file_selection_new(_("Open")));
	gtk_file_selection_hide_fileop_buttons(app.fs);
	gtk_signal_connect_object(GTK_OBJECT (app.fs->cancel_button), 
				  "clicked",
				  (GtkSignalFunc) gtk_widget_destroy,
				  (gpointer)app.fs);
	gtk_signal_connect(GTK_OBJECT(app.fs->ok_button), 
			   "clicked",
			   (GtkSignalFunc)file_open_cb,
			   (gpointer)uih);

	gtk_widget_show(GTK_WIDGET(app.fs));
	gtk_window_set_modal (GTK_WINDOW(app.fs), TRUE);
}

void 
file_menu_save_cb (GnomeUIHandler *uih, void *data, char *path)
{
	Buffer *buffer;
	Frame *frame;
	GnomeViewFrame *view_frame;

	g_return_if_fail(uih != NULL);
	g_return_if_fail(GNOME_IS_UI_HANDLER(uih));

	if (!(frame = get_active_frame (GTK_WIDGET(uih->top->app)))) {
		g_warning ("unable to get active frame");
		return;
	}

	if (!(view_frame = get_active_view_frame (frame))) {
		g_warning ("unable to get active view frame");
		return;
	}

	if (!(buffer = gtk_object_get_data(GTK_OBJECT(view_frame),"buffer"))) {
		g_warning ("unable to get active buffer");
		return;
	}

	if (gnome_object_has_interface(GNOME_OBJECT (buffer->server), 
				       "IDL:GNOME/PersistFile:1.0")) {
		buffer_save_pf (buffer, buffer->name);
	} else if (gnome_object_has_interface(GNOME_OBJECT (buffer->server), 
					      "IDL:GNOME/PersistStream:1.0")) {
		buffer_save_ps (buffer, buffer->name);
	} else {
		g_warning ("no storage interface found");
	}
}

void  
file_menu_save_as_cb (GnomeUIHandler *uih, void *data, char *path)
{
	Frame *frame;
	GnomeViewFrame *view_frame;

	g_return_if_fail(uih != NULL);
	g_return_if_fail(GNOME_IS_UI_HANDLER(uih));

	if (!(frame = get_active_frame (GTK_WIDGET(uih->top->app)))) return;
	if (!(view_frame = get_active_view_frame (frame))) return;
	
	app.fs=GTK_FILE_SELECTION(gtk_file_selection_new(_("Save As")));
	gtk_file_selection_hide_fileop_buttons(app.fs);
	gtk_signal_connect_object(GTK_OBJECT (app.fs->cancel_button), 
				  "clicked",
				  (GtkSignalFunc) gtk_widget_destroy,
				  (gpointer)app.fs);
	gtk_signal_connect(GTK_OBJECT(app.fs->ok_button), 
			   "clicked",
			   (GtkSignalFunc)file_save_as_cb,
			   (gpointer)uih);

	gtk_widget_show(GTK_WIDGET(app.fs));
	gtk_window_set_modal (GTK_WINDOW(app.fs), TRUE);
}


