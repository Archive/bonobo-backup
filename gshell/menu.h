/*
 * Author:
 *   Dietmar Maurer (dietmar@maurer-it.com)
 *
 * Copyright 1999 Maurer IT Systemlösungen (http://www.maurer-it.com)
 */

#ifndef _GSHELL_MENU_H_
#define _GSHELL_MENU_H_

#include "gshell.h"

void set_file_menu_state  (Frame *frame, gboolean state);
void set_edit_menu_state  (Frame *frame, gboolean state);
void frame_create_menus   (Frame *frame);
void frame_create_toolbar (Frame *frame);
void update_buffer_menu   (void);

#endif
