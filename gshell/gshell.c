/*
 * This program is a bonobo container whith an emacs like user interface
 * (based on sample-container from  Nat Friedman (nat@nat.org))
 *
 * Author:
 *   Dietmar Maurer (dietmar@maurer-it.com)
 *
 * Copyright 1999 Maurer IT Systemlösungen (http://www.maurer-it.com)
 */

#include "gshell.h"
#include "inout.h"
#include "menu.h"

Application app;

static void            frame_close_cb        (GtkWidget *widget);

void 
help_menu_help_gnome_cb (GnomeUIHandler *uih, void *data, char *path)
{
	gchar *p;
	p = gnome_help_file_path ("help-browser", "default-page.html");
	if (p) gnome_help_goto (NULL, p);
}

void 
help_menu_about_cb (GnomeUIHandler *uih, void *data, char *path)
{
	static GtkWidget *about;
	const gchar *authors[] = {
		"Dietmar Maurer",
		NULL
	};

	about = gnome_about_new 
		( "Gnome Shell", VERSION,
		  "Copyright (C) 1999 Maurer IT Systemlösungen"
		  "                     ",
		  authors,
		  _("This program is part of the GNOME project for "
		    "Linux. Gnome Shell comes with ABSOLUTELY NO "
		    "WARRANTY. This is free software, and you are "
		    "welcome to redistribute it under the conditions "
		    "of the GNU General Public License. "
		    "Please report bugs to dietmar@maurer-it.com"),
		  NULL);
	
	gtk_widget_show (about);
}

void 
edit_menu_preferences_cb (GnomeUIHandler *uih, void *data, char *path)
{
	g_warning ("edit_menu_properties_cb not implemented");
}

/*
 * Use query_interface to see if `obj' has `interface'.
 */
gboolean
gnome_object_has_interface (GnomeObject *obj, char *interface)
{
	CORBA_Environment ev;
	CORBA_Object requested_interface;
	gboolean retval;
	
	CORBA_exception_init (&ev);

	requested_interface = GNOME_Unknown_query_interface (
		gnome_object_corba_objref (obj), interface, &ev);

	if (!CORBA_Object_is_nil(requested_interface, &ev) && 
	    ev._major == CORBA_NO_EXCEPTION) {
		/* Get rid of the interface we've been passed */
		GNOME_Unknown_unref (requested_interface, &ev);
		CORBA_Object_release (requested_interface, &ev);
		retval = TRUE;
	} else retval = FALSE;

	CORBA_exception_free (&ev);
	return retval;
}

Frame *
get_active_frame (GtkWidget *widget)
{
	GtkWidget *top;
	GList *l;
	Frame *frame;

	g_return_val_if_fail (widget != NULL, NULL);
	g_return_val_if_fail (GTK_IS_WIDGET(widget), NULL);

	top = gtk_widget_get_toplevel(widget);
	for (l = app.frame_list;l;l = l->next) {
		frame = (Frame *)l->data;
		if (frame->app == top) return frame;
	}
	return NULL;
}

static void
container_system_exception_cb (GnomeObject *container_object, 
			       CORBA_Object cobject,
			       CORBA_Environment *ev, gpointer data)
{
	gnome_warning_dialog (_("fatal CORBA exception!  Shutting down..."));

	gnome_object_destroy (GNOME_OBJECT (app.container));

	gtk_main_quit ();
}

static void
client_site_system_exception_cb (GnomeObject *client, CORBA_Object cobject,
				 CORBA_Environment *ev, gpointer data)
{
	//Buffer *buffer = (Buffer *) data;
	g_warning ("component shutdown not implemented");
	//gnome_object_destroy (GNOME_OBJECT (buffer->client_site));
}

static void
view_system_exception_cb (GnomeViewFrame *view_frame, CORBA_Object cobject,
			  CORBA_Environment *ev, gpointer data)
{
	g_warning ("view frame system exception not implemented");
	gnome_object_destroy (GNOME_OBJECT (view_frame));
}

static void
user_activate_request_cb (GnomeViewFrame *view_frame, Frame *frame)
{

	if (frame->active_view_frame == view_frame) return;

        if (frame->active_view_frame != NULL) {
                gnome_view_frame_view_deactivate (frame->active_view_frame);
		if (frame->active_view_frame != NULL)
			gnome_view_frame_set_covered 
				(frame->active_view_frame, TRUE); 
		frame->active_view_frame = NULL;
	}

        gnome_view_frame_view_activate (view_frame);
}

static void
view_activated_cb (GnomeViewFrame *view_frame, gboolean activated, 
		   Frame *frame)
{
	GtkWidget *w;

        if (activated) {
		if (frame->active_view_frame != NULL) {
			g_warning ("View requested to be activated "
				   "but there is already "
				   "an active View!\n");
			return;
		}
		gnome_view_frame_set_covered (view_frame, FALSE);
                frame->active_view_frame = view_frame;
		w = gnome_view_frame_get_wrapper (view_frame);
		//gtk_container_focus (GTK_CONTAINER(w), GTK_DIR_UP);
		gtk_widget_grab_focus(w);
		set_file_menu_state (frame, TRUE);
		set_edit_menu_state (frame, TRUE);
        } else {
		gnome_view_frame_set_covered (view_frame, TRUE);
		if (view_frame == frame->active_view_frame)
			frame->active_view_frame = NULL;
        }                                                   
}

void
buffer_add_view (Buffer *buffer, Frame *frame, gint pos)
{
	GnomeViewFrame *view_frame;
	GtkWidget *view_widget;
	GtkWidget *box,*l;

	view_frame = gnome_client_site_new_view_full (buffer->client_site,
						      FALSE, TRUE);

	if (!view_frame) {
		g_warning("create view failed");
		return;
	}

	gtk_object_set_data (GTK_OBJECT(view_frame), "gshell:buffer", buffer);
	frame->view_list = g_list_append (frame->view_list, view_frame);

	gtk_signal_connect (GTK_OBJECT (view_frame), "system_exception",
			    view_system_exception_cb, buffer);
	gtk_signal_connect (GTK_OBJECT (view_frame), "user_activate",
			    GTK_SIGNAL_FUNC (user_activate_request_cb), frame);
	gtk_signal_connect (GTK_OBJECT (view_frame), "view_activated",
			    GTK_SIGNAL_FUNC (view_activated_cb), frame);

	gnome_view_frame_set_ui_handler (view_frame, frame->uih);
	
	view_widget = gnome_view_frame_get_wrapper (view_frame);
	
	box = gtk_vbox_new (FALSE,0);
	gtk_object_set_data (GTK_OBJECT(box), "gshell:view_frame", view_frame);

	l = gtk_label_new (buffer->name);
	gtk_box_pack_start (GTK_BOX (box), l, FALSE, FALSE, 0);
	gtk_box_pack_start (GTK_BOX (box), view_widget, TRUE, TRUE, 0);

	gtk_box_pack_end (GTK_BOX (frame->vbox), box, TRUE, TRUE, 0);

	if (pos>=0) gtk_box_reorder_child (GTK_BOX (frame->vbox), box, pos);

	gtk_widget_show_all (box);

	user_activate_request_cb (view_frame, frame);
}

Buffer *
buffer_create (const char *component_goad_id)
{
	Buffer *buffer;
	GnomeClientSite *client_site;
	GnomeObjectClient *server;

	server = gnome_object_activate_with_goad_id (NULL, component_goad_id, 
						     0, NULL);
	if (!server) {
		g_warning("Launching component failed");
		return NULL;
	}

	client_site = gnome_client_site_new (app.container);

	if (!gnome_client_site_bind_embeddable (client_site, server)) {
		gnome_object_unref (GNOME_OBJECT (server));
		gnome_object_unref (GNOME_OBJECT (client_site));
		g_warning("Bind component failed");
		return NULL;
	}
	
	buffer = g_new0 (Buffer, 1);
	buffer->client_site = client_site;
	buffer->server = server;
	app.buffer_list = g_list_append (app.buffer_list, buffer);

	gtk_signal_connect (GTK_OBJECT (client_site), "system_exception",
			    client_site_system_exception_cb, buffer);

	return buffer;
}

static GtkWidget *
find_view_frame_pos (Frame *frame, GnomeViewFrame *view_frame, gint *pos)
{
	GList *l;
	gint i = 0;
	GtkObject *o = NULL;

	l = GTK_BOX(frame->vbox)->children;
	
	while (l && (o = GTK_OBJECT(((GtkBoxChild *)l->data)->widget)) && 
	       (gtk_object_get_data (o, "gshell:view_frame") != view_frame)) {
		i++; l = l->next; 
	}

	if (pos) *pos = i;

	if (o) return GTK_WIDGET (o);
	return NULL;
}

gint
view_remove (Frame *frame, GnomeViewFrame *view_frame)
{
	GtkWidget *w,*b;
	gint pos;

	w = gnome_view_frame_get_wrapper (view_frame);
	frame->view_list = g_list_remove (frame->view_list, view_frame);
	if (frame->active_view_frame == view_frame) 
		frame->active_view_frame = NULL;

	b = find_view_frame_pos (frame, view_frame, &pos);
	
	gnome_object_destroy(GNOME_OBJECT(view_frame));
	if (b) gtk_widget_destroy (b);
	return pos;
}

static void
frame_close_cb (GtkWidget *widget)
{
	Frame *frame;
	GnomeViewFrame *view_frame;
	GList *v;

	frame = get_active_frame (widget);

	while ((v = frame->view_list)) {
		view_frame = (GnomeViewFrame *)v->data;
		view_remove (frame, view_frame);
	}

	app.frame_list = g_list_remove (app.frame_list, frame);
	gtk_widget_destroy (widget);
	if (!g_list_length(app.frame_list)) gtk_main_quit ();
}

void 
file_menu_exit_cb (GnomeUIHandler *uih, void *data, char *path)
{
	gnome_object_destroy (GNOME_OBJECT (app.container));
	gtk_main_quit ();
}

void
set_buffer_cb (GnomeUIHandler *uih, void *buffer,
	       const char *path)
{
	Buffer *tmp;
	Frame *frame;
	GnomeViewFrame *view_frame;
	gint pos = -1;

	g_return_if_fail(uih != NULL);
	g_return_if_fail(GNOME_IS_UI_HANDLER(uih));

	frame = get_active_frame (GTK_WIDGET(uih->top->app));

	if ((view_frame = get_active_view_frame (frame))) {
		tmp = gtk_object_get_data(GTK_OBJECT(view_frame), 
					  "gshell:buffer");
		if (tmp == buffer) return;
		pos = view_remove (frame, view_frame);
	}
	buffer_add_view (buffer, frame, pos);
}

void 
view_menu_delete_window_cb (GnomeUIHandler *uih, void *data, char *path)
{
	g_return_if_fail(uih != NULL);
	g_return_if_fail(GNOME_IS_UI_HANDLER(uih));
	
	frame_close_cb(GTK_WIDGET(uih->top->app));
}

static Frame *
frame_create ()
{
	Frame *frame;
	GtkWidget *statusbar;

	frame = g_new0 (Frame, 1);
	frame->app = gnome_app_new ("gshell", "Gnome Shell");

	gtk_window_set_default_size (GTK_WINDOW (frame->app), 640, 400);
	gtk_window_set_policy (GTK_WINDOW (frame->app), TRUE, TRUE, FALSE);

	gtk_signal_connect(GTK_OBJECT(frame->app), "delete_event",
			   GTK_SIGNAL_FUNC(frame_close_cb), NULL);

	statusbar = gtk_statusbar_new ();

	frame->vbox = gtk_vbox_new (FALSE, 0);
	gnome_app_set_contents (GNOME_APP (frame->app), frame->vbox);
	gnome_app_set_statusbar (GNOME_APP (frame->app), statusbar);

	frame->uih = gnome_ui_handler_new ();
	gnome_ui_handler_set_app (frame->uih, GNOME_APP (frame->app));
	gnome_ui_handler_set_statusbar (frame->uih, statusbar);

	frame_create_menus (frame);
	frame_create_toolbar (frame);

	gtk_widget_show_all (frame->app);

	app.frame_list = g_list_append (app.frame_list, frame);

	return frame;
}

GnomeViewFrame *
get_active_view_frame (Frame *frame)
{
	GtkWidget *w = NULL;
	GnomeViewFrame *view_frame = NULL;

	if (!(w = GTK_CONTAINER(frame->vbox)->focus_child)) {
		if (g_list_length(frame->view_list)) {
			view_frame = GNOME_VIEW_FRAME(frame->view_list->data);
			return view_frame;
		}

	}

       	if (w) return gtk_object_get_data (GTK_OBJECT (w), 
					   "gshell:view_frame");

	return NULL;
}

void 
view_menu_new_window_cb (GnomeUIHandler *uih, void *data, char *path)
{
	Frame *frame;
	GnomeViewFrame *view_frame;
	Buffer *buffer = NULL;

	g_return_if_fail(uih != NULL);
	g_return_if_fail(GNOME_IS_UI_HANDLER(uih));

	frame = get_active_frame (GTK_WIDGET(uih->top->app));

	if ((view_frame = get_active_view_frame (frame))) {
		buffer = gtk_object_get_data(GTK_OBJECT(view_frame), 
					     "gshell:buffer");
	}

	frame = frame_create ();
	if (buffer) buffer_add_view (buffer, frame, -1);
	update_buffer_menu ();
}

void 
view_menu_split_window_cb (GnomeUIHandler *uih, void *data, char *path)
{
	Frame *frame;
	GnomeViewFrame *view_frame;
	Buffer *buffer = NULL;
	gint pos;

	g_return_if_fail(uih != NULL);
	g_return_if_fail(GNOME_IS_UI_HANDLER(uih));

	frame = get_active_frame (GTK_WIDGET(uih->top->app));

	if ((view_frame = get_active_view_frame (frame))) {
		find_view_frame_pos (frame, view_frame, &pos);
		buffer = gtk_object_get_data(GTK_OBJECT(view_frame), 
					     "gshell:buffer");
		if (buffer) buffer_add_view (buffer, frame, pos);
	}
}

void 
view_menu_one_window_cb (GnomeUIHandler *uih, void *data, char *path)
{
	Frame *frame;
	GnomeViewFrame *view_frame, *vf;
	GList *v;

	g_return_if_fail(uih != NULL);
	g_return_if_fail(GNOME_IS_UI_HANDLER(uih));

	frame = get_active_frame (GTK_WIDGET(uih->top->app));
	if ((view_frame = get_active_view_frame (frame))) {
		for (v = frame->view_list; v;) {
			vf = (GnomeViewFrame *)v->data;
			v = v->next;
			if (view_frame != vf) view_remove (frame, vf);
		}
	}
}

void 
file_menu_kill_cb (GnomeUIHandler *uih, void *data, char *path)
{
	Frame *frame;
	Buffer *buffer=NULL;
	GnomeViewFrame *view_frame;
	GList *f,*v,*b;
	gchar buf[100];

	g_return_if_fail(uih != NULL);
	g_return_if_fail(GNOME_IS_UI_HANDLER(uih));

	if (!(frame = get_active_frame (GTK_WIDGET(uih->top->app)))) return;

	if ((view_frame = get_active_view_frame (frame))) {
		buffer = gtk_object_get_data(GTK_OBJECT(view_frame), 
					     "gshell:buffer");
	} else return;	  
	
	if (!buffer) return;	  

	app.buffer_list = g_list_remove (app.buffer_list, buffer);

	for (f = app.frame_list; f; f = f->next) {
		frame = (Frame *)f->data;
		for (v = frame->view_list; v;) {
			view_frame = (GnomeViewFrame *)v->data;
			v = v->next;
			if (buffer == gtk_object_get_data
			    (GTK_OBJECT(view_frame), "gshell:buffer")) {
				view_remove (frame, view_frame);
				if (!g_list_length (GTK_BOX(frame->vbox)->children)) {
					if ((b = g_list_last (app.buffer_list))) {
						buffer_add_view ((Buffer *)b->data, frame, -1);
					} else {
						set_file_menu_state (frame, FALSE);
						set_edit_menu_state (frame, FALSE);
					}
				}
			}
		}
	}

	gnome_object_unref (GNOME_OBJECT(buffer->client_site));

	for (f = app.frame_list; f; f = f->next) {
		uih = ((Frame *)f->data)->uih;
		sprintf (buf, "/Buffers/(%d)",buffer->menu_id);
		gnome_ui_handler_menu_remove (uih, buf);		
	}

	if (buffer->name) g_free (buffer->name);
	g_free (buffer);
}

int
main (int argc, char **argv)
{
	CORBA_Environment ev;
	CORBA_ORB orb;

	bindtextdomain (PACKAGE, GNOMELOCALEDIR);
	textdomain (PACKAGE);

	CORBA_exception_init (&ev);

	gnome_CORBA_init ("gshell", "1.0", &argc, argv, 0, &ev);

	CORBA_exception_free (&ev);

	orb = gnome_CORBA_ORB ();

	if (bonobo_init (orb, NULL, NULL) == FALSE)
		g_error (_("Could not initialize Bonobo!\n"));

	app.container = gnome_container_new ();
	gtk_signal_connect (GTK_OBJECT (app.container), "system_exception",
			    container_system_exception_cb, NULL);

	frame_create ();

	bonobo_main ();

	return 0;
}
