/*
 * menu/toolbar related code
 *
 * Author:
 *   Dietmar Maurer (dietmar@maurer-it.com)
 *
 * Copyright 1999 Maurer IT Systemlösungen (http://www.maurer-it.com)
 */

#include "menu.h"
#include "inout.h"

static GnomeUIInfo help_menu [] = {
	GNOMEUIINFO_ITEM_NONE (N_("Help on GNOME"), NULL, 
			       help_menu_help_gnome_cb),
	GNOMEUIINFO_SEPARATOR,
	GNOMEUIINFO_MENU_ABOUT_ITEM (help_menu_about_cb, NULL),
	GNOMEUIINFO_END
};

static GnomeUIInfo buffer_menu [] = {
	GNOMEUIINFO_END
};

static GnomeUIInfo tools_menu [] = {
	GNOMEUIINFO_ITEM_NONE (N_("Spelling"), NULL, NULL),
	GNOMEUIINFO_ITEM_NONE (N_("Calendar"), NULL, NULL),
	GNOMEUIINFO_END
};

static GnomeUIInfo view_menu [] = {
	GNOMEUIINFO_MENU_NEW_WINDOW_ITEM (view_menu_new_window_cb, NULL),
	GNOMEUIINFO_MENU_CLOSE_WINDOW_ITEM (view_menu_delete_window_cb, NULL),
	GNOMEUIINFO_ITEM_NONE (N_("Split Window"), NULL, 
			       view_menu_split_window_cb),
	GNOMEUIINFO_ITEM_NONE (N_("One Window"), NULL, 
			       view_menu_one_window_cb),
	GNOMEUIINFO_SEPARATOR,
	GNOMEUIINFO_ITEM_NONE (N_("Zoom..."), NULL, NULL),
	GNOMEUIINFO_END
};

static GnomeUIInfo edit_menu [] = {
	GNOMEUIINFO_MENU_UNDO_ITEM (NULL, NULL),
	GNOMEUIINFO_MENU_REDO_ITEM (NULL, NULL),
	GNOMEUIINFO_SEPARATOR,
	GNOMEUIINFO_MENU_CUT_ITEM (NULL, NULL),
	GNOMEUIINFO_MENU_COPY_ITEM (NULL, NULL),
	GNOMEUIINFO_MENU_PASTE_ITEM (NULL, NULL),
	GNOMEUIINFO_SEPARATOR,
	GNOMEUIINFO_MENU_PREFERENCES_ITEM (edit_menu_preferences_cb, NULL),
	GNOMEUIINFO_END
};

static GnomeUIInfo file_menu [] = {
	GNOMEUIINFO_MENU_OPEN_ITEM (file_menu_open_cb, NULL),
	GNOMEUIINFO_MENU_SAVE_ITEM (file_menu_save_cb, NULL),
	GNOMEUIINFO_MENU_SAVE_AS_ITEM (file_menu_save_as_cb, NULL),
	GNOMEUIINFO_ITEM_NONE(N_("Kill Current Buffer"), NULL, 
			      file_menu_kill_cb),
	GNOMEUIINFO_SEPARATOR,
	GNOMEUIINFO_MENU_PRINT_ITEM (NULL, NULL),
	GNOMEUIINFO_MENU_PRINT_SETUP_ITEM (NULL, NULL),
	GNOMEUIINFO_SEPARATOR,
	GNOMEUIINFO_MENU_EXIT_ITEM (file_menu_exit_cb, NULL),
	GNOMEUIINFO_END
};

static GnomeUIInfo main_menu [] = { 
	GNOMEUIINFO_SUBTREE (N_("_Buffers"), buffer_menu),
	GNOMEUIINFO_MENU_FILE_TREE (file_menu),
	GNOMEUIINFO_MENU_EDIT_TREE (edit_menu),
	GNOMEUIINFO_MENU_VIEW_TREE (view_menu),
	GNOMEUIINFO_SUBTREE (N_("_Tools"), tools_menu),
	GNOMEUIINFO_MENU_HELP_TREE (help_menu),
	GNOMEUIINFO_END
};

static GnomeUIInfo toolbar[] = {
  GNOMEUIINFO_ITEM_STOCK(N_("Open"), NULL,
			 file_menu_open_cb,GNOME_STOCK_MENU_OPEN),
  GNOMEUIINFO_ITEM_STOCK(N_("Save"), NULL,
			 file_menu_save_cb,GNOME_STOCK_MENU_SAVE),
  GNOMEUIINFO_END
};

void
set_file_menu_state (Frame *frame, gboolean state)
{
	GnomeUIHandler *uih;

	g_return_if_fail (frame != NULL);

	uih = frame->uih;

	gnome_ui_handler_menu_set_sensitivity (uih, "/File/Print", state);
	gnome_ui_handler_menu_set_sensitivity (uih, "/File/Save", state);
	gnome_ui_handler_menu_set_sensitivity (uih, "/File/Save As...", state);
	gnome_ui_handler_menu_set_sensitivity (uih, 
					       "/File/Kill Current Buffer", 
					       state);
	gnome_ui_handler_menu_set_sensitivity (uih, 
					       "/View/Split Window", state);
	gnome_ui_handler_menu_set_sensitivity (uih, 
					       "/View/One Window", state);


}

void
set_edit_menu_state (Frame *frame, gboolean state)
{
	GnomeUIHandler *uih;

	g_return_if_fail (frame != NULL);

	uih = frame->uih;

	gnome_ui_handler_menu_set_sensitivity (uih, "/Edit/Undo", state);
	gnome_ui_handler_menu_set_sensitivity (uih, "/Edit/Redo", state);
	gnome_ui_handler_menu_set_sensitivity (uih, "/Edit/Cut", state);
	gnome_ui_handler_menu_set_sensitivity (uih, "/Edit/Copy", state);
	gnome_ui_handler_menu_set_sensitivity (uih, "/Edit/Paste", state);
}

void 
frame_create_menus (Frame *frame)
{
	GnomeUIHandlerMenuItem *list;
	GnomeUIHandler *uih;

	g_return_if_fail (frame != NULL);

	uih = frame->uih;
	gnome_ui_handler_create_menubar (uih);
	list = gnome_ui_handler_menu_parse_uiinfo_list_with_data 
		(main_menu, frame);
	gnome_ui_handler_menu_add_list (uih, "/", list);
	gnome_ui_handler_menu_free_list (list);

	set_file_menu_state (frame, FALSE);
	set_edit_menu_state (frame, FALSE);
}

void 
frame_create_toolbar (Frame *frame)
{
	GnomeUIHandlerToolbarItem *list;

	gnome_ui_handler_create_toolbar (frame->uih, "Common");
	list = gnome_ui_handler_toolbar_parse_uiinfo_list_with_data 
		(toolbar, frame);
	gnome_ui_handler_toolbar_add_list (frame->uih, "/Common", list);
	gnome_ui_handler_toolbar_free_list (list);
}

void
update_buffer_menu (void)
{
	Frame *frame;
	Buffer *buffer;
	gchar path[100];
	GList *f,*b;
	GnomeUIHandler *uih;

	/* fixme: set the right menu pixmap (from mime type) */

	for (f = app.frame_list; f; f = f->next) {
		frame = (Frame *)f->data;
		uih = frame->uih;
		for (b = app.buffer_list; b; b = b->next) {
			buffer = (Buffer *)b->data;
			sprintf (path, "/Buffers/(%d)",buffer->menu_id);
			/* fixme: direct access to internal data of uih!! */
			if (g_hash_table_lookup (uih->top->path_to_menu_item, 
						 path))
				gnome_ui_handler_menu_remove (uih, path);
			gnome_ui_handler_menu_new_item 
				(uih, path, g_basename(buffer->name), NULL, 
				 -1, GNOME_UI_HANDLER_PIXMAP_STOCK,
				 GNOME_STOCK_PIXMAP_NEW,
				 0, 0, set_buffer_cb, buffer);
		}
	}
}



