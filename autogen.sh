#!/bin/sh
# Run this to generate all the initial makefiles, etc.

srcdir=`dirname $0`
test -z "$srcdir" && srcdir=.

PKG_NAME="Bonobo"

(test -f $srcdir/configure.in \
  && test -d $srcdir/idl) || {
    echo -n "**Error**: Directory "\`$srcdir\'" does not look like the"
    echo " top-level bonobo directory"
    exit 1
}

. $srcdir/macros/autogen.sh
