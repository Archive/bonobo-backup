/*
 * gnome-moniker-client.c: Helper routines for Moniker clients
 *
 * Authors:
 *    Matt Loper      (matt@gnome-support.com)
 *    Miguel de Icaza (miguel@kernel.org)
 *
 * Copyright 1999 Helix Code, Inc.
 */
#include <config.h>
#include <bonobo/gnome-object.h>
#include <bonobo/gnome-moniker.h>
#include <bonobo/bonobo.h>
#include <bonobo/gnome-moniker-client.h>
#include <libgnorba/gnorba.h>

/**
 * gnome_moniker_find_in_naming_service:
 * @name: Name of the moniker as registered in the name server.
 * @goad_id: GOAD ID to locate
 *
 * Looks up in the Name service a service (whose GOAD ID is @goad_id)
 * a file moniker which handles the @name filename.
 *
 * Returns: The CORBA object for the server that contains the @name.
 */
CORBA_Object
gnome_moniker_find_in_naming_service (const char *name, const char *goad_id)
{
	CosNaming_NameComponent nc[3] = {{"GNOME", "subcontext"},
					 {"Monikers", "subcontext"}};
	CosNaming_Name          nom;
	CORBA_Object name_server;
	CORBA_Object object_in_name_server;
	CORBA_Environment ev;

	g_assert (name);
	nom._maximum = 0;
	nom._length = 3;
	nom._buffer = nc;
	nom._release = CORBA_FALSE;

	CORBA_exception_init (&ev);

	nc[2].id   = (char *)name;
	nc[2].kind = (char *)goad_id;

	name_server = gnome_name_service_get();

	g_assert(name_server != CORBA_OBJECT_NIL);

	object_in_name_server = CosNaming_NamingContext_resolve(name_server, &nom, &ev);

	if(ev._major == CORBA_NO_EXCEPTION
	   || (ev._major == CORBA_USER_EXCEPTION
	       && strcmp(CORBA_exception_id(&ev),
			 ex_CosNaming_NamingContext_NotFound))) {

		/* found it! */

		CORBA_Object_release(name_server, &ev);

		CORBA_exception_free(&ev);
		return object_in_name_server;
	}

	/* didn't find it, return nothing */
	CORBA_Object_release(name_server, &ev);
	CORBA_exception_free(&ev);

	return CORBA_OBJECT_NIL;

} 

static void
set_moniker_data (GtkObject *moniker, char *name, char *kind)
{
	gtk_object_set_data (moniker, "gnome-moniker-name", name);
	gtk_object_set_data (moniker, "gnome-moniker-kind", kind);
}

/**
 * gnome_moniker_unregister:
 * @moniker: the Moniker to unregister.
 *
 * Unregisters the moniker that has been previously registered
 * with the name service from it.
 */
void
gnome_moniker_unregister (GnomeMoniker *moniker)
{
	CosNaming_NameComponent nc[3] = {{"GNOME", "subcontext"},
					 {"Monikers", "subcontext"}};
	CosNaming_Name          nom;
	CORBA_Object name_server;
	CORBA_Environment ev;
	char *name, *goad_id;

	g_return_if_fail (moniker != NULL);
	g_return_if_fail (GNOME_IS_MONIKER (moniker));

	/*
	 * 1. Fetch values we stored
	 */
	name = gtk_object_get_data (GTK_OBJECT (moniker), "gnome-moniker-name");
	goad_id = gtk_object_get_data (GTK_OBJECT (moniker), "gnome-moniker-kind");

	/*
	 * 2. Unbind from the name server
	 */
	nom._maximum = 0;
	nom._length = 3;
	nom._buffer = nc;
	nom._release = CORBA_FALSE;

	CORBA_exception_init (&ev);

	nc[2].id   = (char *)name;
	nc[2].kind = (char *)goad_id;

	name_server = gnome_name_service_get ();
	g_assert (name_server != CORBA_OBJECT_NIL);

	CosNaming_NamingContext_unbind (name_server, &nom, &ev);

	CORBA_Object_release (name_server, &ev);
	CORBA_exception_free (&ev);

	/*
	 * 3. Shutdown the registration information we kept arond
	 */
	g_free (name);
	g_free (goad_id);

	set_moniker_data (GTK_OBJECT (moniker), NULL, NULL);
}

/**
 * gnome_moniker_register:
 * @moniker: the moniker to register in the naming service
 * @name: The name to register.
 * @goad_id: the GOAD ID for the handler for the file name in @name
 *
 * This routine registers a moniker handler for the file in @name as
 * serviced by the server referenced by @goad_id.
 *
 * This routine would be typically used by a server object when it start
 * providing services for a file.  For example, the spreadsheet would
 * invoke this routine when a file is opened.
 *
 * When a file is closed by a server, then the unregister routine would
 * be invoked (or alternatively, destroying this moniker will automatically
 * unregister the moniker from the name service).
 */
void
gnome_moniker_register (GnomeMoniker *moniker, const char *name, const char *goad_id)
{
	CosNaming_NameComponent nc[3] = {{"GNOME", "subcontext"},
					 {"Monikers", "subcontext"}};
	CosNaming_Name          nom;
	CORBA_Object name_server;
	CORBA_Object server;
	CORBA_Environment ev;

	g_return_if_fail (moniker != NULL);
	g_return_if_fail (GNOME_IS_MONIKER (moniker));
	g_return_if_fail (name != NULL);
	g_return_if_fail (goad_id != NULL);

	/*
	 * 1. For book-keeping
	 */
	gtk_signal_connect (GTK_OBJECT (moniker), "destroy",
			    gnome_moniker_unregister, NULL);

	/*
	 * 2. Do the actual registration with the name server
	 */
	nom._maximum = 0;
	nom._length = 3;
	nom._buffer = nc;
	nom._release = CORBA_FALSE;

	CORBA_exception_init (&ev);

	nc[2].id   = (char *)name;
	nc[2].kind = (char *)goad_id;

	name_server = gnome_name_service_get ();
	g_assert (name_server != CORBA_OBJECT_NIL);

	server = gnome_object_corba_objref (GNOME_OBJECT (moniker));
	CosNaming_NamingContext_bind (name_server, &nom, server, &ev);

	CORBA_Object_release (name_server, &ev);
	CORBA_exception_free (&ev);

	/*
	 * 3. Information we keep around for unregistering.
	 */
	set_moniker_data (GTK_OBJECT (moniker), g_strdup (name), g_strdup (goad_id));
}

