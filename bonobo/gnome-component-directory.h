/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Bonobo
 *
 *  Copyright (C) 1999 Red Hat Software.
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Elliot Lee <sopwith@redhat.com>
 *          hacked from gmf/libgmf/gmf-filterregistry.*
 *
 */

#ifndef GNOME_COMPONENT_DIRECTORY_H
#define GNOME_COMPONENT_DIRECTORY_H 1

typedef enum {
	COMPONENT_FUNCTION_DISPLAY = 1 << 0,
	COMPONENT_FUNCTION_EDIT = 1 << 1,
	COMPONENT_FUNCTION_PRINT = 1 << 2,
	COMPONENT_FUNCTION_INPLACE = 1 << 3
} GnomeComponentFunctionality;

typedef struct {
	char *goad_id;
	char **mime_types;
	GnomeComponentFunctionality functionality;
} GnomeComponentDirectoryEntry;

typedef struct {
	/* value is a BonoboDirectoryEntry pointer */
	GSList *list;
	/* value is a BonoboDirectoryEntry pointer */
	GHashTable *by_id;
	/* value is a GSList pointer */
	GHashTable *by_mime_type;
} GnomeComponentDirectory;

CORBA_Object gnome_component_activate(GoadServerList *slist,
				      GnomeComponentDirectoryEntry *entry);
CORBA_Object gnome_component_activate_for_type(GoadServerList *slist,
					       GnomeComponentDirectory *dir,
					       const char *mime_type,
					       GnomeComponentFunctionality required_functionality);

/* returns a component for the specified file */
CORBA_Object gnome_component_activate_for_url(GoadServerList *slist,
					      GnomeComponentDirectory *dir,
					      const char *url,
					      GnomeComponentFunctionality required_functionality);

GnomeComponentDirectory *gnome_component_directory_get(void);
void gnome_component_directory_free(GnomeComponentDirectory *dir);

#endif /* GNOME_COMPONENT_DIRECTORY_H */
