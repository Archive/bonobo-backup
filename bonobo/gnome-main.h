/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

#ifndef __GNOME_MAIN_H__
#define __GNOME_MAIN_H__ 1

#include <libgnome/gnome-defs.h>
#include <gtk/gtkobject.h>
#include <bonobo/bonobo.h>

gboolean		    bonobo_init			 (CORBA_ORB orb,
							  PortableServer_POA poa,
							  PortableServer_POAManager manager);
void			    bonobo_main			 (void);
gboolean		    bonobo_activate		 (void);
void			    bonobo_setup_x_error_handler (void);

CORBA_ORB		    bonobo_orb			 (void);
PortableServer_POA	    bonobo_poa			 (void);
PortableServer_POAManager  bonobo_poa_manager		 (void);

#endif  __GNOME_MAIN_H__
