/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
#ifndef _GNOME_COMPOSITE_MONIKER_H_
#define _GNOME_COMPOSITE_MONIKER_H_

#include <bonobo/gnome-moniker.h>

BEGIN_GNOME_DECLS

typedef struct {
	GnomeMoniker moniker;
} GnomeCompositeMoniker;

typedef struct {
	GnomeMonikerClass parent_class;
} GnomeCompositeMonikerClass;

GnomeCompositeMoniker *gnome_composite_moniker_new (GnomeMoniker *head,
						    GnomeMoniker *tail);
END_GNOME_DECLS

#endif
