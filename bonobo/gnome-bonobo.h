/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Main include file for the Bonobo component model
 *
 * FIXME: We should try to optimize the order of the include
 * files here to minimize repeated inclussion of files.
 *
 * Copyright 1999 Helix Code, Inc.
 */

#include <bonobo/gnome-object.h>

#include <bonobo/gnome-moniker.h>
#include <bonobo/gnome-moniker-client.h>

#include <bonobo/gnome-container.h>
#include <bonobo/gnome-object-client.h>
#include <bonobo/gnome-client-site.h>

#include <bonobo/gnome-property-bag.h>
#include <bonobo/gnome-property-bag-client.h>

#include <bonobo/gnome-control.h>
#include <bonobo/gnome-control-frame.h>

#include <bonobo/gnome-view.h>
#include <bonobo/gnome-generic-factory.h>

#include <bonobo/gnome-embeddable.h>
#include <bonobo/gnome-embeddable-factory.h>

#include <bonobo/gnome-main.h>

#include <bonobo/gnome-stream.h>
#include <bonobo/gnome-stream-client.h>
#include <bonobo/gnome-stream-fs.h>

#include <bonobo/gnome-persist.h>
#include <bonobo/gnome-persist-file.h>
#include <bonobo/gnome-persist-stream.h>

#include <bonobo/gnome-ui-handler.h>

#include <bonobo/gnome-object-io.h>

#include <bonobo/gnome-progressive.h>

#include <bonobo/gnome-storage.h>

#include <bonobo/gnome-bonobo-selector.h>

#include <bonobo/gnome-bonobo-widget.h>

