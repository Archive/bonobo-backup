/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*--------------------------------*-C-*---------------------------------*
 *
 *  Copyright 1999, Matt Loper <matt@gnome-support.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 *
 *----------------------------------------------------------------------*/

/*
 * This program will automatically load Embeddables from the goad
 * activation library.
 */

#include <config.h>
#include <gnome.h>
#include <libgnorba/gnorba.h>
#include <bonobo/gnome-bonobo.h>

#include <bonobo/gnome-stream-fs.h>

CORBA_Environment ev;
CORBA_ORB orb;

/* the list of servers given to us by libgnorba */
GoadServerList *gslist = NULL;
GoadServer *servers = NULL;

typedef struct {
	GtkWidget *app;
	GnomeContainer *container;
	GtkWidget *box;
} Application;

Application *app;


/* Add the client_site to the container's list of ClientSites,
 * and do the actual libgnorba-based activation of the embedded object */
static GnomeObjectClient *
launch_server (GnomeClientSite *client_site,
	       GnomeContainer *container, char *goadid)
{
	GnomeObjectClient *object_server;
	
	gnome_container_add (container, GNOME_OBJECT (client_site));

	object_server = gnome_object_activate_with_goad_id (
		NULL, goadid, 0, NULL);

	if (!object_server) {
		g_warning (_("Can not activate object_server\n"));
		return NULL;
	}

	if (!gnome_client_site_bind_embeddable (client_site, object_server))
	{
		g_warning (_("Can not bind object server to client_site\n"));
		return NULL;
	}

	return object_server;
} /* launch_server */

/* Each embedded Embeddable can be destroyed by hitting
 * its `destroy' button.*/
static void
destroy_embedded_object_cb (GtkWidget *btn, GnomeObject *embedded_object)
{
	g_warning ("Right now, unref'ing an embedded Embeddable doesn't work.\nTo be fixed soon.\n");
	gtk_widget_show_all (
		gnome_message_box_new (
		"Right now, unref'ing an embedded Embeddable doesn't work.\nTo be fixed soon.",
		GNOME_MESSAGE_BOX_WARNING, "Ok", NULL));
	
	GNOME_Unknown_unref (gnome_object_corba_objref (embedded_object), &ev);
	CORBA_Object_release (gnome_object_corba_objref (embedded_object), &ev);
}

/* When an embedded object is being loaded, a file dlg
 * appears to let you choose the file to hydrate it from.
 * Hitting cancel in the file dlg means entering this routine
 */
static void
cancel_hydrating_object_from_file_cb (GtkWidget *cancel_btn, GNOME_PersistStream persist)
{
	/* we're done with the file selection dlg, so... */
	gtk_widget_destroy (gtk_widget_get_toplevel (cancel_btn));
	
	GNOME_Unknown_unref (persist, &ev);
	CORBA_Object_release (persist, &ev);
} /* cancel_hydrating_object_from_file_cb */

/* The user has chosen a file to load an embedded
 * object from; this function does the work for that */
static void
hydrate_object_from_file_cb (GtkWidget *ok_btn, GNOME_PersistStream persist)
{
	GtkFileSelection *fs = GTK_FILE_SELECTION (
		gtk_widget_get_toplevel (ok_btn));
	
	gchar *filename = gtk_file_selection_get_filename (fs);
	
	GnomeStream *stream
		= gnome_stream_fs_open (filename, GNOME_Storage_READ);

	GNOME_Stream stream_obj =
		(GNOME_Stream) gnome_object_corba_objref (GNOME_OBJECT (stream));

	if (stream == NULL){
		g_print ("I could not open %s!\n", filename);
		return;
	}

	GNOME_PersistStream_load (persist, stream_obj, &ev);
        if (ev._major != CORBA_NO_EXCEPTION)
		g_print ("The bonobo object failed to load the file %s\n",
			 filename);
	
	/* we're done with the file selection dlg, so... */
	gtk_widget_destroy (GTK_WIDGET (fs));

	GNOME_Unknown_unref (persist, &ev);
	CORBA_Object_release (persist, &ev);	

} /* hydrate_object_from_file_cb */

/* Someone hit the `load' button for this embedded object,
 * so we should create a file dialog to let the object
 * hydrate from a particular file */
static void
create_persist_file_dialog_cb (GtkWidget *unused, GnomeObject *object)
{
	GtkFileSelection *fs;
	GNOME_PersistStream persist;
	
	g_assert (object);

	persist = GNOME_Unknown_query_interface (
		gnome_object_corba_objref (GNOME_OBJECT (object)),
		"IDL:GNOME/PersistStream:1.0", &ev);

        if (ev._major != CORBA_NO_EXCEPTION || !persist)
                return;

	fs = GTK_FILE_SELECTION (
		gtk_file_selection_new ("file selection dialog"));

	gtk_file_selection_hide_fileop_buttons (fs);

	gtk_window_set_position (GTK_WINDOW (fs),
				 GTK_WIN_POS_MOUSE);

	gtk_signal_connect (GTK_OBJECT (fs->ok_button),
			    "clicked",
			    GTK_SIGNAL_FUNC(hydrate_object_from_file_cb),
			    persist);

	gtk_signal_connect (GTK_OBJECT (fs->cancel_button),
			    "clicked",
			    GTK_SIGNAL_FUNC(cancel_hydrating_object_from_file_cb),
			    persist);
      
	if (!GTK_WIDGET_VISIBLE (GTK_WIDGET (fs))) {
		gtk_window_set_modal (GTK_WINDOW (fs), TRUE);
		gtk_widget_show (GTK_WIDGET (fs));
	}

} /* create_persist_file_dialog_cb */


/* Given the goad_id for a Embeddable, activate such
 * an object, and put it in our container.
 * Also include a few buttons to manipulate it with */
static GnomeObjectClient *
add_object_to_container (char *server_goadid)
{
	GtkWidget *frame, *hbox, *vbox, *btn;
	GnomeViewFrame *view_frame;
	GtkWidget *view_widget;
	GnomeObjectClient *server;
	GnomeClientSite *client_site;
	
	g_assert (app && server_goadid);

	client_site = gnome_client_site_new (app->container);

	server = launch_server (client_site, app->container, server_goadid);
	if (server == NULL)
		return NULL;

	view_frame = gnome_client_site_new_view (client_site);
	view_widget = gnome_view_frame_get_wrapper (view_frame);

	frame = gtk_frame_new (server_goadid);
	gtk_widget_show (frame);
	gtk_box_pack_start (GTK_BOX (app->box), frame, TRUE, TRUE, 0);

	vbox = gtk_vbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (vbox), view_widget, TRUE, TRUE, 0);

	hbox = gtk_hbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, FALSE, 5);

	gtk_container_add (GTK_CONTAINER (frame), vbox);
	gnome_view_frame_set_covered (view_frame, FALSE);

	/* add button to allow destruction of embedded Embeddable */
	btn = gtk_button_new_with_label ("Destroy");
	gtk_box_pack_start (GTK_BOX (hbox), btn, TRUE, FALSE, 0);
	gtk_signal_connect (GTK_OBJECT (btn), "clicked",
			    GTK_SIGNAL_FUNC (destroy_embedded_object_cb),
			    (GnomeObject*) server);
	
	if (gnome_object_client_has_interface (server,
					       "IDL:GNOME/PersistStream:1.0",
					       NULL)) {
		GtkWidget *button = gtk_button_new_with_label ("Load");
		gtk_box_pack_start (GTK_BOX (hbox), button, TRUE, FALSE, 0);
		gtk_signal_connect (GTK_OBJECT (button),
				    "clicked",
				    GTK_SIGNAL_FUNC(create_persist_file_dialog_cb),
				    server);
	}

	gtk_widget_show_all (vbox);
	
	return server;

} /* add_object_to_container */

static void
clean_up_and_quit ()
{
	if (gslist)
		goad_server_list_free (gslist);

	CORBA_exception_free (&ev);
	
	gtk_main_quit ();
	g_free (app);
	
	/* FIXME: am getting `GdkWindow unexpectedly destroyed' msgs
	 * when this is called from File->Quit */ 
} /* clean_up_and_quit */


/* A menu item, denoting a particular type of Embeddable,
 * has been selected. Fire up such an object and put it
 * in our container */
static void
embed_bonobo_object_cb (GtkWidget *widget, GoadServer *server)
{
	GnomeObjectClient *object;

	g_assert (server);
	
	object = add_object_to_container (server->server_id);

	if (object == NULL)
	{
		gnome_warning_dialog (_("Could not launch Embeddable."));
		return;
	}

} /* embed_bonobo_object_cb */
 

static gboolean
string_in_array(const char *string, const gchar **array)
{
	int i;
	for(i = 0; array[i]; i++) {
		if(!strcmp(string, array[i]))
			return TRUE;
	}

	return FALSE;
} /* string_in_array */

/* Given a GoadServer (which contains a goad_id,
 * description, etc.), add a menuitem which will
 * allow the activation of that type of object */
static void
add_object_to_menu (GnomeApp *app, GoadServer *sai)
{
	GnomeUIInfo entry[2];
	gchar *description;
	
	g_assert (sai && app);

	description = g_strdup_printf("Add \"%s\" Embeddable",
				      sai->description);
	
	entry[0].type = GNOME_APP_UI_ITEM;
	entry[0].label = description;
	entry[0].hint = NULL;
	entry[0].moreinfo = embed_bonobo_object_cb;
	entry[0].user_data = sai;
	entry[0].unused_data = NULL;
	entry[0].pixmap_type = GNOME_APP_PIXMAP_NONE;
	entry[0].pixmap_info = NULL;
	entry[0].accelerator_key = 0;
	entry[1].type = GNOME_APP_UI_ENDOFINFO;

	gnome_app_insert_menus (GNOME_APP (app),
				"File/",
				entry);

	g_free (description);
	
	return;
} /* add_object_to_menu */

/* Iterate through all the types of objects that can
 * be fired up with libgnorba. For each one that is
 * embeddable (ie has a Embeddable interface), add
 * a menuitem to allow its activation */
static void
add_objects_to_menu (GnomeApp *app)
{
	int i;

	/* get a list of servers from gnorba */
	gslist = goad_server_list_get ();

	if (!gslist) {
		g_print ("Couldn't get a GoadServerList!\n");
		return;
	}

	servers = gslist->list;
	if (!servers) {
		g_print ("The GoadServerList was empty!\n");
		return;
	}

	/* for each item from that list that has a
	 * bonobo_object interface, add it to the menu */
	for (i = 0; servers[i].repo_id; i++)
	{
		if (string_in_array ("IDL:GNOME/Embeddable:1.0",
				     (const gchar**)servers[i].repo_id))
		{
			add_object_to_menu (app, &servers[i]);
		}
		
	}
} /* add_objects_to_menu */


static GnomeUIInfo container_file_menu [] = {
	GNOMEUIINFO_SEPARATOR,
	GNOMEUIINFO_MENU_EXIT_ITEM (clean_up_and_quit, NULL),
	GNOMEUIINFO_END
};

static GnomeUIInfo container_main_menu [] = {
	GNOMEUIINFO_MENU_FILE_TREE (container_file_menu),
	GNOMEUIINFO_END
};


static Application *
application_new (void)
{
	app = g_new0 (Application, 1);
	app->app = gnome_app_new ("test-container",
				  "Sample Container Application");

        gtk_window_set_default_size(GTK_WINDOW(app->app), 640, 480);
        gtk_window_set_policy(GTK_WINDOW(app->app), TRUE, TRUE, FALSE);

	app->container = GNOME_CONTAINER (gnome_container_new ());

	app->box = gtk_vbox_new (FALSE, 0);
	gtk_widget_show (app->box);
	gnome_app_set_contents (GNOME_APP (app->app), app->box);

	gnome_app_create_menus_with_data (
		GNOME_APP (app->app),
		container_main_menu, app);

	add_objects_to_menu (GNOME_APP (app->app));

	gtk_signal_connect (GTK_OBJECT (app->app), "destroy",
			    (GtkSignalFunc)clean_up_and_quit, NULL);

	gtk_widget_show (app->app);

	return app;
	
} /* application_new */

int
main (int argc, char *argv [])
{
	CORBA_exception_init (&ev);
	
	gnome_CORBA_init ("test-container-autoload", "1.0",
			  &argc, argv, 0, &ev);
	
	orb = gnome_CORBA_ORB ();
	
	if (bonobo_init (orb, NULL, NULL) == FALSE)
		g_error (_("Can not bonobo_init\n"));

	app = application_new ();
	bonobo_activate ();
	
	gtk_main ();

	CORBA_exception_free (&ev);
	
	return 0;
}
