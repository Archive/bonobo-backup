/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
#ifndef _GNOME_ITEM_MONIKER_H_
#define _GNOME_ITEM_MONIKER_H_

#include <bonobo/gnome-moniker.h>

BEGIN_GNOME_DECLS

#define GNOME_ITEM_MONIKER_TYPE        (gnome_item_moniker_get_type ())
#define GNOME_ITEM_MONIKER(o)          (GTK_CHECK_CAST ((o), GNOME_ITEM_MONIKER_TYPE, GnomeItemMoniker))
#define GNOME_ITEM_MONIKER_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), GNOME_ITEM_MONIKER_TYPE, GnomeItemMonikerClass))
#define GNOME_IS_ITEM_MONIKER(o)       (GTK_CHECK_TYPE ((o), GNOME_ITEM_MONIKER_TYPE))
#define GNOME_IS_ITEM_MONIKER_CLASS(k) (GTK_CHECK_CLASS_TYPE ((k), GNOME_ITEM_MONIKER_TYPE))

#define GNOME_ITEM_MONIKER_GOAD_ID "GOAD:GNOME:Moniker:Item:1.0"

typedef struct {
	GnomeMoniker moniker;

	char  delimited;
	char *item_name;
} GnomeItemMoniker;

typedef struct {
	GnomeMonikerClass parent_class;
} GnomeItemMonikerClass;

GnomeItemMoniker *gnome_item_moniker_new (char delimiter,
					  const char *item_name);
					    
END_GNOME_DECLS

#endif
