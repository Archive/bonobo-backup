/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/**
 * GNOME File Moniker
 *
 * Monikers are used as "nick names" or handles to a specific object
 * instantiated with a specific set of data.  They can be thought of
 * as a short-hand recipe for invoking a given object with a specified
 * data set.  This invocation process is often referred to as
 * "hydrating."
 *
 * Monikers are typically used in "Object Linking," which is what
 * takes place, for example, when a region of a spread sheet is copied
 * and pasted into a word processor.  Instead of passing the actual
 * spreadsheet data between the applications, a moniker representing
 * the region of the spreadsheet is passed.  The moniker is used to
 * hydrate an embedded spreadhsheet object which is "linked" to the
 * original data, meaning that changes in the original spreadsheet
 * will be reflected in the new, pasted copy.
 *
 * A file moniker is used to activate a component using the data
 * contained in a given file.  Both the component name and the file
 * name are specified in the file moniker, which is a composition
 * of the two:
 *
 *		(compose "gnumeric" "/home/nat/spreadsheet.gnumeric")
 *
 * The moniker above (represented as a lisp composition of a component
 * name and a file name) can be used to create a Gnumeric spreadsheet
 * component and load it with the spreadsheet data contained in
 * /nome/nat/spreadsheet.gnumeric.
 *
 * The moniker used in the example above is actual a composite moniker
 * made up of a GnomeMoniker (represented by "gnumeric") and a
 * GnomeFileMoniker ("/home/ndf/spreadsheet.gnumeric").
 *
 * So a file moniker is used to specify a file whose contents will be
 * used to hydrate a given component.
 *
 * Author:
 *   Miguel de Icaza (miguel@kernel.org)
 */
#include <config.h>
#include <gtk/gtksignal.h>
#include <gtk/gtkmarshal.h>
#include <bonobo/gnome-file-moniker.h>
#include <bonobo/gnome-stream-fs.h>
#include <bonobo/gnome-moniker-client.h>

#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <sys/mman.h>

/* Parent GTK object class */
static GnomeMonikerClass *gnome_file_moniker_parent_class;

static void
gnome_file_moniker_destroy (GtkObject *object)
{
	GnomeFileMoniker *gfm = GNOME_FILE_MONIKER (object);

	g_free (gfm->filename);

	GTK_OBJECT_CLASS (gnome_file_moniker_parent_class)->destroy (object);
} 

static void
gnome_file_moniker_class_init (GnomeFileMonikerClass *klass)
{
	GtkObjectClass *object_class = (GtkObjectClass *) klass;

	gnome_file_moniker_parent_class = gtk_type_class (gnome_moniker_get_type ());

	object_class->destroy = gnome_file_moniker_destroy;
} 

static void
gnome_file_moniker_init (GnomeObject *object)
{
} 

/**
 * gnome_file_moniker_get_type:
 *
 * Returns: The GtkType of the GnomeFileMoniker class.
 */
GtkType
gnome_file_moniker_get_type (void)
{
	static GtkType type = 0;

	if (!type){
		GtkTypeInfo info = { 
			"IDL:GNOME/FileMoniker:1.0",
			sizeof (GnomeFileMoniker),
			sizeof (GnomeFileMonikerClass),
			(GtkClassInitFunc) gnome_file_moniker_class_init,
			(GtkObjectInitFunc) gnome_file_moniker_init,
			NULL, /* reserved 1 */
			NULL, /* reserved 2 */
			(GtkClassInitFunc) NULL
		};

		type = gtk_type_unique (gnome_moniker_get_type (), &info);
	}

	return type;
} 

/*
 * A moniker must have a way of binding to (and returning) the
 * object it points to. Here's how a file moniker does it
 */
static CORBA_Object
gnome_file_moniker_bind (GnomeMoniker *moniker,
			 GNOME_BindOptions bind_options,
			 GNOME_Moniker left_moniker,
			 void *closure)
{
	GnomeFileMoniker *fm = GNOME_FILE_MONIKER (moniker);
	CORBA_Object rtn = CORBA_OBJECT_NIL;
	CORBA_Object persist;
	CORBA_Environment ev;
	gchar *name, *kind;
	
	name = g_strdup_printf ("file_moniker!%s", fm->filename);
	kind = GNOME_PERSIST (fm)->goad_id;
	
	/* 1. Check the naming service to see if we're already available */
	rtn = gnome_moniker_find_in_naming_service (name, kind);
	
	g_free (name);

	if (rtn == CORBA_OBJECT_NIL){
		return rtn;
	}
					    
	/* 2. If we get here, and we don't have a goad_id, bail */
	if (!fm->goad_id) {
		g_print ("Can't associate a mime_type with a goad_id yet\n");
	}

	/* 3. fire up that object specified by the goad_id  */
	rtn = goad_server_activate_with_id (
		NULL,         /* name_server list */
		fm->goad_id,  /* server to activate */
		0,            /* GoadActivationFlags */
		0);           /* params for activation */

	CORBA_exception_init (&ev);

	if (CORBA_Object_is_nil (rtn, &ev)) /* bail */ {
		CORBA_exception_free (&ev);
		return CORBA_OBJECT_NIL;
	}

	/*
	 * 4. try to feed it the file (first by
	 * PersistFile, then by PersistStream)
	 */
	persist = GNOME_Unknown_query_interface (
		rtn, "IDL:GNOME/PersistFile:1.0", &ev);

	if (persist) {
		gboolean success = FALSE;
		
		GNOME_PersistFile_load (persist, fm->filename, &ev);

		if (ev._major != CORBA_NO_EXCEPTION)
			success = FALSE;
		
		GNOME_Unknown_unref (persist, &ev);
		CORBA_Object_release (persist, &ev);

		if (!success){
			GNOME_Unknown_unref (rtn, &ev);
			CORBA_Object_release (rtn, &ev);
			rtn = CORBA_OBJECT_NIL;
		}
	} else {
		/* doesn't have PersistFile; try with PersistStream */
		GnomeStream *stream;
		
		persist = GNOME_Unknown_query_interface (
			rtn, "IDL:GNOME/PersistStream:1.0", &ev);

		CORBA_exception_free (&ev);
		
		if (!persist)
			rtn = CORBA_OBJECT_NIL;
		else {
			stream = gnome_stream_fs_open (
				fm->filename, GNOME_Storage_READ);
			if (!stream)
				rtn = CORBA_OBJECT_NIL;
			else {
				GNOME_PersistStream_load (
					(GNOME_PersistStream) persist,
					gnome_object_corba_objref (GNOME_OBJECT (stream)),
					&ev);
				
				if (ev._major != CORBA_NO_EXCEPTION)
					rtn = CORBA_OBJECT_NIL;

				GNOME_Unknown_unref (persist, &ev);
				CORBA_Object_release (persist, &ev);


			}
		}
	}
	
	CORBA_exception_free (&ev);
	return rtn;
} 

/**
 * gnome_file_moniker_construct:
 * @gfm: The GnomeFileMoniker object to be initialized.
 * @gfm_corba: A CORBA interface object for the GNOME_Moniker interface.
 * @filename: The name of the file to which @gfm should correspond.
 * @goad_id: The GOAD id for the server that can activate @filename
 *
 * Returns: the constructed GnomeFileMoniker object.
 */
GnomeFileMoniker *
gnome_file_moniker_construct (GnomeFileMoniker *gfm,
			      GNOME_Moniker gfm_corba,
			      const char *filename,
			      const char *goad_id)
{
	g_return_val_if_fail (gfm != NULL, NULL);
	g_return_val_if_fail (GNOME_IS_FILE_MONIKER (gfm), NULL);
	g_return_val_if_fail (filename != NULL, NULL);
	g_return_val_if_fail (gfm_corba != CORBA_OBJECT_NIL, NULL);

	gnome_moniker_construct (GNOME_MONIKER (gfm), gfm_corba,
				 GNOME_FILE_MONIKER_GOAD_ID,
				 gnome_file_moniker_bind, NULL);

	gfm->filename = g_strdup (filename);

	return gfm;
} 

/**
 * gnome_file_moniker_new:
 * @filename: the name of the file this moniker represents
 * @goad_id: the GOAD id of the server that is supposed to load this file
 *
 * This function creates a new file moniker for loading the file in @filename
 * with the server specified by @goad_id.
 *
 * Returns: A GnomeFileMoniker object.
 */
GnomeFileMoniker *
gnome_file_moniker_new (const char *filename, const char *goad_id)
{
	GnomeFileMoniker *gfm;
	GNOME_Moniker gfm_corba;
	
	g_return_val_if_fail (filename != NULL, NULL);

	gfm = gtk_type_new (gnome_file_moniker_get_type ());
	gfm_corba = gnome_moniker_create_corba_object (
		GNOME_MONIKER (gfm));
	if (gfm_corba == CORBA_OBJECT_NIL){
		gtk_object_destroy (GTK_OBJECT (gfm));
		return NULL;
	}
	gnome_file_moniker_construct (gfm,
				      gfm_corba,
				      filename, goad_id);

	return gfm;
} 
