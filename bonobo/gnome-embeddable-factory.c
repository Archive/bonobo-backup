/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/**
 * GNOME EmbeddableFactory object.
 *
 * The GnomeEmbeddableFactory object is used to instantiate new
 * GnomeEmbeddable objects.  It acts as a wrapper for the
 * GNOME::EmbeddableFactory CORBA interface, and dispatches to
 * a user-specified factory function whenever its create_objecd()
 * method is invoked.
 *
 * Author:
 *   Miguel de Icaza (miguel@kernel.org)
 *
 * Copyright 1999 Helix Code, Inc.
 */
#include <config.h>
#include <gtk/gtksignal.h>
#include <gtk/gtkmarshal.h>
#include <libgnorba/gnorba.h>
#include <bonobo/bonobo.h>
#include <bonobo/gnome-main.h>
#include <bonobo/gnome-embeddable-factory.h>

POA_GNOME_GenericFactory__vepv gnome_embeddable_factory_vepv;

static GnomeObjectClass *gnome_embeddable_factory_parent_class;

static CORBA_boolean
impl_GNOME_EmbeddableFactory_supports (PortableServer_Servant servant,
				       const CORBA_char *obj_goad_id,
				       CORBA_Environment *ev)
{
	GnomeObject *object = gnome_object_from_servant (servant);

	g_message ("support invoked\n");
	return TRUE;
}

static CORBA_Object
impl_GNOME_EmbeddableFactory_create_object (PortableServer_Servant servant,
					    const CORBA_char *obj_goad_id,
					    const GNOME_stringlist *params,
					    CORBA_Environment *ev)
{
	GnomeEmbeddableFactoryClass *class;
	GnomeEmbeddableFactory *factory;
	GnomeObject *object;

	factory = GNOME_EMBEDDABLE_FACTORY (gnome_object_from_servant (servant));

	class = GNOME_EMBEDDABLE_FACTORY_CLASS (GTK_OBJECT (factory)->klass);
	object = (*class->new_embeddable)(factory);

	if (!object)
		return CORBA_OBJECT_NIL;
	
	return CORBA_Object_duplicate (gnome_object_corba_objref (GNOME_OBJECT (object)), ev);
}

static CORBA_Object
create_gnome_embeddable_factory (GnomeObject *object)
{
	POA_GNOME_GenericFactory *servant;
	CORBA_Environment ev;

	CORBA_exception_init (&ev);

	servant = (POA_GNOME_GenericFactory *)g_new0 (GnomeObjectServant, 1);
	servant->vepv = &gnome_embeddable_factory_vepv;

	POA_GNOME_GenericFactory__init ((PortableServer_Servant) servant, &ev);
	if (ev._major != CORBA_NO_EXCEPTION){
		g_free (servant);
		CORBA_exception_free (&ev);
		return CORBA_OBJECT_NIL;
	}

	CORBA_exception_free (&ev);
	return gnome_object_activate_servant (object, servant);
}

/**
 * gnome_embeddable_factory_construct:
 * @goad_id: The GOAD id that the new factory will implement.
 * @c_factory: The object to be initialized.
 * @corba_factory: The CORBA object which supports the
 * GNOME::EmbeddableFactory interface and which will be used to
 * construct this GnomeEmbeddableFactory Gtk object.
 * @factory: A callback which is used to create new GnomeEmbeddable object instances.
 * @data: The closure data to be passed to the @factory callback routine.
 *
 * Initializes @c_factory with the command-line arguments and registers
 * the new factory in the name server.
 *
 * Returns: The initialized GnomeEmbeddableFactory object.
 */
GnomeEmbeddableFactory *
gnome_embeddable_factory_construct (const char *goad_id,
				    GnomeEmbeddableFactory *c_factory,
				    GNOME_Embeddable       corba_factory,
				    GnomeEmbeddableFactoryFn factory,
				    void *data)
{
	CORBA_Environment ev;
	int ret;
	
	g_return_val_if_fail (c_factory != NULL, NULL);
	g_return_val_if_fail (GNOME_IS_EMBEDDABLE_FACTORY (c_factory), NULL);
	g_return_val_if_fail (factory != NULL, NULL);
	g_return_val_if_fail (corba_factory != CORBA_OBJECT_NIL, NULL);
	
	gnome_object_construct (GNOME_OBJECT (c_factory), corba_factory);

	c_factory->factory = factory;
	c_factory->factory_closure = data;
	c_factory->goad_id = g_strdup (goad_id);

	CORBA_exception_init (&ev);

	ret = goad_server_register (
		NULL, corba_factory, c_factory->goad_id, "server",
		&ev);

	CORBA_exception_free (&ev);

	if (ret != 0){
		gnome_object_unref (GNOME_OBJECT (c_factory));
		return NULL;
	}
	
	return c_factory;
}

/**
 * gnome_embeddable_factory_new:
 * @goad_id: The GOAD id that this factory implements
 * @factory: A callback which is used to create new GnomeEmbeddable object instances.
 * @data: The closure data to be passed to the @factory callback routine.
 *
 * This is a helper routine that simplifies the creation of factory
 * objects for GNOME::Embeddable.  The @factory function will be
 * invoked by the CORBA server when a request arrives to create a new
 * instance of an object supporting the GNOME::Embeddable interface.
 * The factory callback routine is passed the @data pointer to provide
 * the creation function with some state information.
 *
 * Returns: A GnomeEmbeddableFactory object that has an activated
 * GNOME::EmbeddableFactory object that has registered with the GNOME
 * name server.
 */
GnomeEmbeddableFactory *
gnome_embeddable_factory_new (const char *goad_id, GnomeEmbeddableFactoryFn factory, void *data)
{
	GnomeEmbeddableFactory *c_factory;
	GNOME_GenericFactory corba_factory;

	g_return_val_if_fail (factory != NULL, NULL);
	
	c_factory = gtk_type_new (gnome_embeddable_factory_get_type ());

	corba_factory = create_gnome_embeddable_factory (GNOME_OBJECT (c_factory));
	if (corba_factory == CORBA_OBJECT_NIL){
		gtk_object_destroy (GTK_OBJECT (c_factory));
		return NULL;
	}
	
	return gnome_embeddable_factory_construct (
		goad_id, c_factory, corba_factory, factory, data);
}

static void
gnome_embeddable_factory_finalize (GtkObject *object)
{
	GnomeEmbeddableFactory *c_factory G_GNUC_UNUSED = GNOME_EMBEDDABLE_FACTORY (object);
	CORBA_Environment ev;

	CORBA_exception_init (&ev);
	goad_server_unregister (NULL, c_factory->goad_id, "server", &ev);
	CORBA_exception_free (&ev);
	g_free (c_factory->goad_id);
	
	GTK_OBJECT_CLASS (gnome_embeddable_factory_parent_class)->destroy (object);
}

static GnomeObject *
gnome_embeddable_factory_new_embeddable (GnomeEmbeddableFactory *factory)
{
	g_return_val_if_fail (factory != NULL, NULL);
	g_return_val_if_fail (GNOME_IS_EMBEDDABLE_FACTORY (factory), NULL);

	return (*factory->factory)(factory, factory->factory_closure);
}

static void
gnome_embeddable_factory_class_init (GnomeEmbeddableFactoryClass *klass)
{
	GtkObjectClass *object_class = (GtkObjectClass *) klass;

	gnome_embeddable_factory_parent_class = gtk_type_class (gnome_object_get_type ());

	object_class->finalize = gnome_embeddable_factory_finalize;

	class->new_embeddable = gnome_embeddable_factory_new_embeddable;

	init_embeddable_factory_corba_class ();
}

static void
gnome_embeddable_factory_init (GnomeObject *object)
{
}

/**
 * gnome_embeddable_factory_get_type:
 *
 * Returns: The GtkType of the GnomeEmbeddableFactory class.
 */
GtkType
gnome_embeddable_factory_get_type (void)
{
	static GtkType type = 0;

	if (!type){
		GtkTypeInfo info = {
			"IDL:GNOME/EmbeddableFactory:1.0",
			sizeof (GnomeEmbeddableFactory),
			sizeof (GnomeEmbeddableFactoryClass),
			(GtkClassInitFunc) gnome_embeddable_factory_class_init,
			(GtkObjectInitFunc) gnome_embeddable_factory_init,
			NULL, /* reserved 1 */
			NULL, /* reserved 2 */
			(GtkClassInitFunc) NULL
		};

		type = gtk_type_unique (gnome_object_get_type (), &info);
	}

	return type;
}

/**
 * gnome_embeddable_factory_set:
 * @c_factory: The GnomeEmbeddableFactory object whose callback will be set.
 * @factory: A callback routine which is used to create new object instances.
 * @data: The closure data to be passed to the @factory callback.
 *
 * Sets the callback and callback closure for @c_factory to
 * @factory and @data, respectively.
 */
void
gnome_embeddable_factory_set (GnomeEmbeddableFactory *c_factory,
			      GnomeEmbeddableFactoryFn factory,
			      void *data)
{
	g_return_if_fail (c_factory != NULL);
	g_return_if_fail (GNOME_IS_EMBEDDABLE_FACTORY (c_factory));
	g_return_if_fail (factory != NULL);

	c_factory->factory = factory;
	c_factory->factory_closure = data;
}

/**
 * gnome_embeddable_factory_get_epv:
 *
 */
POA_GNOME_GenericFactory__epv *
gnome_embeddable_factory_get_epv (void)
{
	POA_GNOME_GenericFactory__epv *epv;

	epv = g_new0 (POA_GNOME_GenericFactory__epv, 1);

	epv->supports = impl_GNOME_EmbeddableFactory_supports;
	epv->create_object = impl_GNOME_EmbeddableFactory_create_object;

	return epv;
}

static void
init_embeddable_factory_corba_class (void)
{
	gnome_embeddable_factory_vepv.GNOME_Unknown_epv = gnome_object_get_epv ();
	gnome_embeddable_factory_vepv.GNOME_EmbeddableFactory_epv =
		gnome_embeddable_factory_get_epv ();
}
