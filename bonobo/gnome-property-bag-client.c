/*
 * Author:
 *   Nat Friedman (nat@nat.org)
 *
 * Copyright 1999, Helix Code, Inc.
 */
#include <config.h>
#include <bonobo/gnome-property-bag-client.h>
#include <bonobo/gnome-property-types.h>

static GnomePropertyBagClient *
gnome_property_bag_client_construct (GnomePropertyBagClient *pbc,
				     GNOME_PropertyBag corba_pb)
{
	pbc->corba_pb = corba_pb;

	return pbc;
}

/**
 * gnome_property_bag_client_new:
 * @corba_property_bag: A CORBA object reference for a remote
 * #GNOME_PropertyBag.
 *
 * Returns: A new #GnomePropertyBagClient object which can
 * be used to access @corba_property_bag.  You can, of course,
 * interact with @corba_property_bag directly using CORBA; this
 * object is just provided as a convenience.
 */
GnomePropertyBagClient *
gnome_property_bag_client_new (GNOME_PropertyBag corba_property_bag)
{
	GnomePropertyBagClient *pbc;

	g_return_val_if_fail (corba_property_bag != CORBA_OBJECT_NIL, NULL);

	pbc = gtk_type_new (gnome_property_bag_client_get_type ());

	return gnome_property_bag_client_construct (pbc, corba_property_bag);
}

/**
 * gnome_property_bag_client_get_properties:
 * @pbc: A #GnomePropertyBagClient which is bound to a remote
 * #GNOME_PropertyBag.
 *
 * Returns: A #GList filled with #GNOME_Property CORBA object
 * references for all of the properties stored in the remote
 * #GnomePropertyBag.
 */
GList *
gnome_property_bag_client_get_properties (GnomePropertyBagClient *pbc)
{
	GNOME_PropertyList  *props;
	GList		    *prop_list;
	int		     i;
	CORBA_Environment    ev;

	g_return_val_if_fail (pbc != NULL, NULL);
	g_return_val_if_fail (GNOME_IS_PROPERTY_BAG_CLIENT (pbc), NULL);

	CORBA_exception_init (&ev);

	props = GNOME_PropertyBag_get_properties (pbc->corba_pb, &ev);
	if (ev._major != CORBA_NO_EXCEPTION) {
		gnome_object_check_env (GNOME_OBJECT (pbc), pbc->corba_pb, &ev);
		CORBA_exception_free (&ev);
		return NULL;
	}

	prop_list = NULL;
	for (i = 0; i < props->_length; i ++) {

		/*
		 * FIXME: Is it necessary to duplicate these?  I'm
		 * inclined to think that it isn't.
		 */
		prop_list = g_list_prepend (
			prop_list,
			CORBA_Object_duplicate (props->_buffer [i], &ev));
		if (ev._major != CORBA_NO_EXCEPTION) {
			CORBA_Environment ev2;
			GList *curr;

			CORBA_exception_free (&ev);

			for (curr = prop_list; curr != NULL; curr = curr->next) {
				CORBA_exception_init (&ev);
				CORBA_Object_release ((CORBA_Object) curr->data, &ev2);
				CORBA_exception_free (&ev);
			}

			g_list_free (prop_list);

			return NULL;
		}
	}

	CORBA_exception_free (&ev);

	CORBA_free (props);

	return prop_list;
}

/**
 * gnome_property_bag_client_free_properties:
 * @list: A #GList containing GNOME_Property corba objrefs (as
 * produced by gnome_property_bag_client_get_properties(), for
 * example).
 *
 * Releases the CORBA Objrefs stored in @list and frees the list.
 */
void
gnome_property_bag_client_free_properties (GList *list)
{
	GList *l;

	if (list == NULL)
		return;

	for (l = list; l != NULL; l = l->next) {
		CORBA_Environment ev;
		GNOME_Property    prop;

		prop = (GNOME_Property) l->data;

		CORBA_exception_init (&ev);

		CORBA_Object_release (prop, &ev);

		if (ev._major != CORBA_NO_EXCEPTION) {
			g_warning ("gnome_property_bag_client_free_properties: Exception releasing objref!");
			CORBA_exception_free (&ev);
			return;
		}

		CORBA_exception_free (&ev);
	}

	g_list_free (list);
}

/**
 * gnome_property_bag_client_get_property_names:
 * @pbc: A #GnomePropertyBagClient which is bound to a remote
 * #GNOME_PropertyBag.
 *
 * This function exists as a convenience, so that you don't have to
 * iterate through all of the #GNOME_Property objects in order to get
 * a list of their names.  It should be used in place of such an
 * iteration, as it uses fewer resources on the remote
 * #GnomePropertyBag.
 *
 * Returns: A #GList filled with strings containing the names of all
 * the properties stored in the remote #GnomePropertyBag.
 */
GList *
gnome_property_bag_client_get_property_names (GnomePropertyBagClient *pbc)
{
	GNOME_PropertyNames  *names;
	GList		     *name_list;
	int		      i;
	CORBA_Environment     ev;

	g_return_val_if_fail (pbc != NULL, NULL);
	g_return_val_if_fail (GNOME_IS_PROPERTY_BAG_CLIENT (pbc), NULL);

	CORBA_exception_init (&ev);

	names = GNOME_PropertyBag_get_property_names (pbc->corba_pb, &ev);
	if (ev._major != CORBA_NO_EXCEPTION) {
		CORBA_exception_free (&ev);
		return NULL;
	}
	
	CORBA_exception_free (&ev);

	name_list = NULL;
	for (i = 0; i < names->_length; i ++) {
		 char *name;

		 name = g_strdup (names->_buffer [i]);
		 name_list = g_list_prepend (name_list, name);
	}

	CORBA_free (names);

	return name_list;
}

/**
 * gnome_property_bag_client_get_property:
 * @pbc: A GnomePropertyBagClient which is associated with a remote
 * GnomePropertyBag.
 * @name: A string containing the name of the property which is to
 * be fetched.
 *
 * Returns: A #GNOME_Property CORBA object reference for the
 *
 */
GNOME_Property
gnome_property_bag_client_get_property (GnomePropertyBagClient *pbc,
					const char *property_name)
{
	CORBA_Environment ev;
	GNOME_Property    prop;

	g_return_val_if_fail (pbc != NULL, NULL);
	g_return_val_if_fail (GNOME_IS_PROPERTY_BAG_CLIENT (pbc), NULL);

	CORBA_exception_init (&ev);

	prop = GNOME_PropertyBag_get_property (pbc->corba_pb, property_name, &ev);
	if (ev._major != CORBA_NO_EXCEPTION) {
		CORBA_exception_free (&ev);
		return CORBA_OBJECT_NIL;
	}

	CORBA_exception_free (&ev);

	return prop;
}


/*
 * Bonobo Property streaming functions.
 */

/**
 * gnome_property_bag_client_persist:
 * @pbc: A #GnomePropertyBagClient object which is bound to a remote
 * #GNOME_PropertyBag server.
 * @stream: A #GnomeStream into which the data in @pbc will be written.
 *
 * Reads the property data stored in the #GNOME_Property_bag to which
 * @pbc is bound and streams it into @stream.  The typical use for
 * this function is to save the property data for a given Bonobo
 * Control into a persistent store to which @stream is attached.
 */
void
gnome_property_bag_client_persist (GnomePropertyBagClient *pbc,
				   GNOME_Stream stream)
{
	GNOME_PersistStream persist;
	CORBA_Environment   ev;

	g_return_if_fail (pbc != NULL);
	g_return_if_fail (GNOME_IS_PROPERTY_BAG_CLIENT (pbc));
	g_return_if_fail (stream != NULL);
	g_return_if_fail (GNOME_IS_STREAM (stream));

	CORBA_exception_init (&ev);

	persist = GNOME_Unknown_query_interface (pbc->corba_pb, "IDL:GNOME/PersistStream:1.0", &ev);

	if (ev._major != CORBA_NO_EXCEPTION ||
	    persist   == CORBA_OBJECT_NIL) {
		g_warning ("GnomePropertyBagClient: No PersistStream interface "
			   "found on remote PropertyBag!\n");
		CORBA_exception_free (&ev);
		return;
	}

	GNOME_PersistStream_save (persist, stream, &ev);

	if (ev._major != CORBA_NO_EXCEPTION) {
		g_warning ("GnomePropertyBagClient: Exception caught while persisting "
			   "remote PropertyBag!\n");
		CORBA_exception_free (&ev);
		return;
	}

	GNOME_Unknown_unref  (persist, &ev);
	if (ev._major != CORBA_NO_EXCEPTION) {
		g_warning ("GnomePropertyBagClient: Exception caught while unrefing PersistStream!\n");
		CORBA_exception_free (&ev);
		CORBA_exception_init (&ev);
	}

	CORBA_Object_release (persist, &ev);
	if (ev._major != CORBA_NO_EXCEPTION) {
		g_warning ("GnomePropertyBagClient: Exception caught while releasing "
			   "PersistStream objref!\n");
	}

	CORBA_exception_free (&ev);
}

/**
 * gnome_property_bag_client_depersist:
 */
void
gnome_property_bag_client_depersist (GnomePropertyBagClient *pbc,
				     GNOME_Stream stream)
{
	GNOME_PersistStream persist;
	CORBA_Environment   ev;

	g_return_if_fail (pbc != NULL);
	g_return_if_fail (GNOME_IS_PROPERTY_BAG_CLIENT (pbc));
	g_return_if_fail (stream != NULL);
	g_return_if_fail (GNOME_IS_STREAM (stream));

	CORBA_exception_init (&ev);

	persist = GNOME_Unknown_query_interface (pbc->corba_pb, "IDL:GNOME/PersistStream:1.0", &ev);

	if (ev._major != CORBA_NO_EXCEPTION ||
	    persist   == CORBA_OBJECT_NIL) {
		g_warning ("GnomePropertyBagClient: No PersistStream interface "
			   "found on remote PropertyBag!\n");
		CORBA_exception_free (&ev);
		return;
	}

	GNOME_PersistStream_load (persist, stream, &ev);

	if (ev._major != CORBA_NO_EXCEPTION) {
		g_warning ("GnomePropertyBagClient: Exception caught while persisting "
			   "remote PropertyBag!\n");
		CORBA_exception_free (&ev);
		return;
	}

	GNOME_Unknown_unref  (persist, &ev);
	if (ev._major != CORBA_NO_EXCEPTION) {
		g_warning ("GnomePropertyBagClient: Exception caught while unrefing PersistStream!\n");
		CORBA_exception_free (&ev);
		CORBA_exception_init (&ev);
	}

	CORBA_Object_release (persist, &ev);
	if (ev._major != CORBA_NO_EXCEPTION) {
		g_warning ("GnomePropertyBagClient: Exception caught while releasing "
			   "PersistStream objref!\n");
	}

	CORBA_exception_free (&ev);
}


/*
 * Property convenience functions.
 */

/**
 * gnome_property_bag_client_get_property_type:
 */
CORBA_TypeCode
gnome_property_bag_client_get_property_type (GnomePropertyBagClient *pbc,
					     const char *propname)
{
	CORBA_Environment ev;
	GNOME_Property prop;
	CORBA_TypeCode tc;

	g_return_val_if_fail (pbc != NULL, (CORBA_TypeCode) TC_null);
	g_return_val_if_fail (GNOME_IS_PROPERTY_BAG_CLIENT (pbc), (CORBA_TypeCode) TC_null);
	g_return_val_if_fail (propname != NULL, (CORBA_TypeCode) TC_null);

	prop = gnome_property_bag_client_get_property (pbc, propname);
	g_return_val_if_fail (prop != CORBA_OBJECT_NIL, (CORBA_TypeCode) TC_null);

	CORBA_exception_init (&ev);

	tc = GNOME_Property_get_type (prop, &ev);
	if (ev._major != CORBA_NO_EXCEPTION) {
		g_warning ("gnome_property_bag_client_get_property_type: Exception getting TypeCode!");

		CORBA_exception_free (&ev);
		CORBA_exception_init (&ev);

		CORBA_Object_release (prop, &ev);
		CORBA_exception_free (&ev);

		return (CORBA_TypeCode) TC_null;
	}

	CORBA_Object_release (prop, &ev);
	CORBA_exception_free (&ev);

	return tc;
}

typedef enum {
	FIELD_VALUE,
	FIELD_DEFAULT
} PropUtilFieldType;

static CORBA_any *
gnome_property_bag_client_get_field_any (GnomePropertyBagClient *pbc,
					 const char *propname,
					 PropUtilFieldType field)

{
	GNOME_Property     prop;
	CORBA_Environment  ev;
	CORBA_any         *any;

	g_return_val_if_fail (pbc != NULL, NULL);
	g_return_val_if_fail (GNOME_IS_PROPERTY_BAG_CLIENT (pbc), NULL);
	g_return_val_if_fail (propname != NULL, NULL);

	prop = gnome_property_bag_client_get_property (pbc, propname);
	g_return_val_if_fail (prop != CORBA_OBJECT_NIL, NULL);

	CORBA_exception_init (&ev);

	if (field == FIELD_VALUE)
		any = GNOME_Property_get_value (prop, &ev);
	else
		any = GNOME_Property_get_default (prop, &ev);

	if (ev._major != CORBA_NO_EXCEPTION) {
		g_warning ("gnome_property_bag_client_get_field_any: Exception getting property value!");

		CORBA_exception_free (&ev);
		CORBA_exception_init (&ev);

		CORBA_Object_release (prop, &ev);
		CORBA_exception_free (&ev);

		return NULL;
	}

	CORBA_Object_release (prop, &ev);
	CORBA_exception_free (&ev);

	return any;
}

static gboolean
gnome_property_bag_client_get_field_boolean (GnomePropertyBagClient *pbc,
					     const char *propname,
					     PropUtilFieldType field)
{
	CORBA_any *any;
	gboolean   b;

	g_return_val_if_fail (pbc != NULL, FALSE);
	g_return_val_if_fail (GNOME_IS_PROPERTY_BAG_CLIENT (pbc), FALSE);
	g_return_val_if_fail (propname != NULL, FALSE);

	any = gnome_property_bag_client_get_field_any (pbc, propname, field);

	if (any == NULL)
		return FALSE;

	g_return_val_if_fail (any->_type->kind == CORBA_tk_boolean, 0);

	b = *(gboolean *) any->_value;

	CORBA_any__free (any, NULL, TRUE);

	return b;
}

static gshort
gnome_property_bag_client_get_field_short (GnomePropertyBagClient *pbc,
					   const char *propname,
					   PropUtilFieldType field)
{
	CORBA_any *any;
	gshort     s;

	g_return_val_if_fail (pbc != NULL, 0);
	g_return_val_if_fail (GNOME_IS_PROPERTY_BAG_CLIENT (pbc), 0);
	g_return_val_if_fail (propname != NULL, 0);

	any = gnome_property_bag_client_get_field_any (pbc, propname, field);

	if (any == NULL)
		return 0;

	g_return_val_if_fail (any->_type->kind == CORBA_tk_short, 0);

	s = *(gshort *) any->_value;

	CORBA_any__free (any, NULL, TRUE);

	return s;
}

static gushort
gnome_property_bag_client_get_field_ushort (GnomePropertyBagClient *pbc,
					    const char *propname,
					    PropUtilFieldType field)
{
	CORBA_any *any;
	gushort    s;

	g_return_val_if_fail (pbc != NULL, 0);
	g_return_val_if_fail (GNOME_IS_PROPERTY_BAG_CLIENT (pbc), 0);
	g_return_val_if_fail (propname != NULL, 0);

	any = gnome_property_bag_client_get_field_any (pbc, propname, field);

	if (any == NULL)
		return 0.0;

	g_return_val_if_fail (any->_type->kind == CORBA_tk_ushort, 0);

	s = *(gushort *) any->_value;

	CORBA_any__free (any, NULL, TRUE);

	return s;
}

static glong
gnome_property_bag_client_get_field_long (GnomePropertyBagClient *pbc,
					  const char *propname,
					  PropUtilFieldType field)
{
	CORBA_any *any;
	glong      l;

	g_return_val_if_fail (pbc != NULL, 0);
	g_return_val_if_fail (GNOME_IS_PROPERTY_BAG_CLIENT (pbc), 0);
	g_return_val_if_fail (propname != NULL, 0);

	any = gnome_property_bag_client_get_field_any (pbc, propname, field);

	if (any == NULL)
		return 0.0;

	g_return_val_if_fail (any->_type->kind == CORBA_tk_long, 0);

	l = *(glong *) any->_value;

	CORBA_any__free (any, NULL, TRUE);

	return l;
}

static gulong
gnome_property_bag_client_get_field_ulong (GnomePropertyBagClient *pbc,
					   const char *propname,
					   PropUtilFieldType field)
{
	CORBA_any *any;
	gulong     l;

	g_return_val_if_fail (pbc != NULL, 0);
	g_return_val_if_fail (GNOME_IS_PROPERTY_BAG_CLIENT (pbc), 0);
	g_return_val_if_fail (propname != NULL, 0);

	any = gnome_property_bag_client_get_field_any (pbc, propname, field);

	if (any == NULL)
		return 0.0;

	g_return_val_if_fail (any->_type->kind == CORBA_tk_ulong, 0);

	l = *(gulong *) any->_value;

	CORBA_any__free (any, NULL, TRUE);

	return l;
}

static gfloat
gnome_property_bag_client_get_field_float (GnomePropertyBagClient *pbc,
					   const char *propname,
					   PropUtilFieldType field)
{
	CORBA_any *any;
	gfloat     f;

	g_return_val_if_fail (pbc != NULL, 0.0);
	g_return_val_if_fail (GNOME_IS_PROPERTY_BAG_CLIENT (pbc), 0.0);
	g_return_val_if_fail (propname != NULL, 0.0);

	any = gnome_property_bag_client_get_field_any (pbc, propname, field);

	if (any == NULL)
		return 0.0;

	g_return_val_if_fail (any->_type->kind == CORBA_tk_float, 0.0);

	f = *(gfloat *) any->_value;

	CORBA_any__free (any, NULL, TRUE);

	return f;
}

static gdouble
gnome_property_bag_client_get_field_double (GnomePropertyBagClient *pbc,
					    const char *propname,
					    PropUtilFieldType field)
{
	CORBA_any *any;
	gdouble    d;

	g_return_val_if_fail (pbc != NULL, 0.0);
	g_return_val_if_fail (GNOME_IS_PROPERTY_BAG_CLIENT (pbc), 0.0);
	g_return_val_if_fail (propname != NULL, 0.0);

	any = gnome_property_bag_client_get_field_any (pbc, propname, field);

	if (any == NULL)
		return 0.0;

	g_return_val_if_fail (any->_type->kind == CORBA_tk_double, 0.0);

	d = *(gdouble *) any->_value;

	CORBA_any__free (any, NULL, TRUE);

	return d;
}

static char *
gnome_property_bag_client_get_field_string (GnomePropertyBagClient *pbc,
					    const char *propname,
					    PropUtilFieldType field)
{
	CORBA_any *any;
	char      *str;

	g_return_val_if_fail (pbc != NULL, NULL);
	g_return_val_if_fail (GNOME_IS_PROPERTY_BAG_CLIENT (pbc), NULL);
	g_return_val_if_fail (propname != NULL, NULL);

	any = gnome_property_bag_client_get_field_any (pbc, propname, field);

	if (any == NULL)
		return NULL;

	g_return_val_if_fail (any->_type->kind == CORBA_tk_string, NULL);

	str = g_strdup (*(char **) any->_value);

	CORBA_any__free (any, NULL, TRUE);

	return str;
}

/*
 * Querying property values.
 */
gboolean
gnome_property_bag_client_get_value_boolean (GnomePropertyBagClient *pbc,
					     const char *propname)
{
	return gnome_property_bag_client_get_field_boolean (pbc, propname, FIELD_VALUE);
}

gshort
gnome_property_bag_client_get_value_short (GnomePropertyBagClient *pbc,
					   const char *propname)
{
	return gnome_property_bag_client_get_field_short (pbc, propname, FIELD_VALUE);
}

gushort
gnome_property_bag_client_get_value_ushort (GnomePropertyBagClient *pbc,
					    const char *propname)
{
	return gnome_property_bag_client_get_field_ushort (pbc, propname, FIELD_VALUE);
}

glong
gnome_property_bag_client_get_value_long (GnomePropertyBagClient *pbc,
					  const char *propname)
{
	return gnome_property_bag_client_get_field_long (pbc, propname, FIELD_VALUE);
}

gulong
gnome_property_bag_client_get_value_ulong (GnomePropertyBagClient *pbc,
					   const char *propname)
{
	return gnome_property_bag_client_get_field_ulong (pbc, propname, FIELD_VALUE);
}

gfloat
gnome_property_bag_client_get_value_float (GnomePropertyBagClient *pbc,
					   const char *propname)
{
	return gnome_property_bag_client_get_field_float (pbc, propname, FIELD_VALUE);
}

gdouble
gnome_property_bag_client_get_value_double (GnomePropertyBagClient *pbc,
					    const char *propname)
{
	return gnome_property_bag_client_get_field_double (pbc, propname, FIELD_VALUE);
}

char *
gnome_property_bag_client_get_value_string (GnomePropertyBagClient *pbc,
					    const char *propname)
{
	return gnome_property_bag_client_get_field_string (pbc, propname, FIELD_VALUE);
}

CORBA_any *
gnome_property_bag_client_get_value_any (GnomePropertyBagClient *pbc,
					 const char *propname)
{
	return gnome_property_bag_client_get_field_any (pbc, propname, FIELD_VALUE);
}


/*
 * Querying property default values.
 */
gboolean
gnome_property_bag_client_get_default_boolean (GnomePropertyBagClient *pbc,
					       const char *propname)
{
	return gnome_property_bag_client_get_field_boolean (pbc, propname, FIELD_DEFAULT);
}

gshort
gnome_property_bag_client_get_default_short (GnomePropertyBagClient *pbc,
					     const char *propname)
{
	return gnome_property_bag_client_get_field_short (pbc, propname, FIELD_DEFAULT);
}

gushort
gnome_property_bag_client_get_default_ushort (GnomePropertyBagClient *pbc,
					      const char *propname)
{
	return gnome_property_bag_client_get_field_ushort (pbc, propname, FIELD_DEFAULT);
}


glong
gnome_property_bag_client_get_default_long (GnomePropertyBagClient *pbc,
					    const char *propname)
{
	return gnome_property_bag_client_get_field_long (pbc, propname, FIELD_DEFAULT);
}

gulong
gnome_property_bag_client_get_default_ulong (GnomePropertyBagClient *pbc,
					     const char *propname)
{
	return gnome_property_bag_client_get_field_ulong (pbc, propname, FIELD_DEFAULT);
}

gfloat
gnome_property_bag_client_get_default_float (GnomePropertyBagClient *pbc,
					     const char *propname)
{
	return gnome_property_bag_client_get_field_float (pbc, propname, FIELD_DEFAULT);
}

gdouble
gnome_property_bag_client_get_default_double (GnomePropertyBagClient *pbc,
					      const char *propname)
{
	return gnome_property_bag_client_get_field_double (pbc, propname, FIELD_DEFAULT);
}

char *
gnome_property_bag_client_get_default_string (GnomePropertyBagClient *pbc,
					      const char *propname)
{
	return gnome_property_bag_client_get_field_string (pbc, propname, FIELD_DEFAULT);
}

CORBA_any *
gnome_property_bag_client_get_default_any (GnomePropertyBagClient *pbc,
					   const char *propname)
{
	return gnome_property_bag_client_get_field_any (pbc, propname, FIELD_DEFAULT);
}

/* Setting property values. */

void
gnome_property_bag_client_set_value_any (GnomePropertyBagClient *pbc,
					 const char *propname,
					 CORBA_any *value)
{
	GNOME_Property    prop;
	CORBA_Environment ev;

	g_return_if_fail (pbc != NULL);
	g_return_if_fail (GNOME_IS_PROPERTY_BAG_CLIENT (pbc));
	g_return_if_fail (propname != NULL);
	g_return_if_fail (value != NULL);

	prop = gnome_property_bag_client_get_property (pbc, propname);
	g_return_if_fail (prop != CORBA_OBJECT_NIL);

	CORBA_exception_init (&ev);

	GNOME_Property_set_value (prop, value, &ev);

	if (ev._major != CORBA_NO_EXCEPTION) {
		g_warning ("gnome_property_bag_client_set_value_any: Exception setting property!");
		CORBA_exception_free (&ev);
		CORBA_exception_init (&ev);
	}

	CORBA_Object_release (prop, &ev);
	CORBA_exception_free (&ev);

	return;
}

void
gnome_property_bag_client_set_value_boolean (GnomePropertyBagClient *pbc,
					     const char *propname,
					     gboolean value)
{
	CORBA_any      *any;

	g_return_if_fail (pbc != NULL);
	g_return_if_fail (GNOME_IS_PROPERTY_BAG_CLIENT (pbc));
	g_return_if_fail (propname != NULL);

	any = gnome_property_marshal_boolean ("boolean", (const gpointer) &value, NULL);
	g_return_if_fail (any != NULL);

	gnome_property_bag_client_set_value_any (pbc, propname, any);

	CORBA_any__free (any, NULL, TRUE);
}

void
gnome_property_bag_client_set_value_short (GnomePropertyBagClient *pbc,
					   const char *propname,
					   gshort value)
{
	CORBA_any      *any;

	g_return_if_fail (pbc != NULL);
	g_return_if_fail (GNOME_IS_PROPERTY_BAG_CLIENT (pbc));
	g_return_if_fail (propname != NULL);

	any = gnome_property_marshal_short ("short", (const gpointer) &value, NULL);
	g_return_if_fail (any != NULL);

	gnome_property_bag_client_set_value_any (pbc, propname, any);

	CORBA_any__free (any, NULL, TRUE);
}

void
gnome_property_bag_client_set_value_ushort (GnomePropertyBagClient *pbc,
					    const char *propname,
					    gushort value)
{
	CORBA_any      *any;

	g_return_if_fail (pbc != NULL);
	g_return_if_fail (GNOME_IS_PROPERTY_BAG_CLIENT (pbc));
	g_return_if_fail (propname != NULL);

	any = gnome_property_marshal_ushort ("ushort", (const gpointer) &value, NULL);
	g_return_if_fail (any != NULL);

	gnome_property_bag_client_set_value_any (pbc, propname, any);

	CORBA_any__free (any, NULL, TRUE);
}

void
gnome_property_bag_client_set_value_long (GnomePropertyBagClient *pbc,
					  const char *propname,
					  glong value)
{
	CORBA_any      *any;

	g_return_if_fail (pbc != NULL);
	g_return_if_fail (GNOME_IS_PROPERTY_BAG_CLIENT (pbc));
	g_return_if_fail (propname != NULL);

	any = gnome_property_marshal_long ("long", (const gpointer) &value, NULL);
	g_return_if_fail (any != NULL);

	gnome_property_bag_client_set_value_any (pbc, propname, any);

	CORBA_any__free (any, NULL, TRUE);
}

void
gnome_property_bag_client_set_value_ulong (GnomePropertyBagClient *pbc,
					   const char *propname,
					   gulong value)
{
	CORBA_any      *any;

	g_return_if_fail (pbc != NULL);
	g_return_if_fail (GNOME_IS_PROPERTY_BAG_CLIENT (pbc));
	g_return_if_fail (propname != NULL);

	any = gnome_property_marshal_ulong ("ulong", (const gpointer) &value, NULL);
	g_return_if_fail (any != NULL);

	gnome_property_bag_client_set_value_any (pbc, propname, any);

	CORBA_any__free (any, NULL, TRUE);
}

void
gnome_property_bag_client_set_value_float (GnomePropertyBagClient *pbc,
					   const char *propname,
					   gfloat value)
{
	CORBA_any      *any;

	g_return_if_fail (pbc != NULL);
	g_return_if_fail (GNOME_IS_PROPERTY_BAG_CLIENT (pbc));
	g_return_if_fail (propname != NULL);

	any = gnome_property_marshal_float ("float", (const gpointer) &value, NULL);
	g_return_if_fail (any != NULL);

	gnome_property_bag_client_set_value_any (pbc, propname, any);

	CORBA_any__free (any, NULL, TRUE);
}

void
gnome_property_bag_client_set_value_double (GnomePropertyBagClient *pbc,
					    const char *propname,
					    gdouble value)
{
	CORBA_any      *any;

	g_return_if_fail (pbc != NULL);
	g_return_if_fail (GNOME_IS_PROPERTY_BAG_CLIENT (pbc));
	g_return_if_fail (propname != NULL);

	any = gnome_property_marshal_double ("double", (const gpointer) &value, NULL);
	g_return_if_fail (any != NULL);

	gnome_property_bag_client_set_value_any (pbc, propname, any);

	CORBA_any__free (any, NULL, TRUE);
}

void
gnome_property_bag_client_set_value_string (GnomePropertyBagClient *pbc,
					    const char *propname,
					    char *value)
{
	CORBA_any      *any;

	g_return_if_fail (pbc != NULL);
	g_return_if_fail (GNOME_IS_PROPERTY_BAG_CLIENT (pbc));
	g_return_if_fail (propname != NULL);

	any = gnome_property_marshal_string ("string", (const gpointer) value, NULL);
	g_return_if_fail (any != NULL);

	gnome_property_bag_client_set_value_any (pbc, propname, any);

	CORBA_any__free (any, NULL, TRUE);
}

/*
 * Querying other fields and flags.
 */
char *
gnome_property_bag_client_get_docstring (GnomePropertyBagClient *pbc,
					 const char *propname)
{
	CORBA_Environment  ev;
	GNOME_Property     prop;
	CORBA_char        *docstr;

	g_return_val_if_fail (pbc != NULL, NULL);
	g_return_val_if_fail (GNOME_IS_PROPERTY_BAG_CLIENT (pbc), NULL);
	g_return_val_if_fail (propname != NULL, NULL);

	prop = gnome_property_bag_client_get_property (pbc, propname);
	g_return_val_if_fail (prop != CORBA_OBJECT_NIL, NULL);

	CORBA_exception_init (&ev);

	docstr = GNOME_Property_get_doc_string (prop, &ev);

	if (ev._major != CORBA_NO_EXCEPTION) {
		g_warning ("gnome_property_bag_client_get_doc_string: Exception getting doc string!");

		CORBA_exception_free (&ev);
		CORBA_exception_init (&ev);

		CORBA_Object_release (prop, &ev);
		CORBA_exception_free (&ev);

		return NULL;
	}

	CORBA_exception_free (&ev);

	return (char *) docstr;
}

GnomePropertyFlags
gnome_property_bag_client_get_flags (GnomePropertyBagClient *pbc,
				     const char *propname)
{
	GnomePropertyFlags flags;
	GNOME_Property     prop;
	CORBA_Environment  ev;
	gboolean           is_read_only;

	g_return_val_if_fail (pbc != NULL, 0);
	g_return_val_if_fail (GNOME_IS_PROPERTY_BAG_CLIENT (pbc), 0);
	g_return_val_if_fail (propname != NULL, 0);

	prop = gnome_property_bag_client_get_property (pbc, propname);
	g_return_val_if_fail (prop != CORBA_OBJECT_NIL, 0);

	CORBA_exception_init (&ev);

	is_read_only = GNOME_Property_is_read_only (prop, &ev);
	if (ev._major != CORBA_NO_EXCEPTION)
		goto flags_error;

	flags = (is_read_only    ? GNOME_PROPERTY_READ_ONLY       : 0);

	return flags;

 flags_error:
	CORBA_exception_free (&ev);
	CORBA_exception_init (&ev);

	CORBA_Object_release (prop, &ev);
	CORBA_exception_free (&ev);

	return 0;
}



/*
 * GtkObject crap.
 */

static void
gnome_property_bag_client_class_init (GnomePropertyBagClientClass *class)
{
/*	GtkObjectClass *object_class = (GtkObjectClass *) class; */
}

static void
gnome_property_bag_client_init (GnomePropertyBagClient *pbc)
{
}

/**
 * gnome_property_bag_client_get_type:
 *
 * Returns: The GtkType corresponding to the GnomePropertyBagClient
 * class.
 */
GtkType
gnome_property_bag_client_get_type (void)
{
	static GtkType type = 0;

	if (!type){
		GtkTypeInfo info = {
			"GnomePropertyBagClient",
			sizeof (GnomePropertyBagClient),
			sizeof (GnomePropertyBagClientClass),
			(GtkClassInitFunc) gnome_property_bag_client_class_init,
			(GtkObjectInitFunc) gnome_property_bag_client_init,
			NULL, /* reserved 1 */
			NULL, /* reserved 2 */
			(GtkClassInitFunc) NULL
		};

		type = gtk_type_unique (gtk_object_get_type (), &info);
	}

	return type;
}
