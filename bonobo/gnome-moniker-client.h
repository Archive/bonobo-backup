#ifndef _GNOME_MONIKER_CLIENT_H_
#define _GNOME_MONIKER_CLIENT_H_
#include <bonobo/gnome-moniker.h>
#include <bonobo/gnome-object.h>

CORBA_Object gnome_moniker_find_in_naming_service (const char *name,
						   const char *goad_id);

void         gnome_moniker_register   (GnomeMoniker *moniker,
				       const char   *name,
				       const char   *goad_id);
void         gnome_moniker_unregister (GnomeMoniker *moniker);

#endif
