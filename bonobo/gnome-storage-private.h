/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
#ifndef _GNOME_STORAGE_PRIVATE_H_
#define _GNOME_STORAGE_PRIVATE_H_

typedef struct {
	
} StorageDriver;

struct _GnomeStoragePrivate {
	StorageDriver driver;
};

#endif /* _GNOME_STORAGE_PRIVATE_H_ */

