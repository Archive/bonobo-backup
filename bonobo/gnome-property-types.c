/*
 * Gnome Property type functions.
 *
 * This file defines the standard types for Bonobo properties.
 *
 * Author:
 *    Nat Friedman (nat@nat.org)
 *
 * Copyright 1999, Helix Code, Inc.
 */
#include <config.h>
#include <bonobo/gnome-property-types.h>

/*
 * Gross macros to get around ORBit incompleteness.
 *
 * Someone needs a beating.
 */
#define CORBA_short__alloc()	      (CORBA_short *) CORBA_octet_allocbuf (sizeof (CORBA_short))
#define CORBA_unsigned_short__alloc() (CORBA_unsigned_short *) CORBA_octet_allocbuf (sizeof (CORBA_unsigned_short))
#define CORBA_long__alloc()	      (CORBA_long *) CORBA_octet_allocbuf (sizeof (CORBA_long))
#define CORBA_unsigned_long__alloc()  (CORBA_unsigned_long *) CORBA_octet_allocbuf (sizeof (CORBA_unsigned_long))
#define CORBA_string__alloc()	      (CORBA_char **)ORBit_alloc (sizeof (gpointer), CORBA_string__free, GUINT_TO_POINTER (1)) 
#define CORBA_float__alloc()	      (CORBA_float *) CORBA_octet_allocbuf (sizeof (CORBA_float))
#define CORBA_double__alloc()	      (CORBA_double *) CORBA_octet_allocbuf (sizeof (CORBA_double))
#define CORBA_boolean__alloc()	      (CORBA_boolean *) CORBA_octet_allocbuf (sizeof (CORBA_boolean))

/*
 * Standard type marshalers.
 */
CORBA_any *
gnome_property_marshal_boolean (const char *type, const gpointer value,
				gpointer user_data)
{
	CORBA_any *any;
	CORBA_boolean *b;

	g_return_val_if_fail (type != NULL, NULL);
	g_return_val_if_fail (value != NULL, NULL);

	b = CORBA_boolean__alloc ();
	*b = * (CORBA_boolean *) value;
	
	any = CORBA_any__alloc ();
	any->_type = (CORBA_TypeCode) TC_boolean;
	any->_value = b;
	CORBA_any_set_release (any, TRUE);

	return any;
}

CORBA_any *
gnome_property_marshal_string (const char *type, const gpointer value,
			       gpointer user_data)
{
	CORBA_any *any;
	CORBA_char **str;
	const char *string_value;

	if (value == NULL)
		string_value = "";
	else
		string_value = (const char *) value;

	str = CORBA_string__alloc ();
	*str = CORBA_string_dup (string_value);
	
	any = CORBA_any__alloc ();
	any->_type = (CORBA_TypeCode) TC_string;
	any->_value = str;
	CORBA_any_set_release (any, TRUE);

	return any;
}

CORBA_any *
gnome_property_marshal_short (const char *type, const gpointer value,
			      gpointer user_data)
{
	CORBA_any *any;
	CORBA_short *s;

	g_return_val_if_fail (type != NULL, NULL);
	g_return_val_if_fail (value != NULL, NULL);

	s = CORBA_short__alloc ();
	*s = * ( (CORBA_short *)value);

	any = CORBA_any__alloc ();
	any->_type = (CORBA_TypeCode) TC_short;
	any->_value = s;

	return any;
}

CORBA_any *
gnome_property_marshal_ushort (const char *type, const gpointer value,
			       gpointer user_data)
{
	CORBA_any *any;
	CORBA_unsigned_short *s;

	g_return_val_if_fail (type != NULL, NULL);
	g_return_val_if_fail (value != NULL, NULL);

	s = CORBA_unsigned_short__alloc ();
	*s = * ( (CORBA_unsigned_short *)value);

	any = CORBA_any__alloc ();
	any->_type = (CORBA_TypeCode) TC_ushort;
	any->_value = s;
	CORBA_any_set_release (any, TRUE);

	return any;
}

CORBA_any *
gnome_property_marshal_long (const char *type, const gpointer value,
			     gpointer user_data)
{
	CORBA_any *any;
	CORBA_long *l;

	g_return_val_if_fail (type != NULL, NULL);
	g_return_val_if_fail (value != NULL, NULL);

	l = CORBA_long__alloc ();
	*l = * ( (CORBA_long *) value);

	any = CORBA_any__alloc ();
	any->_type = (CORBA_TypeCode) TC_long;
	any->_value = l;
	CORBA_any_set_release (any, TRUE);

	return any;
}

CORBA_any *
gnome_property_marshal_ulong (const char *type, const gpointer value,
			      gpointer user_data)
{
	CORBA_any *any;
	CORBA_unsigned_long *l;

	g_return_val_if_fail (type != NULL, NULL);
	g_return_val_if_fail (value != NULL, NULL);

	l = CORBA_unsigned_long__alloc ();
	*l = * ( (CORBA_unsigned_long *) value);

	any = CORBA_any__alloc ();
	any->_type = (CORBA_TypeCode) TC_ulong;
	any->_value = l;
	CORBA_any_set_release (any, TRUE);

	return any;
}

CORBA_any *
gnome_property_marshal_float (const char *type, const gpointer value,
			      gpointer user_data)
{
	CORBA_any *any;
	CORBA_float *f;

	g_return_val_if_fail (type != NULL, NULL);
	g_return_val_if_fail (value != NULL, NULL);

	f = CORBA_float__alloc ();
	*f = * ( (CORBA_float *) value);

	any = CORBA_any__alloc ();
	any->_type = (CORBA_TypeCode) TC_float;
	any->_value = f;
	CORBA_any_set_release (any, TRUE);

	return any;
}

CORBA_any *
gnome_property_marshal_double (const char *type, const gpointer value,
			       gpointer user_data)
{
	CORBA_any *any;
	CORBA_double *d;

	g_return_val_if_fail (type != NULL, NULL);
	g_return_val_if_fail (value != NULL, NULL);

	d = CORBA_double__alloc ();
	*d = * ( (CORBA_double *) value);

	any = CORBA_any__alloc ();
	any->_type = (CORBA_TypeCode) TC_double;
	any->_value = d;
	CORBA_any_set_release (any, TRUE);

	return any;
}

/*
 * Standard type demarshalers.
 */
gpointer
gnome_property_demarshal_boolean (const char *type, const CORBA_any *any,
				  gpointer user_data)
{
	CORBA_boolean *b;

	g_return_val_if_fail (type != NULL, NULL);
	g_return_val_if_fail (any != NULL, NULL);
	g_return_val_if_fail (any->_type->kind == CORBA_tk_boolean, NULL);

	b = g_new0 (CORBA_boolean, 1);
	*b = * ( (CORBA_boolean *) any->_value);

	return (gpointer) b;
}

gpointer
gnome_property_demarshal_string (const char *type, const CORBA_any *any,
				 gpointer user_data)
{
	char *s;

	g_return_val_if_fail (type != NULL, NULL);
	g_return_val_if_fail (any != NULL, NULL);
	g_return_val_if_fail (any->_type->kind == CORBA_tk_string, NULL);

	s = g_strdup (*(CORBA_char **)any->_value);

	return (gpointer) s;
}

gpointer
gnome_property_demarshal_short (const char *type, const CORBA_any *any,
				gpointer user_data)
{
	CORBA_short *s;

	g_return_val_if_fail (type != NULL, NULL);
	g_return_val_if_fail (any != NULL, NULL);
	g_return_val_if_fail (any->_type->kind == CORBA_tk_short, NULL);


	s = g_new0 (CORBA_short, 1);
	*s = * ( (CORBA_short *)any->_value);

	return (gpointer) s;
}

gpointer
gnome_property_demarshal_ushort (const char *type, const CORBA_any *any,
				 gpointer user_data)
{
	CORBA_unsigned_short *s;

	g_return_val_if_fail (type != NULL, NULL);
	g_return_val_if_fail (any != NULL, NULL);
	g_return_val_if_fail (any->_type->kind == CORBA_tk_ushort, NULL);

	s = g_new0 (CORBA_unsigned_short, 1);
	*s = * ( (CORBA_unsigned_short *)any->_value);

	return (gpointer) s;
}

gpointer
gnome_property_demarshal_long (const char *type, const CORBA_any *any,
			       gpointer user_data)
{
	CORBA_long *l;

	g_return_val_if_fail (type != NULL, NULL);
	g_return_val_if_fail (any != NULL, NULL);
	g_return_val_if_fail (any->_type->kind == CORBA_tk_long, NULL);

	l = g_new0 (CORBA_long, 1);
	*l = * ( (CORBA_long *)any->_value);

	return (gpointer) l;
}

gpointer
gnome_property_demarshal_ulong (const char *type, const CORBA_any *any,
				gpointer user_data)
{
	CORBA_unsigned_long *l;

	g_return_val_if_fail (type != NULL, NULL);
	g_return_val_if_fail (any != NULL, NULL);
	g_return_val_if_fail (any->_type->kind == CORBA_tk_ulong, NULL);

	l = g_new0 (CORBA_unsigned_long, 1);
	*l = * ( (CORBA_unsigned_long *)any->_value);

	return (gpointer) l;
}

gpointer
gnome_property_demarshal_float (const char *type, const CORBA_any *any,
				gpointer user_data)
{
	CORBA_float *f;

	g_return_val_if_fail (type != NULL, NULL);
	g_return_val_if_fail (any != NULL, NULL);
	g_return_val_if_fail (any->_type->kind == CORBA_tk_float, NULL);

	f = g_new0 (CORBA_float, 1);
	*f = * ( (CORBA_float *)any->_value);

	return (gpointer) f;
}

gpointer
gnome_property_demarshal_double (const char *type, const CORBA_any *any,
				 gpointer user_data)
{
	CORBA_double *d;

	g_return_val_if_fail (type != NULL, NULL);
	g_return_val_if_fail (any != NULL, NULL);
	g_return_val_if_fail (any->_type->kind == CORBA_tk_double, NULL);

	d = g_new0 (CORBA_double, 1);
	*d = * ( (CORBA_double *)any->_value);

	return (gpointer) d;
}


/*
 * Standard type comparers.
 */
gboolean
gnome_property_compare_boolean (const char *type, gpointer value1,
				gpointer value2, gpointer user_data)
{
	gboolean *b1 = value1;
	gboolean *b2 = value2;

	g_return_val_if_fail (value1 != NULL, FALSE);
	g_return_val_if_fail (value2 != NULL, FALSE);

	return (*b1 == *b2);
}

gboolean
gnome_property_compare_string (const char *type, gpointer value1,
			       gpointer value2, gpointer user_data)
{
	char *s1 = value1;
	char *s2 = value2;

	g_return_val_if_fail (value1 != NULL, FALSE);
	g_return_val_if_fail (value2 != NULL, FALSE);

	return (! strcmp (s1, s2));
}

gboolean
gnome_property_compare_short (const char *type, gpointer value1,
			       gpointer value2, gpointer user_data)
{
	gshort *s1 = value1;
	gshort *s2 = value2;

	g_return_val_if_fail (value1 != NULL, FALSE);
	g_return_val_if_fail (value2 != NULL, FALSE);

	return (*s1 == *s2);
}

gboolean
gnome_property_compare_ushort (const char *type, gpointer value1,
			       gpointer value2, gpointer user_data)
{
	gushort *s1 = value1;
	gushort *s2 = value2;

	g_return_val_if_fail (value1 != NULL, FALSE);
	g_return_val_if_fail (value2 != NULL, FALSE);

	return (*s1 == *s2);
}

gboolean
gnome_property_compare_long (const char *type, gpointer value1,
			     gpointer value2, gpointer user_data)
{
	glong *l1 = value1;
	glong *l2 = value2;

	g_return_val_if_fail (value1 != NULL, FALSE);
	g_return_val_if_fail (value2 != NULL, FALSE);

	return (*l1 == *l2);
}

gboolean
gnome_property_compare_ulong (const char *type, gpointer value1,
			      gpointer value2, gpointer user_data)
{
	gulong *l1 = value1;
	gulong *l2 = value2;

	g_return_val_if_fail (value1 != NULL, FALSE);
	g_return_val_if_fail (value2 != NULL, FALSE);

	return (*l1 == *l2);
}

/*
 * Floating point comparison is extremely stupid and never works,
 * so we just say that they're never equal.
 */
gboolean
gnome_property_compare_float (const char *type, gpointer value1,
			      gpointer value2, gpointer user_data)
{
	return FALSE;
}

gboolean
gnome_property_compare_double (const char *type, gpointer value1,
			       gpointer value2, gpointer user_data)
{
	return FALSE;
}

/*
 * Standard type releaser.
 */
void
gnome_property_generic_releaser (const char *type, gpointer value,
				 gpointer user_data)
{
	g_free (value);
}
