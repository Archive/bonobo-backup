/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/**
 * GNOME view object
 *
 * Authors:
 *   Miguel de Icaza (miguel@kernel.org)
 *   Nat Friedman    (nat@nat.org)
 *
 * Copyright 1999 Helix Code, Inc.
 */
#include <config.h>
#include <gtk/gtksignal.h>
#include <gtk/gtkmarshal.h>
#include <gtk/gtkplug.h>
#include <bonobo/gnome-main.h>
#include <bonobo/gnome-view.h>
#include <gdk/gdkprivate.h>

/* Parent object class in GTK hierarchy */
static GnomeControlClass *gnome_view_parent_class;

/* The entry point vectors for the server we provide */
POA_GNOME_View__epv    gnome_view_epv;
POA_GNOME_Control__epv gnome_view_overridden_control_epv;
POA_GNOME_View__vepv   gnome_view_vepv;

enum {
	VIEW_UNDO_LAST_OPERATION,
	DO_VERB,
	SET_ZOOM_FACTOR,
	LAST_SIGNAL
};

static guint view_signals [LAST_SIGNAL];

typedef void (*GnomeSignal_NONE__DOUBLE) (GtkObject *object, double arg1, gpointer user_data);

struct _GnomeViewPrivate {
	GHashTable *verb_callbacks;
	GHashTable *verb_callback_closures;
};

static void
impl_GNOME_View_do_verb (PortableServer_Servant servant,
			 const CORBA_char      *verb_name,
			 CORBA_Environment     *ev)
{
	GnomeView *view = GNOME_VIEW (gnome_object_from_servant (servant));

	gnome_view_execute_verb (view, verb_name);
}

static void
impl_GNOME_View_set_zoom_factor (PortableServer_Servant servant,
				 const CORBA_double zoom,
				 CORBA_Environment *ev)
{
	GnomeView *view = GNOME_VIEW (gnome_object_from_servant (servant));

	gtk_signal_emit (GTK_OBJECT (view),
			 view_signals [SET_ZOOM_FACTOR], zoom);
}

/**
 * gnome_view_corba_object_create:
 * @object: the GtkObject that will wrap the CORBA object
 *
 * Creates and activates the CORBA object that is wrapped by the
 * @object GnomeObject.
 *
 * Returns: An activated object reference to the created object
 * or %CORBA_OBJECT_NIL in case of failure.
 */
GNOME_View
gnome_view_corba_object_create (GnomeObject *object)
{
	POA_GNOME_View *servant;
	CORBA_Environment ev;
	
	servant = (POA_GNOME_View *) g_new0 (GnomeObjectServant, 1);
	servant->vepv = &gnome_view_vepv;

	CORBA_exception_init (&ev);
	POA_GNOME_View__init ((PortableServer_Servant) servant, &ev);
	if (ev._major != CORBA_NO_EXCEPTION){

		g_free (servant);
		CORBA_exception_free (&ev);
		return CORBA_OBJECT_NIL;
	}

	CORBA_exception_free (&ev);
	return (GNOME_View) gnome_object_activate_servant (object, servant);
}

/**
 * gnome_view_activate:
 * @view: 
 * @activate: 
 * 
 *   A basic default handler so we can at least
 * get focus in a development component; 95% of these
 * will override this to merge menus for them.
 **/
static void
gnome_view_activate (GnomeView *view, gboolean activate, gpointer user_data)
{
	gnome_view_activate_notify (view, activate);
}

/**
 * gnome_view_construct:
 * @view: The GnomeView object to be initialized.
 * @corba_view: The CORBA GNOME_View interface for the new GnomeView object.
 * @widget: A GtkWidget contains the view and * will be passed to the container
 * process's ViewFrame object.
 * 
 * @item_creator might be NULL for widget-based views.
 *
 * Returns: the intialized GnomeView object.
 */
GnomeView *
gnome_view_construct (GnomeView *view, GNOME_View corba_view, GtkWidget *widget)
{
	g_return_val_if_fail (view != NULL, NULL);
	g_return_val_if_fail (GNOME_IS_VIEW (view), NULL);
	g_return_val_if_fail (corba_view != CORBA_OBJECT_NIL, NULL);
	g_return_val_if_fail (widget != NULL, NULL);
	g_return_val_if_fail (GTK_IS_WIDGET (widget), NULL);
	
	gnome_control_construct (GNOME_CONTROL (view), corba_view, widget);

	view->priv->verb_callbacks = g_hash_table_new (g_str_hash, g_str_equal);
	view->priv->verb_callback_closures = g_hash_table_new (g_str_hash, g_str_equal);

/*	gtk_signal_connect (GTK_OBJECT (view), "view_activate",
			    GTK_SIGNAL_FUNC (gnome_view_activate),
			    NULL);*/
	return view;
}

/**
 * gnome_view_new:
 * @widget: a GTK widget that contains the view and will be passed to the
 * container process.
 *
 * This function creates a new GnomeView object for @widget
 *
 * Returns: a GnomeView object that implements the GNOME::View CORBA
 * service that will transfer the @widget to the container process.
 */
GnomeView *
gnome_view_new (GtkWidget *widget)
{
	GnomeView *view;
	GNOME_View corba_view;
	
	g_return_val_if_fail (widget != NULL, NULL);
	g_return_val_if_fail (GTK_IS_WIDGET (widget), NULL);

	view = gtk_type_new (gnome_view_get_type ());

	corba_view = gnome_view_corba_object_create (GNOME_OBJECT (view));
	if (corba_view == CORBA_OBJECT_NIL) {
		gtk_object_destroy (GTK_OBJECT (view));
		return NULL;
	}
	
	return gnome_view_construct (view, corba_view, widget);
}

static gboolean
gnome_view_destroy_remove_verb (gpointer key, gpointer value,
				gpointer user_data)
{
	g_free (key);

	return TRUE;
}

static void
gnome_view_destroy (GtkObject *object)
{
	GnomeView *view;

	g_return_if_fail (object != NULL);
	g_return_if_fail (GNOME_IS_VIEW (object));
	
	view = GNOME_VIEW (object);

	/*
	 * Free up all the verbs associated with this View.
	 */
	g_hash_table_foreach_remove (view->priv->verb_callbacks,
				     gnome_view_destroy_remove_verb, NULL);
	g_hash_table_destroy (view->priv->verb_callbacks);

	g_hash_table_destroy (view->priv->verb_callback_closures);

	g_free (view->priv);
	
	GTK_OBJECT_CLASS (gnome_view_parent_class)->destroy (object);
}

/**
 * gnome_view_get_epv:
 */
POA_GNOME_View__epv *
gnome_view_get_epv (void)
{
	POA_GNOME_View__epv *epv;

	epv = g_new0 (POA_GNOME_View__epv, 1);

	epv->do_verb	     = impl_GNOME_View_do_verb;
	epv->set_zoom_factor = impl_GNOME_View_set_zoom_factor;

	return epv;
}

static void
init_view_corba_class (void)
{
	gnome_view_vepv.GNOME_Unknown_epv = gnome_object_get_epv ();
	gnome_view_vepv.GNOME_Control_epv = gnome_control_get_epv ();
	gnome_view_vepv.GNOME_View_epv	  = gnome_view_get_epv ();
}

static void 
gnome_marshal_NONE__DOUBLE (GtkObject * object,
			    GtkSignalFunc func,
			    gpointer func_data,
			    GtkArg * args)
{
	GnomeSignal_NONE__DOUBLE rfunc;
	rfunc = (GnomeSignal_NONE__DOUBLE) func;
	(*rfunc) (object,
		  GTK_VALUE_DOUBLE (args[0]),
		  func_data);
}

static void
gnome_view_class_init (GnomeViewClass *klass)
{
	GtkObjectClass *object_class = (GtkObjectClass *) klass;

	gnome_view_parent_class = gtk_type_class (gnome_control_get_type ());

	view_signals [DO_VERB] =
                gtk_signal_new ("do_verb",
                                GTK_RUN_LAST,
                                object_class->type,
                                GTK_SIGNAL_OFFSET (GnomeViewClass, do_verb), 
                                gtk_marshal_NONE__POINTER,
                                GTK_TYPE_NONE, 1,
				GTK_TYPE_STRING);

	view_signals [SET_ZOOM_FACTOR] =
                gtk_signal_new ("set_zoom_factor",
                                GTK_RUN_LAST,
                                object_class->type,
                                GTK_SIGNAL_OFFSET (GnomeViewClass, set_zoom_factor), 
                                gnome_marshal_NONE__DOUBLE,
                                GTK_TYPE_NONE, 1,
				GTK_TYPE_DOUBLE);

	gtk_object_class_add_signals (object_class, view_signals, LAST_SIGNAL);

	object_class->destroy = gnome_view_destroy;

	init_view_corba_class ();
}

static void
gnome_view_init (GnomeView *view)
{
	view->priv = g_new0 (GnomeViewPrivate, 1);
}

/**
 * gnome_view_get_type:
 *
 * Returns: The GtkType corresponding to the GnomeView class.
 */
GtkType
gnome_view_get_type (void)
{
	static GtkType type = 0;

	if (!type){
		GtkTypeInfo info = {
			"GNOMEView",
			sizeof (GnomeView),
			sizeof (GnomeViewClass),
			(GtkClassInitFunc) gnome_view_class_init,
			(GtkObjectInitFunc) gnome_view_init,
			NULL, /* reserved 1 */
			NULL, /* reserved 2 */
			(GtkClassInitFunc) NULL
		};

		type = gtk_type_unique (gnome_control_get_type (), &info);
	}

	return type;
}

/**
 * gnome_view_set_embeddable:
 * @view: A GnomeView object.
 * @embeddable: The GnomeEmbeddable object for which @view is a view.
 *
 * This function associates @view with the specified GnomeEmbeddabe
 * object, @embeddable.
 */
void
gnome_view_set_embeddable (GnomeView *view, GnomeEmbeddable *embeddable)
{
	g_return_if_fail (view != NULL);
	g_return_if_fail (GNOME_IS_VIEW (view));
	g_return_if_fail (embeddable != NULL);
	g_return_if_fail (GNOME_IS_EMBEDDABLE (embeddable));

	if (view->embeddable != NULL)
		gtk_object_unref (GTK_OBJECT (view->embeddable));

	view->embeddable = embeddable;
	gtk_object_ref (GTK_OBJECT (view->embeddable));
}

/**
 * gnome_view_get_embeddable:
 * @view: A GnomeView object.
 *
 * Returns: The GnomeEmbeddable object for which @view is a GnomeView.
 */
GnomeEmbeddable *
gnome_view_get_embeddable (GnomeView *view)
{
	g_return_val_if_fail (view != NULL, NULL);
	g_return_val_if_fail (GNOME_IS_VIEW (view), NULL);

	return view->embeddable;
}

/**
 * gnome_view_set_view_frame:
 * @view: A GnomeView object.
 * @view_frame: A CORBA interface for the ViewFrame which contains this View.
 *
 * Sets the ViewFrame for @view to @view_frame.
 */
void
gnome_view_set_view_frame (GnomeView *view, GNOME_ViewFrame view_frame)
{
	g_return_if_fail (view != NULL);
	g_return_if_fail (GNOME_IS_VIEW (view));
	
	gnome_control_set_control_frame (GNOME_CONTROL (view), (GNOME_ControlFrame) view_frame);

	view->view_frame = view_frame;
}

/**
 * gnome_view_get_view_frame:
 * @view: A GnomeView object whose GNOME_ViewFrame CORBA interface is
 * being retrieved.
 *
 * Returns: The GNOME_ViewFrame CORBA object associated with @view, this is
 * a CORBA_object_duplicated object.  You need to CORBA_free it when you are
 * done with it.
 */
GNOME_ViewFrame
gnome_view_get_view_frame (GnomeView *view)

{
	g_return_val_if_fail (view != NULL, CORBA_OBJECT_NIL);
	g_return_val_if_fail (GNOME_IS_VIEW (view), CORBA_OBJECT_NIL);
	
	return view->view_frame;
}

/**
 * gnome_view_set_ui_handler:
 * @view: A GnomeView object.
 * @uih: A GnomeUIHandler object.
 *
 * Sets the GnomeUIHandler for @view to @uih.  This provides a
 * convenient way for a component to store the GnomeUIHandler which it
 * will use to merge menus and toolbars.
 */
void
gnome_view_set_ui_handler (GnomeView *view, GnomeUIHandler *uih)
{
	g_return_if_fail (view != NULL);
	g_return_if_fail (GNOME_IS_VIEW (view));

	gnome_control_set_ui_handler (GNOME_CONTROL (view), uih);
}

/**
 * gnome_view_get_ui_handler:
 * @view: A GnomeView object for which a GnomeUIHandler has been created and set.
 *
 * Returns: The GnomeUIHandler which was associated with @view using
 * gnome_view_set_ui_handler().
 */
GnomeUIHandler *
gnome_view_get_ui_handler (GnomeView *view)
{
	g_return_val_if_fail (view != NULL, NULL);
	g_return_val_if_fail (GNOME_IS_VIEW (view), NULL);


	return gnome_control_get_ui_handler (GNOME_CONTROL (view));
}

/**
 * gnome_view_get_remote_ui_handler:
 * @view: A GnomeView object which is bound to a remote GnomeViewFrame.
 *
 * Returns: The GNOME_UIHandler CORBA server for the remote GnomeViewFrame.
 */
GNOME_UIHandler
gnome_view_get_remote_ui_handler (GnomeView *view)
{
	g_return_val_if_fail (view != NULL, CORBA_OBJECT_NIL);
	g_return_val_if_fail (GNOME_IS_VIEW (view), CORBA_OBJECT_NIL);

	return gnome_control_get_remote_ui_handler (GNOME_CONTROL (view));
}

/**
 * gnome_view_activate_notify:
 * @view: A GnomeView object which is bound to a remote GnomeViewFrame..
 * @activate: %TRUE if the view is activated, %FALSE otherwise.
 *
 * This function notifies @view's remote ViewFrame that the activation
 * state of @view has changed.
 */
void
gnome_view_activate_notify (GnomeView *view, gboolean activated)
{
	g_return_if_fail (view != NULL);
	g_return_if_fail (GNOME_IS_VIEW (view));

	gnome_control_activate_notify (GNOME_CONTROL (view), activated);
}


/**
 * gnome_view_register_verb:
 * @view: A GnomeView object.
 * @verb_name: The name of the verb to register.
 * @callback: A function to call when @verb_name is executed on @view.
 * @user_data: A closure to pass to @callback when it is invoked.
 *
 * Registers a verb called @verb_name against @view.  When @verb_name
 * is executed, the View will dispatch to @callback.
 */
void
gnome_view_register_verb (GnomeView *view, const char *verb_name,
			  GnomeViewVerbFunc callback, gpointer user_data)
{
	char *key;

	g_return_if_fail (view != NULL);
	g_return_if_fail (GNOME_IS_VIEW (view));
	g_return_if_fail (verb_name != NULL);

	key = g_strdup (verb_name);

	g_hash_table_insert (view->priv->verb_callbacks, key, callback);
	g_hash_table_insert (view->priv->verb_callback_closures, key, user_data);
}

/**
 * gnome_view_unregister_verb:
 * @view: A GnomeView object.
 * @verb_name: The name of a verb to be unregistered.
 *
 * Unregisters the verb called @verb_name from @view.
 */
void
gnome_view_unregister_verb (GnomeView *view, const char *verb_name)
{
	gchar *original_key;

	g_return_if_fail (view != NULL);
	g_return_if_fail (GNOME_IS_VIEW (view));
	g_return_if_fail (verb_name != NULL);

	if (! g_hash_table_lookup_extended (view->priv->verb_callbacks, verb_name,
					    (gpointer *) &original_key, NULL))
		return;
	g_hash_table_remove (view->priv->verb_callbacks, verb_name);
	g_hash_table_remove (view->priv->verb_callback_closures, verb_name);
	g_free (original_key);
}

/**
 * gnome_view_execute_verb:
 * @view: A GnomeView object.
 * @verb_name: The name of the verb to execute on @view.
 *
 * Executes the verb specified by @verb_name on @view, emitting a
 * "do_verb" signal and calling the registered verb callback, if one
 * exists for @verb_name on @view.
 */
void
gnome_view_execute_verb (GnomeView *view, const char *verb_name)
{
	GnomeViewVerbFunc callback;

	/*
	 * Always emit a signal when a verb is executed.
	 */
	gtk_signal_emit (
		GTK_OBJECT (view),
		view_signals [DO_VERB],
		(gchar *) verb_name);

	/*
	 * The user may have registered a callback for this particular
	 * verb.  If so, dispatch to that callback.
	 */
	callback = g_hash_table_lookup (view->priv->verb_callbacks, verb_name);
	if (callback != NULL) {
		void *user_data;

		user_data = g_hash_table_lookup (view->priv->verb_callback_closures, verb_name);

		(*callback) (view, (const char *) verb_name, user_data);
	}
}

static void
gnome_view_verb_selected_cb (GnomeUIHandler *uih, void *user_data,
			     const char *path)
{
	GnomeView  *view = GNOME_VIEW (user_data);
	const char *verb_name;

	g_assert (path != NULL);

	/*
	 * Extract the verb name from the selected verb.
	 */
	verb_name = path + 1;

	/*
	 * Execute it.
	 */
	gnome_view_execute_verb (view, verb_name);

	/*
	 * Store the verb name.
	 */
	gtk_object_set_data (GTK_OBJECT (view), "view_executed_verb_name",
			     g_strdup (verb_name));
	
}

/**
 * gnome_view_popup_verbs:
 * @view: A GnomeView object.
 *
 * Creates a popup menu, filling it with the list of verbs supported
 * by @view.  If a verb is selected, it is executed on @view.  Returns
 * a newly-allocated string containing the name of the selected verb,
 * or %NULL if no verb is selected.
 */
char *
gnome_view_popup_verbs (GnomeView *view)
{
	GnomeUIHandler *popup;
	const GList *verbs;
	const GList *l;
	char *verb;

	g_return_val_if_fail (view != NULL, NULL);
	g_return_val_if_fail (GNOME_IS_VIEW (view), NULL);
	g_return_val_if_fail (view->embeddable != NULL, NULL);

	/*
	 * Get a list of the available verbs from our embeddable.
	 */
	verbs = gnome_embeddable_get_verbs (view->embeddable);

	/*
	 * Build the menu.
	 */
	popup = gnome_ui_handler_new ();
	gnome_ui_handler_create_popup_menu (popup);

	for (l = verbs; l != NULL; l = l->next) {
		const GnomeVerb *verb = (GnomeVerb *) l->data;
		char *path;

		path = g_strconcat ("/", verb->name, NULL);
		gnome_ui_handler_menu_new_item (popup, path,
						verb->label, verb->hint,
						-1,
						GNOME_UI_HANDLER_PIXMAP_NONE, NULL,
						0, (GdkModifierType) 0,
						gnome_view_verb_selected_cb,
						view);

		g_free (path);
	}

	/*
	 * Pop up the menu.
	 */
	gnome_ui_handler_do_popup_menu (popup);

	/*
	 * Destroy it.
	 */
	gnome_object_unref (GNOME_OBJECT (popup));

	/*
	 * Grab the name of the executed verb.
	 */
	verb = gtk_object_get_data (GTK_OBJECT (view), "view_executed_verb_name");
	gtk_object_remove_data (GTK_OBJECT (view), "view_executed_verb_name");

	return verb;
}
