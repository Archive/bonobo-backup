/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/**
 * GNOME bind context.
 *
 * A binding context is used to keep track of temporary objects
 * used during the creation of another object through monikers.
 *
 * It is used as a place holder.  Every item is refed when added
 * and unrefed when removed. 
 *
 * Author:
 *   Miguel de Icaza (miguel@kernel.org)
 */
#include <config.h>
#include <gtk/gtksignal.h>
#include <gtk/gtkmarshal.h>
#include <bonobo/bonobo.h>
#include <bonobo/gnome-bind-context.h>

static GnomeObjectClass *gnome_bind_context_parent_class;

static POA_GNOME_BindContext__epv gnome_bind_context_epv;
static POA_GNOME_BindContext__vepv gnome_bind_context_vepv;

static CORBA_Object
create_gnome_bind_context (GnomeObject *object)
{
	POA_GNOME_BindContext *servant;
	CORBA_Environment ev;

	servant = (POA_GNOME_BindContext *)g_new0 (GnomeObjectServant, 1);
	servant->vepv = &gnome_bind_context_vepv;

	CORBA_exception_init (&ev);

	POA_GNOME_BindContext__init ((PortableServer_Servant) servant, &ev);
	if (ev._major != CORBA_NO_EXCEPTION){
		CORBA_exception_free (&ev);
		g_free (servant);
		return CORBA_OBJECT_NIL;
	}

	CORBA_exception_free (&ev);

	return gnome_object_activate_servant (object, servant);
	
}

GnomeBindContext *
gnome_bind_context_construct (GnomeBindContext *bindctx,
			      GNOME_BindContext corba_bindctx)
{
	g_return_val_if_fail (bindctx != NULL, NULL);
	g_return_val_if_fail (GNOME_IS_BIND_CONTEXT (bindctx), NULL);
	g_return_val_if_fail (corba_bindctx != CORBA_OBJECT_NIL, NULL);

	gnome_object_construct (GNOME_OBJECT (bindctx), corba_bindctx);
	
	return bindctx;
}

/**
 * gnome_bind_context_new:
 *
 * Returns: A new GnomeBindContext object which can be used to
 * associate a set of objects together during moniker-driven
 * object creation.
 */
GnomeBindContext *
gnome_bind_context_new (void)
{
	GNOME_BindContext corba_bindctx;
	GnomeBindContext *bindctx;
	
	bindctx = gtk_type_new (gnome_bind_context_get_type ());

	corba_bindctx = create_gnome_bind_context (GNOME_OBJECT (bindctx));
	if (corba_bindctx == CORBA_OBJECT_NIL){
		gtk_object_destroy (GTK_OBJECT (bindctx));
		return NULL;
	}
	

	return gnome_bind_context_construct (bindctx, corba_bindctx);
}

static void
gnome_bind_context_destroy (GtkObject *object)
{
	GnomeBindContext *bindctx = GNOME_BIND_CONTEXT (object);

	g_hash_table_destroy (bindctx->table);
	
	GTK_OBJECT_CLASS (gnome_bind_context_parent_class)->destroy (object);
}

static void
impl_register_object (PortableServer_Servant servant,
		      const GNOME_Unknown object,
		      CORBA_Environment *ev)
{
	GnomeObject *this = gnome_object_from_servant (servant);
	GnomeBindContext *bindctx = GNOME_BIND_CONTEXT (this);
	
	if (g_hash_table_lookup (bindctx->table, object) == NULL){
		GNOME_Unknown o = (GNOME_Unknown) gnome_object_corba_objref (GNOME_OBJECT (this));
		CORBA_Environment ev;

		CORBA_exception_init (&ev);

		o = CORBA_Object_duplicate (gnome_object_corba_objref (
			GNOME_OBJECT (this)), &ev);
		g_hash_table_insert (bindctx->table, o, o);
		GNOME_Unknown_ref (o, &ev);

		CORBA_exception_free (&ev);
	}
}

static void
impl_drop_one_object (PortableServer_Servant servant,
		      const GNOME_Unknown object,
		      CORBA_Environment *ev)
{
	GnomeObject *this = gnome_object_from_servant (servant);
	GnomeBindContext *bindctx = GNOME_BIND_CONTEXT (this);
	CORBA_Object objref;

	objref = g_hash_table_lookup (bindctx->table, object);
	if (objref){
		CORBA_Environment ev;

		CORBA_exception_init (&ev);

		g_hash_table_remove (bindctx->table, object);
		GNOME_Unknown_unref (objref, &ev);
		CORBA_Object_release (objref, &ev);

		CORBA_exception_free (&ev);
	}
}

static gboolean
remove_it (gpointer key, gpointer value, gpointer user_data)
{
	GNOME_Unknown o = key;
	CORBA_Environment *ev = (CORBA_Environment *) user_data;

	GNOME_Unknown_unref (o, ev);
	return TRUE;
}

static void
impl_drop_objects (PortableServer_Servant servant,
		   CORBA_Environment *ev)
{
	GnomeObject *this = gnome_object_from_servant (servant);
	GnomeBindContext *bindctx = GNOME_BIND_CONTEXT (this);
	CORBA_Environment ev;

	CORBA_exception_init (&ev);

	g_hash_table_foreach_remove (bindctx->table, remove_it, &ev);

	CORBA_exception_free (&ev);
}

static void
init_bind_context_corba_class (void)
{
	gnome_bind_context_epv.register_object = impl_register_object;
	gnome_bind_context_epv.drop_one_object = impl_drop_one_object;
	gnome_bind_context_epv.drop_objects    = impl_drop_objects;
	
	/* The VEPV */
	gnome_bind_context_vepv.GNOME_Unknown_epv = &gnome_object_epv;
	gnome_bind_context_vepv.GNOME_BindContext_epv = &gnome_bind_context_epv;
}

static void
gnome_bind_context_class_init (GnomeBindContextClass *klass)
{
	GtkObjectClass *object_class = (GtkObjectClass *) klass;

	gnome_bind_context_parent_class = gtk_type_class (gnome_object_get_type ());

	object_class->destroy = gnome_bind_context_destroy;

	init_bind_context_corba_class ();
}

static void
gnome_bind_context_init (GnomeObject *object)
{
	GnomeBindContext *bindctx = GNOME_BIND_CONTEXT (object);

	g_warning ("We should fix the objref hash table here");
	bindctx->table = g_hash_table_new (g_direct_hash, g_direct_equal);
}

/**
 * gnome_bind_context_get_type:
 *
 * Returns: the GtkType associated with the GnomeBindContext class.
 */
GtkType
gnome_bind_context_get_type (void)
{
	static GtkType type = 0;

	if (!type){
		GtkTypeInfo info = {
			"IDL:GNOME/BindContext:1.0",
			sizeof (GnomeBindContext),
			sizeof (GnomeBindContextClass),
			(GtkClassInitFunc) gnome_bind_context_class_init,
			(GtkObjectInitFunc) gnome_bind_context_init,
			NULL, /* reserved 1 */
			NULL, /* reserved 2 */
			(GtkClassInitFunc) NULL
		};

		type = gtk_type_unique (gnome_object_get_type (), &info);
	}

	return type;
}
