/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
#ifndef _GNOME_FILE_MONIKER_H_
#define _GNOME_FILE_MONIKER_H_

#include <bonobo/gnome-moniker.h>

BEGIN_GNOME_DECLS

#define GNOME_FILE_MONIKER_TYPE        (gnome_file_moniker_get_type ())
#define GNOME_FILE_MONIKER(o)          (GTK_CHECK_CAST ((o), GNOME_FILE_MONIKER_TYPE, GnomeFileMoniker))
#define GNOME_FILE_MONIKER_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), GNOME_FILE_MONIKER_TYPE, GnomeFileMonikerClass))
#define GNOME_IS_FILE_MONIKER(o)       (GTK_CHECK_TYPE ((o), GNOME_FILE_MONIKER_TYPE))
#define GNOME_IS_FILE_MONIKER_CLASS(k) (GTK_CHECK_CLASS_TYPE ((k), GNOME_FILE_MONIKER_TYPE))

#define GNOME_FILE_MONIKER_GOAD_ID "GOAD:GNOME:Moniker:File:1.0"

typedef struct {
	GnomeMoniker moniker;
	char *goad_id;
	char *filename;
} GnomeFileMoniker;

typedef struct {
	GnomeMonikerClass parent_class;
} GnomeFileMonikerClass;


GtkType           gnome_file_moniker_get_type  (void);
GnomeFileMoniker *gnome_file_moniker_new       (const char *filename,
						const char *goad_id);

GnomeFileMoniker *gnome_file_moniker_construct (GnomeFileMoniker *gfm,
						GNOME_Moniker gfm_corba,
						const char *filename,
						const char *goad_id);
					    
END_GNOME_DECLS

#endif
