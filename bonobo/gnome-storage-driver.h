/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
#ifndef _GNOME_STORAGE_DRIVER_H_

typedef struct {
	
} GnomeStorageDriver;

GnomeStorageDriver *gnome_storage_driver_new (char *type, char *path, char *open_mode);

#endif
