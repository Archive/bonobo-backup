#ifndef __GNOME_PROPERTY_TYPES_H__
#define __GNOME_PROPERTY_TYPES_H__

#include <bonobo/gnome-property-bag.h>

BEGIN_GNOME_DECLS

/*
 * Standard type marshaler.
 */
CORBA_any *gnome_property_marshal_boolean   (const char *type, const gpointer value,
					     gpointer user_data);
CORBA_any *gnome_property_marshal_string    (const char *type, const gpointer value,
					     gpointer user_data);
CORBA_any *gnome_property_marshal_short     (const char *type, const gpointer value,
					     gpointer user_data);
CORBA_any *gnome_property_marshal_ushort    (const char *type, const gpointer value,
					     gpointer user_data);
CORBA_any *gnome_property_marshal_long	    (const char *type, const gpointer value,
					     gpointer user_data);
CORBA_any *gnome_property_marshal_ulong     (const char *type, const gpointer value,
					     gpointer user_data);
CORBA_any *gnome_property_marshal_float     (const char *type, const gpointer value,
					     gpointer user_data);
CORBA_any *gnome_property_marshal_double    (const char *type, const gpointer value,
					     gpointer user_data);

/*
 * Standard type demarshalers.
 */
gpointer   gnome_property_demarshal_boolean (const char *type, const CORBA_any *any,
					     gpointer user_data);
gpointer   gnome_property_demarshal_string  (const char *type, const CORBA_any *any,
					     gpointer user_data);
gpointer   gnome_property_demarshal_short   (const char *type, const CORBA_any *any,
					     gpointer user_data);
gpointer   gnome_property_demarshal_ushort  (const char *type, const CORBA_any *any,
					     gpointer user_data);
gpointer   gnome_property_demarshal_long    (const char *type, const CORBA_any *any,
					     gpointer user_data);
gpointer   gnome_property_demarshal_ulong   (const char *type, const CORBA_any *any,
					     gpointer user_data);
gpointer   gnome_property_demarshal_float   (const char *type, const CORBA_any *any,
					     gpointer user_data);
gpointer   gnome_property_demarshal_double  (const char *type, const CORBA_any *any,
					     gpointer user_data);

/*
 * Standard type comparers.
 */
gboolean   gnome_property_compare_boolean   (const char *type, gpointer value1,
					     gpointer value2, gpointer user_data);
gboolean   gnome_property_compare_string    (const char *type, gpointer value1,
					     gpointer value2, gpointer user_data);
gboolean   gnome_property_compare_short     (const char *type, gpointer value1,
					     gpointer value2, gpointer user_data);
gboolean   gnome_property_compare_ushort    (const char *type, gpointer value1,
					     gpointer value2, gpointer user_data);
gboolean   gnome_property_compare_long      (const char *type, gpointer value1,
					     gpointer value2, gpointer user_data);
gboolean   gnome_property_compare_ulong     (const char *type, gpointer value1,
					     gpointer value2, gpointer user_data);
gboolean   gnome_property_compare_float     (const char *type, gpointer value1,
					     gpointer value2, gpointer user_data);
gboolean   gnome_property_compare_double    (const char *type, gpointer value1,
					     gpointer value2, gpointer user_data);


/*
 * Standard type releaser.
 */
void	   gnome_property_generic_releaser  (const char *type, gpointer value,
					     gpointer user_data);

END_GNOME_DECLS

#endif /* ! __GNOME_PROPERTY_TYPES_H__ */
