/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
#include <config.h>
#include <gnome.h>
#include <libgnorba/gnorba.h>
#include <bonobo/bonobo.h>
#include <bonobo/gnome-bonobo.h>

CORBA_Environment ev;
CORBA_ORB orb;

static GnomeCanvasComponent *
item_factory (GnomeEmbeddable *bonobo_object, GnomeCanvas *canvas, void *data)
{
	GnomeCanvasItem *item;

	item = gnome_canvas_item_new (
		GNOME_CANVAS_GROUP (gnome_canvas_root (canvas)),
		gnome_canvas_rect_get_type (),
		"x1", 0.0,
		"y1", 0.0,
		"x2", 20.0,
		"y2", 20.0,
		"outline_color", "red",
		"fill_color", "blue",
		NULL);

	return gnome_canvas_component_new (item);
}

static GnomeObject *
bonobo_item_factory (GnomeEmbeddableFactory *factory, void *closure)
{
	GnomeEmbeddable *server;

	printf ("I am in item factory\n");
	server =  gnome_embeddable_new_canvas_item (item_factory, NULL);
	if (server == NULL)
		g_error ("Can not create gnome_embeddable");

	return (GnomeObject*) server;
}

static void
init_server_factory (void)
{
	gnome_embeddable_factory_new (
		"Test_item_server_factory", bonobo_item_factory, NULL);
}

int
main (int argc, char *argv [])
{
	CORBA_exception_init (&ev);

	gnome_CORBA_init_with_popt_table ("MyServer", "1.0", &argc, argv, NULL, 0, NULL, GNORBA_INIT_SERVER_FUNC, &ev);
	orb = gnome_CORBA_ORB ();
	
	if (bonobo_init (orb, NULL, NULL) == FALSE)
		g_error ("Can not bonobo_init\n");

	init_server_factory ();
	bonobo_main ();
	CORBA_exception_free (&ev);

	return 0;
}
