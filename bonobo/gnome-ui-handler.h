/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
#ifndef _GNOME_UI_HANDLER_H_
#define _GNOME_UI_HANDLER_H_

#include <bonobo/gnome-object.h>
#include <gtk/gtkwidget.h>
#include <glib.h>
#include <gtk/gtkmenushell.h>
#include <gtk/gtkcheckmenuitem.h>
#include <gtk/gtkaccelgroup.h>
#include <libgnomeui/gnome-app.h>
#include <libgnomeui/gnome-app-helper.h>

#define GNOME_UI_HANDLER_TYPE        (gnome_ui_handler_get_type ())
#define GNOME_UI_HANDLER(o)          (GTK_CHECK_CAST ((o), GNOME_UI_HANDLER_TYPE, GnomeUIHandler))
#define GNOME_UI_HANDLER_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), GNOME_UI_HANDLER_TYPE, GnomeUIHandlerClass))
#define GNOME_IS_UI_HANDLER(o)       (GTK_CHECK_TYPE ((o), GNOME_UI_HANDLER_TYPE))
#define GNOME_IS_UI_HANDLER_CLASS(k) (GTK_CHECK_CLASS_TYPE ((k), GNOME_UI_HANDLER_TYPE))

/*
 * Forward declarations.
 */
typedef struct _GnomeUIHandler GnomeUIHandler;
typedef struct _GnomeUIHandlerPrivate GnomeUIHandlerPrivate;
typedef struct _GnomeUIHandlerTopLevelData GnomeUIHandlerTopLevelData;
typedef struct _GnomeUIHandlerMenuItem GnomeUIHandlerMenuItem;
typedef struct _GnomeUIHandlerToolbarItem GnomeUIHandlerToolbarItem;

/*
 * The order of the arguments to this function might seem a bit
 * confusing; the closure (user_data), typically passed as the final
 * argument to a callback, is followed by a path parameter.  This is
 * to provide compatibility with the GnomeUIInfo (gnome-app-helper)
 * callback routines, whose arguments are (GtkWidget *menu_item, void
 * *user_data).
 */
typedef void (*GnomeUIHandlerCallbackFunc) (GnomeUIHandler *uih, void *user_data, const char *path);

/*
 * The GnomeUIHandler object.
 */
struct _GnomeUIHandler {
	GnomeObject			 base;

	/*
	 * If this UIHandler is for an object which is embedded in a
	 * container, then this field points to the top-level
	 * container's GNOME::UIHandler CORBA interface.  The
	 * top-level container (the container which is not itself a
	 * containee) manages the actual menu and toolbar item
	 * GtkWidgets.
	 */
	GNOME_UIHandler			 top_level_uih;

	/*
	 * The callback data for the menu items.
	 */
	GHashTable			*path_to_menu_callback;

	/*
	 * The callback data for the toolbar items.
	 */
	GHashTable			*path_to_toolbar_callback;
	GHashTable			*path_to_toolbar_toolbar;
	
	/*
	 * The top-level GnomeUIHandler must manage a lot of extra
	 * data (e.g. the actual menu/toolbar widgets, an internal
	 * representation of every menu/toolbar item, etc.).  This
	 * information is stored here.
	 */
	GnomeUIHandlerTopLevelData	*top;

	GnomeUIHandlerPrivate *priv;
};

/*
 * The data which a top-level UIHandler server must maintain.
 */
struct _GnomeUIHandlerTopLevelData {
	/*
	 * These hash tables are used to maintain an internal
	 * representation of the menu and toolbar trees.  They store
	 * internal data structures which you shouldn't muck with.
	 */
	GHashTable			*path_to_menu_item;

	GHashTable			*path_to_toolbar_item;
	GHashTable			*name_to_toolbar;

	/*
	 * Some application-global widget data.
	 */
	GnomeApp			*app;
	GtkAccelGroup			*accelgroup;
	GtkWidget			*statusbar;
	GtkWidget			*menubar;
	GtkWidget			*common_toolbar;
	GHashTable			*toolbars;

	/*
	 * This hash table maps menu item paths to the actual
	 * GtkMenuItem widgets.
	 */
	GHashTable			*path_to_menu_widget;

	/*
	 * This hash table maps subtree paths to the GtkMenuShell
	 * widget.
	 */
	GHashTable			*path_to_menu_shell;

	/* FIXME: Comments */
	GHashTable			*name_to_toolbar_widget;
	GHashTable			*path_to_toolbar_item_widget;

	/*
	 * If this UIHandler is for a top-level application which is
	 * acting as a container for one or more embedded objects,
	 * then this list contains the interfaces for the
	 * GnomeUIHandlers for the embedded objects.
	 */
	GList				*containee_uihs;
};

typedef struct {
	GnomeObjectClass parent_class;

	/*
	 * Signals.
	 */
	void (*menu_item_activated)	(GnomeUIHandler *uih, char *path, void *item_data);
	void (*menu_item_removed)	(GnomeUIHandler *uih, char *path, void *item_data);
	void (*menu_item_overriden)	(GnomeUIHandler *uih, char *path, void *item_data);
	void (*menu_item_reinstated)	(GnomeUIHandler *uih, char *path, void *item_data);

	void (*toolbar_item_activated)	(GnomeUIHandler *uih, char *path, void *item_data);
	void (*toolbar_item_removed)	(GnomeUIHandler *uih, char *path, void *item_data);
	void (*toolbar_item_overriden)	(GnomeUIHandler *uih, char *path, void *item_data);
	void (*toolbar_item_reinstated)	(GnomeUIHandler *uih, char *path, void *item_data);
} GnomeUIHandlerClass;

typedef enum {
	GNOME_UI_HANDLER_PIXMAP_NONE,
	GNOME_UI_HANDLER_PIXMAP_STOCK,
	GNOME_UI_HANDLER_PIXMAP_FILENAME,
	GNOME_UI_HANDLER_PIXMAP_XPM_DATA,
	GNOME_UI_HANDLER_PIXMAP_RGBA_DATA,
	GNOME_UI_HANDLER_PIXMAP_RGB_DATA
} GnomeUIHandlerPixmapType;

typedef enum {
	GNOME_UI_HANDLER_MENU_END,
	GNOME_UI_HANDLER_MENU_ITEM,
	GNOME_UI_HANDLER_MENU_SUBTREE,
	GNOME_UI_HANDLER_MENU_RADIOITEM,
	GNOME_UI_HANDLER_MENU_RADIOGROUP,
	GNOME_UI_HANDLER_MENU_TOGGLEITEM,
	GNOME_UI_HANDLER_MENU_SEPARATOR
} GnomeUIHandlerMenuItemType;

struct _GnomeUIHandlerMenuItem {
	char			        *path;

	GnomeUIHandlerMenuItemType	 type;
	char			        *label;
	char			        *hint;

	int				 pos;

	/*
	 * If this item is a subtree, then this field can point to a
	 * NULL-terminated array of GnomeUIHandlerMenuItems in the
	 * subtree.  If this item is a radio item, then this list
	 * contains the items in the radio group.
	 */
	GnomeUIHandlerMenuItem		*children;

	GnomeUIHandlerPixmapType	 pixmap_type;
	gpointer			 pixmap_data;

	guint				 accelerator_key;
	GdkModifierType			 ac_mods;

	/*
	 * The callback function and closure.
	 */
	GnomeUIHandlerCallbackFunc	 callback;
	gpointer			 callback_data;
};

typedef enum {
	GNOME_UI_HANDLER_TOOLBAR_END,
	GNOME_UI_HANDLER_TOOLBAR_ITEM,
	GNOME_UI_HANDLER_TOOLBAR_RADIOITEM,
	GNOME_UI_HANDLER_TOOLBAR_RADIOGROUP,
	GNOME_UI_HANDLER_TOOLBAR_TOGGLEITEM,
	GNOME_UI_HANDLER_TOOLBAR_SEPARATOR,
	GNOME_UI_HANDLER_TOOLBAR_CONTROL
} GnomeUIHandlerToolbarItemType;

struct _GnomeUIHandlerToolbarItem {
	char				*path;

	GnomeUIHandlerToolbarItemType	 type;

	char				*label;
	char				*hint;

	int				 pos;

	GNOME_Control			 control;

	GnomeUIHandlerToolbarItem	*children;

	GnomeUIHandlerPixmapType	 pixmap_type;
	gpointer			 pixmap_data;

	guint				 accelerator_key;
	GdkModifierType			 ac_mods;

	/*
	 * The callback function and closure.
	 */
	GnomeUIHandlerCallbackFunc	 callback;
	gpointer			 callback_data;
};

/*
 * The basic functions for managing the GnomeUIHandler object.
 */
GtkType			 gnome_ui_handler_get_type			(void);
GnomeUIHandler		*gnome_ui_handler_construct			(GnomeUIHandler *ui_handler,
									 GNOME_UIHandler corba_uihandler);
GnomeUIHandler		*gnome_ui_handler_new				(void);
void			 gnome_ui_handler_set_container			(GnomeUIHandler *uih,
									 GNOME_UIHandler container);
void			 gnome_ui_handler_unset_container		(GnomeUIHandler *uih);
void			 gnome_ui_handler_add_containee			(GnomeUIHandler *uih,
									 GNOME_UIHandler containee);
void			 gnome_ui_handler_remove_containee		(GnomeUIHandler *uih,
									 GNOME_UIHandler containee);
void			 gnome_ui_handler_set_accelgroup		(GnomeUIHandler *uih,
									 GtkAccelGroup *accelgroup);
GtkAccelGroup		*gnome_ui_handler_get_accelgroup		(GnomeUIHandler *uih);

/*
 * This path-building routine will be used for both toolbar and menu
 * item paths.
 */
char			*gnome_ui_handler_build_path			(char *comp1, ...);
char			*gnome_ui_handler_build_path_v			(va_list path_components);

/*
 * Menus.
 */

/* Toplevel menu routines. */
void			 gnome_ui_handler_set_app			(GnomeUIHandler *uih, GnomeApp *app);
void			 gnome_ui_handler_set_statusbar			(GnomeUIHandler *uih, GtkWidget *statusbar);
GtkWidget		*gnome_ui_handler_get_statusbar			(GnomeUIHandler *uih);

void			 gnome_ui_handler_set_menubar			(GnomeUIHandler *uih, GtkWidget *menubar);
GtkWidget		*gnome_ui_handler_get_menubar			(GnomeUIHandler *uih);

void			 gnome_ui_handler_create_menubar		(GnomeUIHandler *uih);
void			 gnome_ui_handler_create_popup_menu		(GnomeUIHandler *uih);
void			 gnome_ui_handler_do_popup_menu			(GnomeUIHandler *uih);
void			 gnome_ui_handler_set_toolbar			(GnomeUIHandler *uih, const char *name,
									 GtkWidget *toolbar);



/* Menu routines everyone can use. */
void			 gnome_ui_handler_menu_new			(GnomeUIHandler *uih, const char *path,
									 GnomeUIHandlerMenuItemType type,
									 const char *label, const char *hint,
									 int pos, GnomeUIHandlerPixmapType pixmap_type,
									 gpointer pixmap_data,
									 guint accelerator_key, GdkModifierType ac_mods,
									 GnomeUIHandlerCallbackFunc callback,
									 gpointer callback_data);
void			 gnome_ui_handler_menu_new_item			(GnomeUIHandler *uih, const char *path,
									 const char *label, const char *hint,
									 int pos, GnomeUIHandlerPixmapType pixmap_type,
									 gpointer pixmap_data,
									 guint accelerator_key, GdkModifierType ac_mods,
									 GnomeUIHandlerCallbackFunc callback,
									 gpointer callback_data);
void			 gnome_ui_handler_menu_new_subtree		(GnomeUIHandler *uih, const char *path,
									 const char *label, const char *hint, int pos,
									 GnomeUIHandlerPixmapType pixmap_type,
									 gpointer pixmap_data, guint accelerator_key,
									 GdkModifierType ac_mods);
void			 gnome_ui_handler_menu_new_separator		(GnomeUIHandler *uih, const char *path, int pos);
void			 gnome_ui_handler_menu_new_radiogroup		(GnomeUIHandler *uih, const char *path);
void			 gnome_ui_handler_menu_new_radioitem		(GnomeUIHandler *uih, const char *path,
									 const char *label, const char *hint, int pos,
									 guint accelerator_key, GdkModifierType ac_mods,
									 GnomeUIHandlerCallbackFunc callback,
									 gpointer callback_data);
void			 gnome_ui_handler_menu_new_toggleitem		(GnomeUIHandler *uih, const char *path,
									 const char *label, const char *hint, int pos,
									 guint accelerator_key, GdkModifierType ac_mods,
									 GnomeUIHandlerCallbackFunc callback,
									 gpointer callback_data);
void			 gnome_ui_handler_menu_add_one			(GnomeUIHandler *uih, const char *parent_path,
									 GnomeUIHandlerMenuItem *item);
void			 gnome_ui_handler_menu_add_list			(GnomeUIHandler *uih, const char *parent_path,
									 GnomeUIHandlerMenuItem *array);
void			 gnome_ui_handler_menu_add_tree			(GnomeUIHandler *uih, const char *parent_path,
									 GnomeUIHandlerMenuItem *tree);
void			 gnome_ui_handler_menu_remove			(GnomeUIHandler *uih, const char *path);
GnomeUIHandlerMenuItem	*gnome_ui_handler_menu_fetch_one		(GnomeUIHandler *uih, const char *path);
GnomeUIHandlerMenuItem	*gnome_ui_handler_menu_fetch_list		(GnomeUIHandler *uih, const char *path);
GnomeUIHandlerMenuItem	*gnome_ui_handler_menu_fetch_tree		(GnomeUIHandler *uih, const char *path);

void			 gnome_ui_handler_menu_free_one			(GnomeUIHandlerMenuItem *item);
void			 gnome_ui_handler_menu_free_list		(GnomeUIHandlerMenuItem *item);
void			 gnome_ui_handler_menu_free_tree		(GnomeUIHandlerMenuItem *item);


GList			*gnome_ui_handler_menu_fetch_by_callback	(GnomeUIHandler *uih,
									 GnomeUIHandlerCallbackFunc callback);
GList			*gnome_ui_handler_menu_fetch_by_callback_data	(GnomeUIHandler *uih, gpointer callback_data);
GList			*gnome_ui_handler_menu_get_child_paths		(GnomeUIHandler *uih, const char *parent_path);

GnomeUIHandlerMenuItem	*gnome_ui_handler_menu_parse_uiinfo_one		(GnomeUIInfo *uii);
GnomeUIHandlerMenuItem	*gnome_ui_handler_menu_parse_uiinfo_list	(GnomeUIInfo *uii);
GnomeUIHandlerMenuItem	*gnome_ui_handler_menu_parse_uiinfo_tree	(GnomeUIInfo *uii);

GnomeUIHandlerMenuItem	*gnome_ui_handler_menu_parse_uiinfo_one_with_data	(GnomeUIInfo *uii, void *data);
GnomeUIHandlerMenuItem	*gnome_ui_handler_menu_parse_uiinfo_list_with_data	(GnomeUIInfo *uii, void *data);
GnomeUIHandlerMenuItem	*gnome_ui_handler_menu_parse_uiinfo_tree_with_data	(GnomeUIInfo *uii, void *data);

int			 gnome_ui_handler_menu_get_pos			(GnomeUIHandler *uih, const char *path);

void			 gnome_ui_handler_menu_set_sensitivity		(GnomeUIHandler *uih, const char *path,
									 gboolean sensitive);
gboolean		 gnome_ui_handler_menu_get_sensitivity		(GnomeUIHandler *uih, const char *path);

void			 gnome_ui_handler_menu_set_label		(GnomeUIHandler *uih, const char *path,
									 const gchar *label);
gchar			*gnome_ui_handler_menu_get_label		(GnomeUIHandler *uih, const char *path);

void			 gnome_ui_handler_menu_set_hint			(GnomeUIHandler *uih, const char *path,
									 const gchar *hint);
gchar			*gnome_ui_handler_menu_get_hint			(GnomeUIHandler *uih, const char *path);

void			 gnome_ui_handler_menu_set_pixmap		(GnomeUIHandler *uih, const char *path,
									 GnomeUIHandlerPixmapType type, gpointer data);
void			 gnome_ui_handler_menu_get_pixmap		(GnomeUIHandler *uih, const char *path,
									 GnomeUIHandlerPixmapType *type, gpointer *data);

void			 gnome_ui_handler_menu_set_accel		(GnomeUIHandler *uih, const char *path,
									 guint accelerator_key, GdkModifierType ac_mods);
void			 gnome_ui_handler_menu_get_accel		(GnomeUIHandler *uih, const char *path,
									 guint *accelerator_key, GdkModifierType *ac_mods);

void			 gnome_ui_handler_menu_set_callback		(GnomeUIHandler *uih, const char *path,
									 GnomeUIHandlerCallbackFunc callback,
									 gpointer callback_data);
void			 gnome_ui_handler_menu_get_callback		(GnomeUIHandler *uih, const char *path,
									 GnomeUIHandlerCallbackFunc *callback,
									 gpointer *callback_data);

void			 gnome_ui_handler_menu_set_toggle_state		(GnomeUIHandler *uih, const char *path,
									 gboolean state);
gboolean		 gnome_ui_handler_menu_get_toggle_state		(GnomeUIHandler *uih, const char *path);

void			 gnome_ui_handler_menu_set_radio_state		(GnomeUIHandler *uih, const char *path,
									 gboolean state);
gboolean		 gnome_ui_handler_menu_get_radio_state		(GnomeUIHandler *uih, const char *path);

char			*gnome_ui_handler_menu_get_radio_group_selection(GnomeUIHandler *uih, const char *rg_path);

/*
 * The Toolbar manipulation routines.
 *
 * The name of the toolbar is the first element in the toolbar path.
 * For example:
 *		"Common/Save"
 *		"Graphics Tools/Erase"
 * 
 * Where both "Common" and "Graphics Tools" are the names of 
 * toolbars.
 * 
 */

void				 gnome_ui_handler_create_toolbar	(GnomeUIHandler *uih, const char *name);
void				 gnome_ui_handler_remove_toolbar	(GnomeUIHandler *uih, const char *name);
GList				*gnome_ui_handler_get_toolbar_list	(GnomeUIHandler *uih);

void				 gnome_ui_handler_toolbar_set_orientation (GnomeUIHandler *uih, const char *path,
									   GtkOrientation orientation);
GtkOrientation			 gnome_ui_handler_toolbar_get_orientation (GnomeUIHandler *uih, const char *path);



void				 gnome_ui_handler_toolbar_new		(GnomeUIHandler *uih, const char *path,
									 GnomeUIHandlerToolbarItemType type,
									 const char *label, const char *hint,
									 int pos, const GNOME_Control control,
									 GnomeUIHandlerPixmapType pixmap_type,
									 gconstpointer pixmap_data, guint accelerator_key,
									 GdkModifierType ac_mods,
									 GnomeUIHandlerCallbackFunc callback,
									 gpointer callback_data);
void				 gnome_ui_handler_toolbar_new_control	(GnomeUIHandler *uih, const char *path,
									 int pos, GNOME_Control control);
void				 gnome_ui_handler_toolbar_new_item	(GnomeUIHandler *uih, const char *path,
									 const char *label, const char *hint, int pos,
									 GnomeUIHandlerPixmapType pixmap_type,
									 gconstpointer pixmap_data,
									 guint accelerator_key, GdkModifierType ac_mods,
									 GnomeUIHandlerCallbackFunc callback,
									 gpointer callback_data);
void				 gnome_ui_handler_toolbar_new_separator	(GnomeUIHandler *uih, const char *path, int pos);
void				 gnome_ui_handler_toolbar_new_radiogroup(GnomeUIHandler *uih, const char *path);
void				 gnome_ui_handler_toolbar_new_radioitem	(GnomeUIHandler *uih, const char *path,
									 const char *label, const char *hint, int pos,
									 guint accelerator_key, GdkModifierType ac_mods,
									 GnomeUIHandlerCallbackFunc callback,
									 gpointer callback_data);
void				 gnome_ui_handler_toolbar_new_toggleitem(GnomeUIHandler *uih, const char *path,
									 const char *label, const char *hint, int pos,
									 guint accelerator_key, GdkModifierType ac_mods,
									 GnomeUIHandlerCallbackFunc callback,
									 gpointer callback_data);
void				 gnome_ui_handler_toolbar_add_one	(GnomeUIHandler *uih, const char *parent_path,
									 GnomeUIHandlerToolbarItem *item);
void				 gnome_ui_handler_toolbar_add_list	(GnomeUIHandler *uih, const char *parent_path,
									 GnomeUIHandlerToolbarItem *item);
void				 gnome_ui_handler_toolbar_add_tree	(GnomeUIHandler *uih, const char *parent_path,
									 GnomeUIHandlerToolbarItem *item);
void				 gnome_ui_handler_toolbar_remove	(GnomeUIHandler *uih, const char *path);
GnomeUIHandlerToolbarItem	*gnome_ui_handler_toolbar_fetch_one	(GnomeUIHandler *uih, const char *path);
GnomeUIHandlerToolbarItem	*gnome_ui_handler_toolbar_fetch_list	(GnomeUIHandler *uih, const char *path);
GnomeUIHandlerToolbarItem	*gnome_ui_handler_toolbar_fetch_tree	(GnomeUIHandler *uih, const char *path);
GList				*gnome_ui_handler_toolbar_fetch_by_callback (GnomeUIHandler *uih,
									     GnomeUIHandlerCallbackFunc callback);
GList				*gnome_ui_handler_toolbar_fetch_by_callback_data (GnomeUIHandler *uih,
										  gpointer callback_data);
GList				*gnome_ui_handler_toolbar_get_child_paths (GnomeUIHandler *uih, const char *path);

GnomeUIHandlerToolbarItem	*gnome_ui_handler_toolbar_parse_uiinfo_one	(GnomeUIInfo *uii);
GnomeUIHandlerToolbarItem	*gnome_ui_handler_toolbar_parse_uiinfo_list	(GnomeUIInfo *uii);
GnomeUIHandlerToolbarItem	*gnome_ui_handler_toolbar_parse_uiinfo_tree	(GnomeUIInfo *uii);

GnomeUIHandlerToolbarItem	*gnome_ui_handler_toolbar_parse_uiinfo_one_with_data (GnomeUIInfo *uii, void *data);
GnomeUIHandlerToolbarItem	*gnome_ui_handler_toolbar_parse_uiinfo_list_with_data (GnomeUIInfo *uii, void *data);
GnomeUIHandlerToolbarItem	*gnome_ui_handler_toolbar_parse_uiinfo_tree_with_data (GnomeUIInfo *uii, void *data);

void			         gnome_ui_handler_toolbar_free_one	        (GnomeUIHandlerToolbarItem *item);
void			         gnome_ui_handler_toolbar_free_list		(GnomeUIHandlerToolbarItem *item);
void			         gnome_ui_handler_toolbar_free_tree		(GnomeUIHandlerToolbarItem *item);

int				 gnome_ui_handler_toolbar_item_get_pos	(GnomeUIHandler *uih, const char *path);
void				 gnome_ui_handler_toolbar_item_set_pos	(GnomeUIHandler *uih, const char *path,
									 int pos);

void				 gnome_ui_handler_toolbar_item_set_sensitivity (GnomeUIHandler *uih, const char *path,
										gboolean sensitive);
gboolean			 gnome_ui_handler_toolbar_item_get_sensitivity (GnomeUIHandler *uih, const char *path);

void				 gnome_ui_handler_toolbar_item_set_label	(GnomeUIHandler *uih, const char *path,
										 gchar *label);
gchar				*gnome_ui_handler_toolbar_item_get_label	(GnomeUIHandler *uih, const char *path);

void				 gnome_ui_handler_toolbar_item_set_hint	(GnomeUIHandler *uih, const char *path,
									 gchar *hint);
gchar				*gnome_ui_handler_toolbar_item_get_hint	(GnomeUIHandler *uih, const char *path);

void				 gnome_ui_handler_toolbar_item_set_pixmap	(GnomeUIHandler *uih, const char *path,
										 GnomeUIHandlerPixmapType type, gpointer data);
void				 gnome_ui_handler_toolbar_item_get_pixmap	(GnomeUIHandler *uih, const char *path,
										 GnomeUIHandlerPixmapType *type, gpointer *data);

void				 gnome_ui_handler_toolbar_item_set_accel	(GnomeUIHandler *uih, const char *path,
										 guint accelerator_key, GdkModifierType ac_mods);
void				 gnome_ui_handler_toolbar_item_get_accel	(GnomeUIHandler *uih, const char *path,
										 guint *aaccelerator_key, GdkModifierType *ac_mods);

void				 gnome_ui_handler_toolbar_item_set_callback	(GnomeUIHandler *uih, const char *path,
										 GnomeUIHandlerCallbackFunc callback,
										 gpointer callback_data);

void				 gnome_ui_handler_toolbar_item_get_callback	(GnomeUIHandler *uih, const char *path,
										 GnomeUIHandlerCallbackFunc *callback,
										 gpointer *callback_data);

void				 gnome_ui_handler_toolbar_item_toggle_set_state (GnomeUIHandler *uih, const char *path, gboolean state);
gboolean			 gnome_ui_handler_toolbar_item_toggle_get_state (GnomeUIHandler *uih, const char *path);

gboolean			 gnome_ui_handler_toolbar_item_radio_get_state  (GnomeUIHandler *uih, const char *path);
void				 gnome_ui_handler_toolbar_item_radio_set_state  (GnomeUIHandler *uih, const char *path, gboolean state);

void                             gnome_ui_handler_toolbar_item_set_orientation (GnomeUIHandler *uih, const char *path,
										GtkOrientation orientation);
GtkOrientation                   gnome_ui_handler_toolbar_item_get_orientation (GnomeUIHandler *uih, const char *path);

extern POA_GNOME_UIHandler__vepv gnome_ui_handler_vepv;
POA_GNOME_UIHandler__epv *gnome_ui_handler_get_epv (void);

#endif /* _GNOME_UI_HANDLER_H_ */
