/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Bonobo
 *
 *  Copyright (C) 1999 Red Hat Software.
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Elliot Lee <sopwith@redhat.com>
 *          Hacked from gmf/libgmf/gmf-filterregistry.*
 *
 */

#include <config.h>

#include <string.h>
#include <sys/types.h>
#include <stdio.h>
#include <unistd.h>
#include <dirent.h>

#include "gnome-component-directory.h"

#define FILTER_LISTING_PATH "/bonobo/components"

CORBA_Object gnome_component_activate(GoadServerList *slist,
				      GnomeComponentDirectoryEntry *entry)
{
	return goad_server_activate_with_id(slist, entry->goad_id, 0, NULL);
}

static gboolean string_in_array(const char *string, const char **array)
{
	int i;
	for(i = 0; array[i]; i++) {
		if(!strcmp(string, array[i]))
			return TRUE;
	}

	return FALSE;
}

CORBA_Object
gnome_component_activate_for_url(GoadServerList *slist,
				 GnomeComponentDirectory *dir,
				 const char *url,
				 GnomeComponentFunctionality required_functionality)
{
	return gnome_component_activate_for_type(slist, dir, gnome_mime_type_of_file(url), required_functionality);
}

CORBA_Object gnome_component_activate_for_type(GoadServerList *slist,
					       GnomeComponentDirectory *dir,
					       const char *mime_type,
					       GnomeComponentFunctionality required_functionality)
{
	CORBA_Object retval = CORBA_OBJECT_NIL;
	GSList *ltmp, *mylist = NULL;
	GnomeComponentDirectory *reg;
	CORBA_Environment ev;

	g_return_val_if_fail(mime_type, CORBA_OBJECT_NIL);

	reg = dir;
	if(!reg)
		reg = gnome_component_directory_get();

	g_return_val_if_fail(reg, CORBA_OBJECT_NIL);

	CORBA_exception_init(&ev);

	/* Pass one - try to find stuff that matches exactly. */
	for(ltmp = g_hash_table_lookup(reg->by_mime_type, mime_type); ltmp; ltmp = g_slist_next(ltmp)) {
		GnomeComponentDirectoryEntry *ent;

		ent = (GnomeComponentDirectoryEntry *)ltmp->data;

		if(required_functionality != ent->functionality) {
			mylist = g_slist_prepend(mylist, ent);
			continue;
		}

		retval = gnome_component_activate(slist, ent);
		if(!CORBA_Object_is_nil(retval, &ev))
			break;
		/* If activating a component fails once, we don't want to bother putting it onto myslist to be tried again :-) */
	}

	/* Pass two - try to find stuff that matches at least the requested functionality level */
	if(CORBA_Object_is_nil(retval, &ev)) {
		for(ltmp = myslist; ltmp; ltmp = g_slist_next(ltmp)) {
			GnomeComponentDirectoryEntry *ent;

			ent = (GnomeComponentDirectoryEntry *)ltmp->data;

			if((required_functionality & ent->functionality) != required_functionality)
				continue;

			retval = gnome_component_activate(slist, ent);
			if(!CORBA_Object_is_nil(retval, &ev))
				break;
		}
	}

	if(!dir)
		gnome_component_directory_free(reg);

	CORBA_exception_free(&ev);

	g_slist_free(mylist);

	return retval;
}

static void
gmf_registry_entry_add_for_type(GHashTable *ht,
				char *type,
				GnomeComponentDirectoryEntry *ent)
{
	GSList *ltmp;
	ltmp = g_hash_table_lookup(ht, type);

	if(ltmp)
		g_hash_table_insert(ht, type, g_slist_prepend(ltmp, ent));
	else
		g_hash_table_insert(ht, g_strdup(type), g_slist_prepend(NULL, ent));
}

static GnomeComponentFunctionality
component_functionality(char **func_strings)
{
	GnomeComponentFunctionality retval = 0;
	int i;

	for(i = 0; func_strings[i]; i++) {
		char *ctmp;

		ctmp = func_strings[i];

		if(!strcmp(ctmp, "display")) {
			retval |= COMPONENT_FUNCTION_DISPLAY;
		} else if(!strcmp(ctmp, "edit")) {
			retval |= COMPONENT_FUNCTION_EDIT;
		} else if(!strcmp(ctmp, "print")) {
			retval |= COMPONENT_FUNCTION_PRINT;
		} else if(!strcmp(ctmp, "inplace")) {
			retval |= COMPONENT_FUNCTION_INPLACE;
		} else
			g_warning("Unknown functionality specifier %s", ctmp);
	}

	return retval;
}

static void
gmf_filter_registry_read_file(GnomeComponentDirectory *registry, const char *file, GString *tmpstr)
{
	gpointer iter;
	char *dummy;
	GnomeComponentDirectoryEntry *ent;
	int i, n;

	gnome_config_push_prefix(file);

	iter = gnome_config_init_iterator_sections(file);

	while((iter = gnome_config_iterator_next(iter, &dummy, NULL))) {
		char **ctmp;
		ent = g_new0(GMFFilterRegistryEntry, 1);

		/* get info for entry */
		if(*file == '=')
			g_string_sprintf(tmpstr, "=%s/=goad_id", dummy);
		else
			g_string_sprintf(tmpstr, "%s/goad_id", dummy);

		ent->goad_id = gnome_config_get_string(tmpstr->str);
		g_assert(ent->goad_id && *ent->goad_id);

		if(*file == '=')
			g_string_sprintf(tmpstr, "=%s/=mime_types", dummy);
		else
			g_string_sprintf(tmpstr, "%s/mime_types", dummy);
		gnome_config_get_vector(tmpstr->str, &n, &ent->mime_types);
		ent->mime_types = g_realloc(ent->mime_types, sizeof(char *) * (n+1));
		ent->mime_types[n] = NULL;

		if(*file == '=')
			g_string_sprintf(tmpstr, "=%s/=functionality", dummy);
		else
			g_string_sprintf(tmpstr, "%s/functionality", dummy);
		gnome_config_get_vector(tmpstr->str, &n, &ctmp);
		ctmp = g_realloc(ctmp, sizeof(char *) * (n+1));
		ctmp = NULL;
		ent->functionality = component_functionality(ctmp);
		g_strfreev(ctmp);

		/* put entry into registry */
		registry->list = g_slist_append(registry->list, ent);
		g_hash_table_insert(registry->by_id, ent->goad_id, ent);
    
		for(i = 0; ent->mime_types[i]; i++)
			gmf_registry_entry_add_for_type(registry->by_mime_type,
							ent->mime_types[i], ent);

		g_free(dummy);
	}

	gnome_config_pop_prefix();
}

static void
gmf_filter_registry_read_dir(GnomeComponentDirectory *registry, const char *dir)
{
	GString *tmpstr2;
	GString *tmpstr;
	struct dirent *dent;
	DIR *dirh;

	dirh = opendir(dir);
	if(!dirh) return;

	tmpstr = g_string_new(NULL);
	tmpstr2 = g_string_new(NULL);

	for(dent = readdir(dirh); dent; dent = readdir(dirh)) {
		register char *p;

		p = strrchr(dent->d_name, '.');
		if (!p || (strcmp (p, ".bonobo") != 0))
			continue;

		g_string_sprintf(tmpstr2, "=%s/%s", dir, dent->d_name);
		gmf_filter_registry_read_file(registry, tmpstr2->str, tmpstr);
	}

	g_string_free(tmpstr, TRUE);
	g_string_free(tmpstr2, TRUE);
}

GnomeComponentDirectory *
gnome_component_directory_get(void)
{
	GnomeComponentDirectory *retval;
	GString *tmpstr;
	char *ctmp;

	retval = g_new0(GnomeComponentDirectory, 1);
	retval->by_id = g_hash_table_new(g_str_hash, g_str_equal);
	retval->by_mime_type = g_hash_table_new(g_str_hash, g_str_equal);

	tmpstr = g_string_new(NULL);

	g_string_sprintf(tmpstr, "%s" FILTER_LISTING_PATH, gnome_user_dir);
	gmf_filter_registry_read_dir(retval, tmpstr->str);

	ctmp = gnome_unconditional_config_file(FILTER_LISTING_PATH);
	gmf_filter_registry_read_dir(retval, ctmp);
	g_free(ctmp);

	g_string_free(tmpstr, TRUE);

	return retval;
}

static void
gmf_filter_registry_entry_free(GMFFilterRegistryEntry *entry)
{
	g_free(entry->goad_id);
	g_strfreev(entry->mime_types);
}

static gboolean
gmf_bytype_free(gchar *key, GSList *list)
{
	g_free(key);
	g_slist_free(list);

	return TRUE;
}

void
gnome_component_directory_free(GnomeComponentDirectory *registry)
{
	if(!registry)
		return;

	g_hash_table_foreach_remove(registry->by_mime_type,
				    (GHRFunc)gmf_bytype_free,
				    registry);
	g_slist_foreach(registry->list,
			(GFunc)gmf_filter_registry_entry_free,
			registry);

	g_hash_table_destroy(registry->by_id);
	g_hash_table_destroy(registry->by_mime_type);
	g_slist_free(registry->list);

	g_free(registry);
}
