/*
 * A simple program to test the GnomeUIHandler.
 *
 * Author:
 *    Nat Friedman (nat@nat.org)
 */

 
#include <config.h>
#include <gnome.h>
#include <libgnorba/gnorba.h>
#include <gdk/gdkprivate.h>
#include <gdk/gdkx.h>
#include <bonobo/gnome-bonobo.h>
#include <bonobo/gnome-ui-handler.h>
#include <sys/stat.h>
#include <unistd.h>

#include "/home/ndf/projects/gnome/gnome-applets/battery/bolt.xpm"

CORBA_Environment ev;
CORBA_ORB orb;


typedef struct {
	GnomeUIHandler *uih;
	GtkWidget *app;
	GnomeContainer *container;
	GtkWidget *box;
	GnomeAppBar *appbar;
	GNOME_View view;
} Application;

static gint
quit_cb (GnomeUIHandler *uih, void *data, char *path)
{
	gnome_ui_handler_menu_remove (uih, "/File");
}

GnomeUIInfo sort_menu[] = {
	{ GNOME_APP_UI_ITEM, N_("_Sort Submenu"), NULL,
	  NULL, NULL, NULL,
	  GNOME_APP_PIXMAP_DATA, bolt_xpm,
	  'S', GDK_CONTROL_MASK, NULL },
	{ GNOME_APP_UI_ITEM, N_("Sort Submenu _Recursive"), NULL,
	  NULL, NULL, NULL,
	  GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_SPELLCHECK,
	  'R', GDK_CONTROL_MASK, NULL },
	GNOMEUIINFO_END

};

GnomeUIInfo file_menu[] = {

	GNOMEUIINFO_MENU_NEW_ITEM(N_("_New Submenu..."), N_("Create a new submenu"), NULL, NULL),

	GNOMEUIINFO_MENU_NEW_ITEM(N_("New _Item..."), N_("Create a new menu item"), NULL, NULL),

	{ GNOME_APP_UI_ITEM, N_("_Delete..."), NULL, NULL, NULL, NULL,
	  GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_CUT, 'D', GDK_CONTROL_MASK,
	  NULL },

	GNOMEUIINFO_SEPARATOR,

	GNOMEUIINFO_SUBTREE ("_Sort...", sort_menu),

	GNOMEUIINFO_SEPARATOR,

	GNOMEUIINFO_MENU_EXIT_ITEM (quit_cb, NULL),

	GNOMEUIINFO_END
};

GnomeUIInfo main_menu[] = {
	GNOMEUIINFO_MENU_FILE_TREE (file_menu),
	GNOMEUIINFO_END
};

static Application *
application_new (void)
{
	Application *app;

	app = g_new0 (Application, 1);
	app->app = gnome_app_new ("test-ui-handler",
				  "Sample UIHandler Application");
	app->container = GNOME_CONTAINER (gnome_container_new ());

	app->box = gtk_vbox_new (FALSE, 0);
	gtk_widget_show (app->box);
	gnome_app_set_contents (GNOME_APP (app->app), app->box);

	app->appbar = gnome_appbar_new (FALSE, TRUE, GNOME_PREFERENCES_USER);
	gnome_app_set_statusbar (GNOME_APP (app->app), GTK_WIDGET (app->appbar));

	app->uih = gnome_ui_handler_new ();
	gnome_ui_handler_set_statusbar (app->uih, GNOME_APPBAR (app->appbar));

	gnome_ui_handler_set_app (app->uih, GNOME_APP (app->app));
	gnome_ui_handler_create_menubar (app->uih);

	gnome_ui_handler_menu_new_subtree (app->uih, "/File", N_("_File"), NULL, -1,
					   GNOME_UI_HANDLER_PIXMAP_NONE, NULL, 0, 0);
	gnome_ui_handler_menu_new_item (app->uih, "/File/Stuff", N_("_Stuff"), NULL, -1,
					GNOME_UI_HANDLER_PIXMAP_NONE, NULL, 0, 0,
					NULL, NULL);
	gnome_ui_handler_menu_new_radiogroup (app->uih, "/File/rg");
	gnome_ui_handler_menu_new_radioitem (app->uih, "/File/rg/1", "radio item 1", NULL, -1, 0, 0, NULL, NULL);
	gnome_ui_handler_menu_new_radioitem (app->uih, "/File/rg/2", "radio item 2", NULL, -1, 0, 0, NULL, NULL);
	gnome_ui_handler_menu_set_radio_state (app->uih, "/File/rg/2", TRUE);

	gnome_ui_handler_create_toolbar (app->uih, "Common");

	gnome_ui_handler_toolbar_new_item (app->uih,
					   "/Common/item 1",
					   "Item 1", "Try clicking, dumbass.",
					   0, GNOME_UI_HANDLER_PIXMAP_NONE, NULL, 0, 0,
					   NULL, NULL);

	gtk_widget_show (app->app);

	return app;
}

int
main (int argc, char *argv [])
{
	Application *app;

	CORBA_exception_init (&ev);
	
	gnome_CORBA_init ("MyShell", "1.0", &argc, argv, 0, &ev);
	orb = gnome_CORBA_ORB ();
	
	if (bonobo_init (orb, NULL, NULL) == FALSE)
		g_error (_("Can not bonobo_init\n"));

	app = application_new ();
	
	gtk_main ();

	CORBA_exception_free (&ev);
	
	return 0;
}
