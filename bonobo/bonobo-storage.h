/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
#ifndef _BONOBO_STORAGE_H_
#define _BONOBO_STORAGE_H_

struct _StorageHandle;
struct _StorageDriver;

typedef struct _StorageHandle StorageHandle;
typedef struct _StorageDriver StorageDriver;

#endif

