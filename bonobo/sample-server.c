/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
#include <config.h>
#include <gnome.h>
#include <libgnorba/gnorba.h>
#include <bonobo/bonobo.h>
#include <bonobo/gnome-bonobo.h>

CORBA_Environment ev;
CORBA_ORB orb;

static GnomeView *
view_factory (GnomeEmbeddable *bonobo_object, const GNOME_ViewFrame view_frame, void *data)
{
	GnomeView *view;
	GtkWidget *widget;
	

	widget = gtk_hbox_new (0, 0);
	gtk_container_add (GTK_CONTAINER (widget), gtk_entry_new ());
	gtk_widget_show_all (widget);
	view = gnome_view_new (widget);
	
	return view;
}

static GnomeObject *
bonobo_object_factory (GnomeEmbeddableFactory *factory, void *closure)
{
	GnomeEmbeddable *server;
	
	server =  gnome_embeddable_new (view_factory, NULL);
	if (server == NULL)
		g_error ("Can not create gnome_embeddable");

	return (GnomeObject*) server;
}

static void
init_server_factory (void)
{
	gnome_embeddable_factory_new (
		"Test_server_factory", bonobo_object_factory, NULL);
}

int
main (int argc, char *argv [])
{
	CORBA_exception_init (&ev);

	gnome_CORBA_init_with_popt_table ("MyServer", "1.0", &argc, argv, NULL, 0, NULL, GNORBA_INIT_SERVER_FUNC, &ev);
	orb = gnome_CORBA_ORB ();
	
	if (bonobo_init (orb, NULL, NULL) == FALSE)
		g_error ("Can not bonobo_init\n");

	init_server_factory ();
	bonobo_main ();
	CORBA_exception_free (&ev);

	return 0;
}

