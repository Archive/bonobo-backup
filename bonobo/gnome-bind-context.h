/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
#ifndef _GNOME_BIND_CONTEXT_H_
#define _GNOME_BIND_CONTEXT_H_

#include <bonobo/gnome-object.h>

BEGIN_GNOME_DECLS

#define GNOME_BIND_CONTEXT_TYPE        (gnome_bind_context_get_type ())
#define GNOME_BIND_CONTEXT(o)          (GTK_CHECK_CAST ((o), GNOME_BIND_CONTEXT_TYPE, GnomeBindContext))
#define GNOME_BIND_CONTEXT_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), GNOME_BIND_CONTEXT_TYPE, GnomeBindContextClass))
#define GNOME_IS_BIND_CONTEXT(o)       (GTK_CHECK_TYPE ((o), GNOME_BIND_CONTEXT_TYPE))
#define GNOME_IS_BIND_CONTEXT_CLASS(k) (GTK_CHECK_CLASS_TYPE ((k), GNOME_BIND_CONTEXT_TYPE))

typedef struct {
	GnomeObject base;
	GHashTable  *table;
} GnomeBindContext;

typedef struct {
	GnomeObjectClass parent_class;
} GnomeBindContextClass;

GnomeBindContext *gnome_bind_context_new       (void);
GnomeBindContext *gnome_bind_context_construct (GnomeBindContext *bindctx,
						GNOME_BindContext corba_bindctx);

GtkType           gnome_bind_context_get_type  (void);
END_GNOME_DECLS

#endif /* _GNOME_BIND_CONTEXT_H_ */
