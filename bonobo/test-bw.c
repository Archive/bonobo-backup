
#include <config.h>
#include <gnome.h>
#include <bonobo/gnome-bonobo.h>

typedef struct {
	GtkWidget  *box;
	char *component_name;
} closure_t;

static void
button_clicked_cb (GtkWidget *button, closure_t *cl)
{
	GtkWidget *bw;

	bw = gnome_bonobo_widget_new_subdoc (cl->component_name, NULL);

	gtk_box_pack_start (GTK_BOX (cl->box), bw, TRUE, TRUE, 0);

	gtk_widget_show_all (cl->box);
}

int
main (int argc, char **argv)
{
	GtkWidget *app;
	CORBA_Environment ev;
	GtkWidget *box;
	GtkWidget *button;
	closure_t  cl;

	if (argc == 2 && argv[1])
		cl.component_name = argv[1];
	else
		cl.component_name = "embeddable:paint-component-simple";

	CORBA_exception_init (&ev);

	gnome_CORBA_init ("MyShell", "1.0", &argc, argv, 0, &ev);

	if (bonobo_init (gnome_CORBA_ORB (), NULL, NULL) == FALSE)
		g_error ("Cannot bonobo_init");

	app = gnome_app_new ("test-bw", "Test Bonobo Widget");

        gtk_window_set_default_size(GTK_WINDOW(app), 640, 480);
        gtk_window_set_policy(GTK_WINDOW(app), TRUE, TRUE, FALSE);

	box = gtk_vbox_new (FALSE, 5);
	cl.box = box;

	button = gtk_button_new_with_label ("Add component");
	gtk_signal_connect (GTK_OBJECT (button), "clicked",
			    GTK_SIGNAL_FUNC (button_clicked_cb), &cl);

	gtk_box_pack_start (GTK_BOX (box), button, FALSE, FALSE, 0);

	gnome_app_set_contents (GNOME_APP (app), box);

	gtk_widget_show_all (app);

	bonobo_main ();

	return 0;
}
