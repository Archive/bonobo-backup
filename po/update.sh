#!/bin/sh

xgettext --default-domain=bonobo --directory=.. \
  --add-comments --keyword=_ --keyword=N_ \
  --files-from=./POTFILES.in \
&& test ! -f bonobo.po \
   || ( rm -f ./bonobo.pot \
    && mv bonobo.po ./bonobo.pot )
