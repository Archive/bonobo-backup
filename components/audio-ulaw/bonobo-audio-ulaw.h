#include <gnome.h>
#include <libgnorba/gnorba.h>
#include <bonobo/gnome-bonobo.h>

#ifndef __BONOBO_AUDIO_ULAW_H__
#define __BONOBO_AUDIO_ULAW_H__

/*
 * Component data
 */
typedef struct {
  GnomeEmbeddable *embeddable;

  gchar *sound;
  guint sound_len;
  
  gint update_callback_id;  
} embeddable_data_t;

/*
 * View data
 */
typedef struct {
	embeddable_data_t *embeddable_data;
	
	GtkWidget        *canvas;
	GtkWidget        *vbox;
	GtkWidget        *hscroll;
	GnomeCanvasItem  *canvas_item;
} view_data_t;

#endif /* __BONOBO_AUDIO_ULAW_H__ */
