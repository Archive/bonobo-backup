/*
 * A BonoboObject to display audio/ulaw.
 *
 * Author:
 *   Chris Lahey <clahey@umich.edu>
 *
 * Based on bonobo-text-plain.c by
 *   Nat Friedman (nat@gnome-support.com)
 *
 */
#include <config.h>
#include "bonobo-audio-ulaw.h"
#include "item-audio.h"
#include "color.h"

/*
 * Number of running objects
 */ 
static int running_objects = 0;
static GnomeEmbeddableFactory *factory = NULL;

/*
 * Utility functions.
 */
static void
destroy_view (GnomeView *view, view_data_t *view_data)
{
	gtk_object_unref (GTK_OBJECT (view_data->vbox));

	g_free (view_data);
}

static void
free_sound (embeddable_data_t *embeddable_data)
{
	if (embeddable_data->sound != NULL)
		g_free (embeddable_data->sound);
	embeddable_data->sound = NULL;
	embeddable_data->sound_len = 0;
} /* free_sound */

static void
update_view_foreach (GnomeView *view, gpointer user_data)
{
	embeddable_data_t *embeddable_data = user_data;
	view_data_t       *view_data = gtk_object_get_data (GTK_OBJECT (view),
							    "view_data");

	gnome_canvas_set_scroll_region (GNOME_CANVAS (view_data->canvas),
					0, 0, embeddable_data->sound_len, 256);
	
	gnome_canvas_item_set (view_data->canvas_item,
			       "EmbeddableData", embeddable_data, NULL);
} /* update_view_foreach */
		  
static gboolean
update_all_views_callback (gpointer data)
{
  embeddable_data_t *embeddable_data = (embeddable_data_t *) data;

  gnome_embeddable_foreach_view (embeddable_data->embeddable,
				 update_view_foreach, embeddable_data);

  embeddable_data->update_callback_id = 0;

  return FALSE;
}

static void
progressive_update (embeddable_data_t *embeddable_data,
		    char *buff,
		    size_t count)
{
	/*
	 * 1. Append the new sound data to the embeddable's sound buffer.
	 */
	embeddable_data->sound = g_realloc (embeddable_data->sound,
					   embeddable_data->sound_len
					   + count);

	memcpy (embeddable_data->sound + embeddable_data->sound_len,
		buff, count);

	embeddable_data->sound_len += count;

	if ( embeddable_data->update_callback_id == 0 )
	  {
	    embeddable_data->update_callback_id =
	      gtk_timeout_add( 10,
			       update_all_views_callback,
			       (gpointer) embeddable_data );
	  }

} /* progressive_update */

/*
 * GNOME::PersistStream
 *
 * These two functions implement the GNOME::PersistStream load and
 * save methods which allow data to be loaded into and out of the
 * BonoboObject.
 */
static int
stream_read (GNOME_Stream stream, embeddable_data_t *embeddable_data)
{
	GNOME_Stream_iobuf *buffer;
	CORBA_long bytes_read;
	CORBA_Environment ev;

	buffer = GNOME_Stream_iobuf__alloc ();

	CORBA_exception_init (&ev);
	do {

#define READ_CHUNK_SIZE 65536
		bytes_read = GNOME_Stream_read (stream, READ_CHUNK_SIZE,
						&buffer, &ev);

		if (ev._major != CORBA_NO_EXCEPTION) {
			CORBA_free (buffer);
			CORBA_exception_free (&ev);
			return -1;
		}

		embeddable_data->sound = g_realloc (embeddable_data->sound,
						  embeddable_data->sound_len
						  + buffer->_length);

		memcpy (embeddable_data->sound + embeddable_data->sound_len,
			buffer->_buffer, buffer->_length);

		embeddable_data->sound_len += buffer->_length;

	} while (bytes_read > 0);

	CORBA_free (buffer);
	CORBA_exception_free (&ev);

	if (bytes_read < 0)
		return -1;

	return 0;
} /* stream_read */

/*
 * This function implements the GNOME::PersistStream:load method.
 */
static int
pstream_load (GnomePersistStream *ps, GNOME_Stream stream,
	      void *data)
{
	embeddable_data_t *embeddable_data = data;

	/*
	 * 1. Free the old sound data.
	 */
	free_sound (embeddable_data);

	/*
	 * 2. Read the new sound data.
	 */
	if (stream_read (stream, embeddable_data) < 0)
		return -1; /* This will raise an exception. */

	/*
	 * 3. Update the displays.
	 */
	gnome_embeddable_foreach_view (embeddable_data->embeddable,
				       update_view_foreach, embeddable_data);

	return 0;
} /* pstream_load */

/*
 * This function implements the GNOME::PersistStream:save method.
 */
static int
pstream_save (GnomePersistStream *ps, GNOME_Stream stream,
	      void *data)
{
	embeddable_data_t  *embeddable_data = data;
	GNOME_Stream_iobuf *buffer;
	size_t              pos;
	CORBA_Environment   ev;

	/*
	 * Write the sound data into the stream.
	 *
	 * FIXME: Do we really _have_ to double-buffer the sound?
	 */
	buffer = GNOME_Stream_iobuf__alloc ();

	data = CORBA_sequence_CORBA_octet_allocbuf (embeddable_data->sound_len);
	memcpy (data, embeddable_data->sound, embeddable_data->sound_len);

	buffer->_buffer = data;
	buffer->_length = embeddable_data->sound_len;

	pos = 0;
	CORBA_exception_init (&ev);
	
	while (pos < embeddable_data->sound_len) {
		CORBA_long bytes_read;

		bytes_read = GNOME_Stream_write (stream, buffer, &ev);

		if (ev._major != CORBA_NO_EXCEPTION) {
			CORBA_free (buffer);
			CORBA_free (data);
			CORBA_exception_free (&ev);
			return -1;
		}

		pos += bytes_read;
	}

	CORBA_free (buffer);
	CORBA_free (data);
	CORBA_exception_free (&ev);
	
	return 0;
} /* pstream_save */

/*
 * GNOME::ProgressiveDataSink
 *
 * These functions implement the ProgressiveDataSink interface
 * methods, which are used to send a slow stream of data to the widget
 * and have it update its views progressively.
 */

static int
progressive_start (GnomeProgressiveDataSink *psink, void *data)
{
	embeddable_data_t * embeddable_data = (embeddable_data_t *) data;

	free_sound (embeddable_data);
	gnome_embeddable_foreach_view (embeddable_data->embeddable,
				 update_view_foreach, embeddable_data);

	return 0;
} /* progressive_start */

static int
progressive_add_data (GnomeProgressiveDataSink *psink,
		      const GNOME_ProgressiveDataSink_iobuf *buffer,
		      void *data)
{
	embeddable_data_t *embeddable_data = (embeddable_data_t *) data;

	progressive_update (embeddable_data, (char *) buffer->_buffer,
			    (size_t) buffer->_length);

	return 0;
} /* progressive_add_data */

/*
 * This callback is invoked when the GnomeEmbeddable object
 * encounters a fatal CORBA exception.
 */
static void
embeddable_system_exception_cb (GnomeEmbeddable *embeddable, CORBA_Object corba_object,
				CORBA_Environment *ev, gpointer data)
{
	gnome_object_destroy (GNOME_OBJECT (embeddable));
}

/*
 * The view encounters a fatal corba exception.
 */
static void
view_system_exception_cb (GnomeView *view, CORBA_Object corba_object,
			  CORBA_Environment *ev, gpointer data)
{
	gnome_object_destroy (GNOME_OBJECT (view));
}

/*
 * BonoboObject construction functions.
 */
static GnomeView *
view_factory (GnomeEmbeddable *embeddable, const GNOME_ViewFrame view_frame, void *data)
{
        GnomeView         *view;
	embeddable_data_t *embeddable_data = data;
	view_data_t       *view_data = g_new0 (view_data_t, 1);

	view_data->canvas = gnome_canvas_new ();
	gtk_widget_set_usize (GTK_WIDGET (view_data->canvas), 256, 256);

	view_data->canvas_item =
	  gnome_canvas_item_new (gnome_canvas_root (GNOME_CANVAS (view_data->canvas)),
				 item_audio_get_type (),
				 "EmbeddableData", embeddable_data,
				 /*				 "height", 100.0,*/
				 NULL );
	gnome_canvas_set_scroll_region (GNOME_CANVAS (view_data->canvas),
					0, 0,
					embeddable_data->sound_len, 256);
	view_data->hscroll = gtk_hscrollbar_new (gtk_layout_get_hadjustment
						 (GTK_LAYOUT (view_data->canvas)));

	view_data->vbox = gtk_vbox_new (0, FALSE);
	gtk_box_pack_start (GTK_BOX (view_data->vbox), view_data->canvas, TRUE, TRUE, 0);
	gtk_box_pack_start (GTK_BOX (view_data->vbox), view_data->hscroll, FALSE, FALSE, 0);
        gtk_widget_show_all (view_data->vbox);

        view = gnome_view_new (view_data->vbox);
	gtk_signal_connect (GTK_OBJECT (view), "destroy",
			    GTK_SIGNAL_FUNC (destroy_view), view_data);

	gtk_signal_connect (GTK_OBJECT (view), "system_exception",
			    GTK_SIGNAL_FUNC (view_system_exception_cb),
			    view_data);

	gtk_object_set_data (GTK_OBJECT (view), "view_data",
			     view_data);

        return view;
} /* view_factory */


static void
embeddable_destroy_cb (GnomeEmbeddable *embeddable, embeddable_data_t *embeddable_data)
{
	if (embeddable_data->sound)
		g_free (embeddable_data->sound);
	embeddable_data->sound     = NULL;
	embeddable_data->sound_len = 0;

	g_free (embeddable_data); 

	running_objects--;
	if (running_objects > 0)
		return;
	/*
	 * When last object has gone unref the factory & quit.
	 */
	gnome_object_unref (GNOME_OBJECT (factory));
	gtk_main_quit ();
}

static GnomeObject *
embeddable_factory (GnomeEmbeddableFactory *this,  void *data)
{
	GnomeEmbeddable *embeddable;
	GnomePersistStream *stream;
	GnomeProgressiveDataSink *psink;
	embeddable_data_t *embeddable_data = data;

	embeddable_data = g_new0 (embeddable_data_t, 1);
	if (!embeddable_data)
		return NULL;
	
	embeddable_data->sound = NULL;
	embeddable_data->sound_len = 0;
	embeddable_data->update_callback_id = 0;
  
	/*
	 * Creates the Embeddable server
	 */
	embeddable = gnome_embeddable_new (view_factory, embeddable_data);
	if (embeddable == NULL) {
		g_free (embeddable_data);
		return NULL;
	}

	embeddable_data->embeddable = embeddable;

	/*
	 * Register the GNOME::PersistStream interface.
	 */
	stream = gnome_persist_stream_new (pstream_load,
					   pstream_save,
					   embeddable_data);
	if (stream == NULL) {
		gnome_object_unref (GNOME_OBJECT (embeddable));
		g_free (embeddable_data);
		return NULL;
	}

	gnome_object_add_interface (GNOME_OBJECT (embeddable),
				    GNOME_OBJECT (stream));

	/*
	 * Register the GNOME::ProgressiveDataSink interface.
	 */
	psink = gnome_progressive_data_sink_new (progressive_start,
						 NULL, /* progressive_end */
						 progressive_add_data,
						 NULL, /* progressive_set_size */
						 embeddable_data);
	
	if (psink == NULL) {
		gnome_object_unref (GNOME_OBJECT (embeddable));
		g_free (embeddable_data);
		return NULL;
	}

	running_objects++;
	gnome_object_add_interface (GNOME_OBJECT (embeddable),
				    GNOME_OBJECT (psink));

	gtk_signal_connect (GTK_OBJECT (embeddable), "destroy",
			    GTK_SIGNAL_FUNC (embeddable_destroy_cb),
			    embeddable_data);

	gtk_signal_connect (GTK_OBJECT (embeddable), "system_exception",
			    GTK_SIGNAL_FUNC (embeddable_system_exception_cb),
			    embeddable_data);

	return (GnomeObject *)embeddable;
} /* embeddable_factory */

static void
init_bonobo_audio_ulaw_factory (void)
{
	factory = gnome_embeddable_factory_new (
		"embeddable-factory:audio-ulaw",
		 embeddable_factory, NULL);
} /* init_bonobo_audio_ulaw_factory */

static void
init_server_factory (int argc, char **argv)
{
	CORBA_Environment  ev;
	CORBA_ORB          orb;

	CORBA_exception_init (&ev);

	gnome_CORBA_init_with_popt_table (
		"bonobo-audio-ulaw", VERSION,
		&argc, argv, NULL, 0, NULL, GNORBA_INIT_SERVER_FUNC, &ev);

	color_init();

	orb = gnome_CORBA_ORB ();
	if (bonobo_init (orb, NULL, NULL) == FALSE)
		g_error (_("I could not initialize Bonobo"));

	CORBA_exception_free (&ev);
} /* init_server_factory */

int
main (int argc, char **argv)
{
	init_server_factory (argc, argv);

	init_bonobo_audio_ulaw_factory ();

	bonobo_main ();

	return 0;
} /* main */
