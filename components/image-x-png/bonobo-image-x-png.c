/*
 * image/x-png BonoboObject.
 *
 * Author:
 *   Miguel de Icaza (miguel@gnu.org)
 *
 * TODO:
 *   1. Progressive loading.
 *   2. Do not display more than required
 *   3. Queue request-resize on image size change/load
 */
#include <config.h>
#include <gnome.h>
#include <libgnorba/gnorba.h>
#include <bonobo/gnome-bonobo.h>
#include <png.h>

CORBA_Environment ev;
CORBA_ORB orb;

/*
 * BonoboObject data
 */
typedef struct {
	GnomeEmbeddable *bonobo_object;

	ArtPixBuf *image;

	GList *views;
} bonobo_object_data_t;

/*
 * View data
 */
typedef struct {
	double                scale;
	bonobo_object_data_t *bonobo_object_data;
	GtkWidget            *drawing_area;
	ArtPixBuf            *scaled;
} view_data_t;

/*
 * Releases an image
 */
static void
release_image (bonobo_object_data_t *cdata)
{
	if (!cdata->image)
		return;
	
	g_free (cdata->image->pixels);
	art_pixbuf_free_shallow (cdata->image);
	cdata->image = NULL;
}

/*
 * Callback for reading from a stream.
 */
static void
stream_read (png_structp png_ptr, png_bytep data, png_size_t size)
{
	GNOME_Stream stream = png_get_io_ptr (png_ptr);
	CORBA_Environment ev;
	GNOME_Stream_iobuf *buffer;
	
	CORBA_exception_init (&ev);

	buffer = GNOME_Stream_iobuf__alloc ();

	GNOME_Stream_read (stream, size, &buffer, &ev);
	memcpy (data, buffer->_buffer, buffer->_length);
	CORBA_free (buffer);

	CORBA_exception_free (&ev);
}

/*
 * Callback for writing to a stream.
 */
static void
stream_write (png_structp png_ptr, png_bytep data, png_size_t size)
{
	GNOME_Stream stream = png_get_io_ptr (png_ptr);
	GNOME_Stream_iobuf buffer;

	buffer._buffer = data;
	buffer._length = size;
	CORBA_sequence_set_release(&buffer, CORBA_FALSE);

	GNOME_Stream_write (stream, &buffer, &ev);

} /* stream_write */

static ArtPixBuf *
get_pixbuf (view_data_t *view_data)
{
	g_return_val_if_fail (view_data != NULL, NULL);

	if (view_data->scaled)
		return view_data->scaled;
	else {
		bonobo_object_data_t *bonobo_object_data;
		bonobo_object_data = view_data->bonobo_object_data;

		g_return_val_if_fail (bonobo_object_data != NULL, NULL);

		return bonobo_object_data->image;
	}
}

static void
redraw_view (view_data_t *view_data, GdkRectangle *rect)
{
	ArtPixBuf *pixbuf = get_pixbuf (view_data);

	g_return_if_fail (pixbuf != NULL);

	/*
	 * Do not draw outside the region that we know how to display
	 */
	if (rect->x > pixbuf->width)
		return;

	if (rect->y > pixbuf->height)
		return;

	/*
	 * Clip the draw region
	 */
	if (rect->x + rect->width > pixbuf->width)
		rect->width = pixbuf->width - rect->x;

	if (rect->y + rect->height > pixbuf->height)
		rect->height = pixbuf->height - rect->y;

	/*
	 * Draw the exposed region.
	 */
	if (pixbuf->has_alpha)
		gdk_draw_rgb_32_image (view_data->drawing_area->window,
				       view_data->drawing_area->style->white_gc,
				       rect->x, rect->y,
				       rect->width,
				       rect->height,
				       GDK_RGB_DITHER_NORMAL,
				       pixbuf->pixels + (pixbuf->rowstride * rect->y + rect->x * 4),
				       pixbuf->rowstride);
	else
		gdk_draw_rgb_image (view_data->drawing_area->window,
				    view_data->drawing_area->style->white_gc,
				    rect->x, rect->y,
				    rect->width,
				    rect->height,
				    GDK_RGB_DITHER_NORMAL,
				    pixbuf->pixels + (pixbuf->rowstride * rect->y + rect->x * 3),
				    pixbuf->rowstride);
}

static void
configure_size (view_data_t *view_data, GdkRectangle *rect)
{
	ArtPixBuf *pixbuf = get_pixbuf (view_data);

	g_return_if_fail (pixbuf != NULL);

	gtk_widget_set_usize (
		view_data->drawing_area,
		pixbuf->width,
		pixbuf->height);

	rect->x = 0;
	rect->y = 0;
	rect->width  = pixbuf->width;
	rect->height = pixbuf->height;
}

static void
redraw_all (bonobo_object_data_t *bonobo_object_data)
{
	GList *l;
	
	for (l = bonobo_object_data->views; l; l = l->next){
		GdkRectangle rect;
		view_data_t *view_data = l->data;

		configure_size (view_data, &rect);
		
		redraw_view (view_data, &rect);
	}
}

/*
 * Loads the current image into the GNOME_Stream.
 */
static int
save_image (GnomePersistStream *ps, GNOME_Stream stream, void *data)
{
	bonobo_object_data_t *bonobo_object_data = data;
	unsigned char **lines = NULL;
	png_structp png_ptr;
	png_infop info_ptr;
	int p, i;

	png_ptr = png_create_write_struct (PNG_LIBPNG_VER_STRING,
					   NULL, NULL, NULL);

	if (png_ptr == NULL)
		goto out1;

	info_ptr = png_create_info_struct (png_ptr);
	if (info_ptr == NULL)
		goto out2;

	if (setjmp (png_ptr->jmpbuf))
		goto out3;

	png_set_filter (png_ptr, 0, PNG_FILTER_NONE);

	png_set_IHDR (png_ptr, info_ptr,
		      bonobo_object_data->image->width,
		      bonobo_object_data->image->height,
		      bonobo_object_data->image->bits_per_sample,
		      PNG_COLOR_TYPE_RGB_ALPHA,
		      PNG_INTERLACE_NONE,
		      PNG_COMPRESSION_TYPE_DEFAULT,
		      PNG_FILTER_TYPE_DEFAULT);

	png_set_write_fn (png_ptr, stream, stream_write, NULL);

	/*
	 * Write the image header.
	 */
	png_write_info (png_ptr, info_ptr);

	/*
	 * Setup the array of row pointers.
	 */
	lines = g_new (unsigned char *, bonobo_object_data->image->height);
	if (lines == NULL)
		goto out2;

	for (p = 0, i = 0; i < bonobo_object_data->image->height; i++) {
		lines [i] = &bonobo_object_data->image->pixels [p];
		p += bonobo_object_data->image->rowstride;
	}

	png_write_image (png_ptr, lines);

	return 0;
	
 out3:
	if (lines)
		g_free (lines);
	
 out2:
	png_destroy_write_struct (&png_ptr, NULL);

 out1:
	return -1;
} /* save_image */

/*
 * Loads an Image from a GNOME_Stream
 */
static int
load_image_from_stream (GnomePersistStream *ps, GNOME_Stream stream, void *data)
{
	bonobo_object_data_t *bonobo_object_data = data;
	png_structp png_ptr;
	png_infop info_ptr;
	png_uint_32 width, height;
	int bit_depth, color_type, interlace_type;
	int rowstride;
	unsigned char **lines;
	char *buffer;
	int p, i;

	lines = NULL;
	buffer = NULL;

	/* 1. Release an old image */
	release_image (bonobo_object_data);

	png_ptr = png_create_read_struct (PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	if (png_ptr == NULL)
		goto out1;
		
	info_ptr = png_create_info_struct (png_ptr);
	if (info_ptr == NULL)
		goto out2;

	/*
	 * Catch exceptions here
	 */
	if (setjmp (png_ptr->jmpbuf))
		goto out4;
	
	png_set_read_fn (png_ptr, stream, stream_read);

	png_read_info (png_ptr, info_ptr);
	png_get_IHDR (png_ptr, info_ptr,
		      &width, &height,
		      &bit_depth, &color_type,
		      &interlace_type, NULL, NULL);
	if (color_type == PNG_COLOR_TYPE_PALETTE)
		png_set_expand (png_ptr);
	if (png_get_valid (png_ptr, info_ptr, PNG_INFO_tRNS))
		png_set_expand(png_ptr);

	png_set_strip_16 (png_ptr);
	png_set_packing (png_ptr);
	
	if (color_type == PNG_COLOR_TYPE_GRAY_ALPHA ||
	    color_type == PNG_COLOR_TYPE_RGB_ALPHA){

		buffer = g_malloc (width * height * 4);
		rowstride = width * 4;
		bonobo_object_data->image = art_pixbuf_new_rgba (buffer, width, height, rowstride);
	} else {
		buffer = g_malloc (width * height * 3);
		rowstride = width * 3;
		bonobo_object_data->image = art_pixbuf_new_rgb (buffer, width, height, rowstride);
	}

	/*
	 * Read the image here
	 */
	lines = g_new (unsigned char *, height);
	if (lines == NULL)
		goto out3;

	for (p = 0, i = 0; i < height; i++){
		lines [i] = &buffer [p];
		p += rowstride;
	}
	png_read_image (png_ptr, lines);
	png_destroy_read_struct (&png_ptr, &info_ptr, NULL);
	g_free (lines);
	redraw_all (bonobo_object_data);
	return 0;

 out4:
	if (lines)
		g_free (lines);
	
 out3:
	if (buffer)
		g_free (buffer);

	if (bonobo_object_data->image){
		art_pixbuf_free_shallow (bonobo_object_data->image);
		bonobo_object_data->image = NULL;
	}
 out2:
        png_destroy_read_struct (&png_ptr, NULL, NULL);
 out1:
	return -1;
}

static void
destroy_view (GnomeView *view, view_data_t *view_data)
{
	view_data->bonobo_object_data->views = g_list_remove (view_data->bonobo_object_data->views, view_data);
	gtk_object_unref (GTK_OBJECT (view_data->drawing_area));

	g_free (view_data);
}

static int
drawing_area_exposed (GtkWidget *widget, GdkEventExpose *event, view_data_t *view_data)
{
	if (!view_data->bonobo_object_data->image)
		return TRUE;
	
	redraw_view (view_data, &event->area);

	return TRUE;
}

static GnomeView *
view_factory (GnomeEmbeddable *bonobo_object,
	      const GNOME_ViewFrame view_frame,
	      void *data)
{
        GnomeView *view;
	bonobo_object_data_t *bonobo_object_data = data;
	view_data_t *view_data = g_new (view_data_t, 1);

	view_data->scale = 1.0;
	view_data->bonobo_object_data = bonobo_object_data;
	view_data->drawing_area = gtk_drawing_area_new ();
	view_data->scaled = NULL;

	gtk_signal_connect (
		GTK_OBJECT (view_data->drawing_area),
		"expose_event",
		GTK_SIGNAL_FUNC (drawing_area_exposed), view_data);

        gtk_widget_show (view_data->drawing_area);
        view = gnome_view_new (view_data->drawing_area);

	gtk_signal_connect (
		GTK_OBJECT (view), "destroy",
		GTK_SIGNAL_FUNC (destroy_view), view_data);

	bonobo_object_data->views = g_list_prepend (bonobo_object_data->views,
						view_data);

        return view;
}

static GnomeObject *
bonobo_object_factory (GnomeEmbeddableFactory *this, void *data)
{
	GnomeEmbeddable *bonobo_object;
	GnomePersistStream *stream;
	bonobo_object_data_t *bonobo_object_data;

	bonobo_object_data = g_new0 (bonobo_object_data_t, 1);
	if (!bonobo_object_data)
		return NULL;

	/*
	 * Creates the BonoboObject server
	 */
	bonobo_object = gnome_embeddable_new (view_factory, bonobo_object_data);
	if (bonobo_object == NULL){
		g_free (bonobo_object_data);
		return NULL;
	}

	/*
	 * Interface GNOME::PersistStream 
	 */
	stream = gnome_persist_stream_new (load_image_from_stream,
					   save_image,
					   bonobo_object_data);
	if (stream == NULL) {
		gtk_object_unref (GTK_OBJECT (bonobo_object));
		g_free (bonobo_object_data);
		return NULL;
	}

	bonobo_object_data->bonobo_object = bonobo_object;
	bonobo_object_data->image         = NULL;

	/*
	 * Bind the interfaces
	 */
	gnome_object_add_interface (GNOME_OBJECT (bonobo_object),
				    GNOME_OBJECT (stream));
	return (GnomeObject *) bonobo_object;
}

static void
init_bonobo_image_x_png_factory (void)
{
	GnomeEmbeddableFactory *factory;
	
	factory = gnome_embeddable_factory_new (
		"bonobo-object-factory:image-x-png",
		bonobo_object_factory, NULL);
}

static void
init_server_factory (int argc, char **argv)
{
	gnome_CORBA_init_with_popt_table (
		"bonobo-image-x-png", "1.0",
		&argc, argv, NULL, 0, NULL, GNORBA_INIT_SERVER_FUNC, &ev);

	if (bonobo_init (CORBA_OBJECT_NIL, CORBA_OBJECT_NIL, CORBA_OBJECT_NIL) == FALSE)
		g_error (_("I could not initialize Bonobo"));
}

int
main (int argc, char *argv [])
{
	CORBA_exception_init (&ev);

	init_server_factory (argc, argv);
	init_bonobo_image_x_png_factory ();

	gtk_widget_set_default_colormap (gdk_rgb_get_cmap ());
	gtk_widget_set_default_visual (gdk_rgb_get_visual ());

	bonobo_main ();
	
	CORBA_exception_free (&ev);

	return 0;
}
