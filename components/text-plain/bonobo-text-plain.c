/*
 * An embeddable Bonobo component to display text/plain.
 *
 * Author:
 *   Nat Friedman (nat@nat.org)
 *
 */
#include <config.h>
#include <gnome.h>
#include <libgnorba/gnorba.h>
#include <bonobo/gnome-bonobo.h>

/* XPM */
static char * bolt_xpm[] = {
"10 18 4 1",
" 	c None",
".	c #000000",
"+	c #F2FF00",
"@	c #000000",
"   .......",
"  .++++++.",
"  .+++++@.",
" .+++++@. ",
" .++++@.  ",
"..+++@.   ",
".+++@.... ",
".+++++++. ",
"....+++@. ",
"  .+++@.  ",
"..+++@.   ",
".++@@.    ",
".+++++.   ",
"...++@.   ",
" .++@.    ",
".++@.     ",
".+@.      ",
"...       "};

/*
 * Number of running objects
 */ 
static int running_objects = 0;

/*
 * Our embeddable factory
 */
static GnomeEmbeddableFactory *factory;

/*
 * BonoboObject data
 */
typedef struct {
	GnomeEmbeddable *bonobo_object;

	gchar             *text;
	guint              text_len;
	
	/* TRUE if data is coming in from ProgressiveDataSink. */
	gboolean           progressive_data;

	/*
	 * The total amount of the text data we're expecting from the
	 * ProgressiveDataSink interface.
	 */
	size_t             total_size;

	/* List of this component's GNOME::Views. */
	GList *views;
} bonobo_object_data_t;

/*
 * View data
 */
typedef struct {
	GnomeView        *view;

	bonobo_object_data_t *bonobo_object_data;

	GtkWidget	 *progress;
	GtkWidget        *text;
	guint		  text_len;

	gboolean	  active;

	/* Internal widgets. */
	GtkWidget	 *sw;
	GtkWidget	 *table;
} view_data_t;

/*
 * Utility functions.
 */
static void
destroy_view (view_data_t *view_data)
{
	view_data->bonobo_object_data->views =
		g_list_remove (view_data->bonobo_object_data->views,
			       view_data);

	g_free (view_data);
} /* destroy_view */

static void
free_text (bonobo_object_data_t *bonobo_object_data)
{
	if (bonobo_object_data->text != NULL)
		g_free (bonobo_object_data->text);
	bonobo_object_data->text = NULL;
	bonobo_object_data->text_len = 0;
} /* free_text */

static void
blank_all_views (bonobo_object_data_t *bonobo_object_data)
{
	GList *l;

	for (l = bonobo_object_data->views; l; l = l->next)
	{
		view_data_t *view_data = l->data;

		if (view_data->text_len == 0)
			continue;

		gtk_text_freeze (GTK_TEXT (view_data->text));

		/*
		 * Seek to the end of the text and delete it all.
		 */
		gtk_text_set_point (GTK_TEXT (view_data->text),
				    view_data->text_len);

		gtk_text_backward_delete (GTK_TEXT (view_data->text),
					  view_data->text_len);
		

		/*
		 * FIXME: This is a nasty hack.  I can't figure out how
		 * to get a GtkText widget to clear its focus area without
		 * doing something like this, though.
		 */
		gdk_window_clear_area (GTK_TEXT (view_data->text)->text_area,
				       0, 0,
				       view_data->text->allocation.width,
				       view_data->text->allocation.height);


		gtk_text_thaw (GTK_TEXT (view_data->text));

#ifndef NO_PROGRESS_METER
		/* Hide the progress meter. */
		gtk_widget_hide (view_data->progress);
#endif /* ! NO_PROGRESS_METER */

		view_data->text_len = 0;
	}
} /* blank_all_views */

static void
update_all_views (bonobo_object_data_t *bonobo_object_data)
{
	GList *l;

	/*
	 * 1. Erase all the views.
	 */
	blank_all_views (bonobo_object_data);

	/*
	 * 2. Draw the new text data into all the views.
	 */
	for (l = bonobo_object_data->views; l; l = l->next)
	{
		view_data_t *view_data = l->data;

		gtk_text_freeze (GTK_TEXT (view_data->text));

		/*
		 * 2.1 Insert the new text.
		 */
		gtk_text_insert (GTK_TEXT (view_data->text),
			         NULL, NULL, NULL,
				 bonobo_object_data->text,
				 bonobo_object_data->text_len);

		view_data->text_len = bonobo_object_data->text_len;

		/*
		 * 2.2 Jump back to the beginning so that the view is
		 * positioned at the beginning of the text.
		 */
		gtk_text_set_point (GTK_TEXT (view_data->text), 0);

		gtk_text_thaw (GTK_TEXT (view_data->text));
	}
} /* update_all_views */
		  
static void
progressive_update (bonobo_object_data_t *bonobo_object_data,
		    char *buff,
		    size_t count)
{
	GList *l;

	/*
	 * 1. Append the new text data to the bonobo_object's text buffer.
	 */
	bonobo_object_data->text = g_realloc (bonobo_object_data->text,
					  bonobo_object_data->text_len
					  + count);

	memcpy (bonobo_object_data->text + bonobo_object_data->text_len,
		buff, count);

	bonobo_object_data->text_len += count;

	/*
	 * 2. Append the new text data to the end of each view.
	 */
	for (l = bonobo_object_data->views; l; l = l->next)
	{
		view_data_t *view_data = l->data;

		gtk_text_freeze (GTK_TEXT (view_data->text));

		/*
		 * 2.1 Jump to the end and append the new text.
		 */
		gtk_text_set_point (GTK_TEXT (view_data->text),
				    view_data->text_len);

		gtk_text_insert (GTK_TEXT (view_data->text),
			         NULL, NULL, NULL,
				 buff, count);

		view_data->text_len += count;

		/*
		 * 2.2 Jump back to the beginning so that when the
		 * file is done loading, the view is positioned at the
		 * beginning.
		 */
		gtk_text_set_point (GTK_TEXT (view_data->text), 0);

#ifndef NO_PROGRESS_METER
		gtk_progress_bar_update (
			GTK_PROGRESS_BAR (view_data->progress),
			(float) bonobo_object_data->text_len /
			bonobo_object_data->total_size);
								 
#endif /* ! NO_PROGRESS_METER */

		gtk_text_thaw (GTK_TEXT (view_data->text));
	}

} /* progressive_update */

/*
 * Callbacks attached to signals.
 */
static gboolean
view_destroy_cb (GnomeView *view_ignored, view_data_t *view_data)
{
	destroy_view (view_data);

	return FALSE;
} /* view_destroy_cb */

static void
view_system_exception_cb (GnomeView *view, CORBA_Object corba_object,
			  CORBA_Environment *ev, gpointer data)
{
	g_warning ("View system exception");
	gnome_object_destroy (GNOME_OBJECT (view));
}

static void
embeddable_system_exception_cb (GnomeEmbeddable *embeddable, CORBA_Object corba_object,
				CORBA_Environment *ev, gpointer data)
{
	g_warning ("Embeddable system exception");
	gnome_object_destroy (GNOME_OBJECT (embeddable));
}

static void
bonobo_object_destroy_cb (GnomeEmbeddable *bonobo_object,
			  bonobo_object_data_t *bonobo_object_data)
{
	free_text (bonobo_object_data);
	g_free (bonobo_object_data);

	running_objects--;
	if (running_objects != 0)
		return;

	if (factory) {
		gnome_object_unref (GNOME_OBJECT (factory));
		factory = NULL;
		gtk_main_quit ();
	}
}


static gboolean
text_inserted_cb (GtkText *text_widget,
		  const gchar *text,
		  gint length,
		  gint *position,
		  view_data_t *view_data_changed)
{
	bonobo_object_data_t *bonobo_object =
		view_data_changed->bonobo_object_data;
	GList *l;

	if (length == 0)
		return FALSE;

	view_data_changed->text_len += length;

	/*
	 * Update the BonoboObject's representation of the data being
	 * displayed.
	 */
	bonobo_object->text = g_realloc (bonobo_object->text,
					 bonobo_object->text_len + length);

	memmove (bonobo_object->text + *position + length,
		 bonobo_object->text + *position,
		 bonobo_object->text_len - *position);
	bonobo_object->text_len += length;

	memcpy (bonobo_object->text + *position, text, length);

	/*
	 * Update the views.
	 */
	for (l = bonobo_object->views; l; l = l->next)
	{
		view_data_t *view_data = l->data;

		if (view_data == view_data_changed)
			continue;

		gtk_text_freeze (GTK_TEXT (view_data->text));

		gtk_text_set_point (GTK_TEXT (view_data->text), *position);
		gtk_text_insert (GTK_TEXT (view_data->text),
				 NULL, NULL, NULL,
				 text, length);
		view_data->text_len += length;
		gtk_text_set_point (GTK_TEXT (view_data->text), 0);

		gtk_text_thaw (GTK_TEXT (view_data->text));
	}
			
	return FALSE;
} /* text_inserted_cb */

static gboolean
text_deleted_cb (GtkText *text,
		 gint start_pos, gint end_pos,
		 view_data_t *view_data_changed)
{
	bonobo_object_data_t *bonobo_object =
		view_data_changed->bonobo_object_data;
	GList *l;

	view_data_changed->text_len -= end_pos - start_pos;

	/*
	 * Update the BonoboObject's representation of the data being
	 * displayed.
	 */
	memmove (bonobo_object->text + start_pos,
		 bonobo_object->text + end_pos,
		 bonobo_object->text_len - end_pos);
	bonobo_object->text_len -= end_pos - start_pos;
	bonobo_object->text = (char *) g_realloc (bonobo_object->text,
						  bonobo_object->text_len);

	/*
	 * Update the views.
	 */
	for (l = bonobo_object->views; l; l = l->next)
	{
		view_data_t *view_data = l->data;

		if (view_data == view_data_changed)
			continue;

		gtk_text_freeze (GTK_TEXT (view_data->text));

		gtk_text_set_point (GTK_TEXT (view_data->text), end_pos);
		gtk_text_backward_delete (GTK_TEXT (view_data->text),
					  end_pos - start_pos);
		view_data->text_len -= end_pos - start_pos;
		gtk_text_set_point (GTK_TEXT (view_data->text), 0);

		gtk_text_thaw (GTK_TEXT (view_data->text));
	}
			
	return FALSE;
} /* text_deleted_cb */

static gint
clear_text_cb (GnomeUIHandler *uih, void *data,
	       const char *path)
{
	view_data_t *view_data = (view_data_t *) data;


	blank_all_views (view_data->bonobo_object_data);
	free_text (view_data->bonobo_object_data);

	return FALSE;
} /* clear_text_cb */


/*
 * GNOME::BonoboObject
 *
 * These functions implement the GNOME::BonoboObject interface.
 */

static void
create_view_menus (view_data_t *view_data)
{
	GNOME_UIHandler remote_uih;
	GnomeUIHandler *uih;
	char label[256];

	/*
	 * Register with the UI Handler and add some menu entries.
	 */
	uih = gnome_view_get_ui_handler (view_data->view);
	remote_uih = gnome_view_get_remote_ui_handler (view_data->view);
	if (remote_uih == CORBA_OBJECT_NIL)
		return;

	gnome_ui_handler_set_container (uih, remote_uih);
		
	gnome_ui_handler_menu_new_separator (uih, "/text\\/plain/separator1", -1);

	snprintf (label, sizeof (label), "Clear Text (containee-added item for view %p)", view_data);
	gnome_ui_handler_menu_new_item (uih, "/text\\/plain/Clear text", 
					label, NULL, -1, GNOME_UI_HANDLER_PIXMAP_XPM_DATA,
					bolt_xpm, 0, 0, clear_text_cb, view_data);

	gnome_ui_handler_menu_new_item (uih, "/text\\/plain/Clear text", 
					label, NULL, -1, GNOME_UI_HANDLER_PIXMAP_XPM_DATA,
					bolt_xpm, 0, 0, clear_text_cb, view_data);

	gnome_ui_handler_create_toolbar (uih, "Common");

	gnome_ui_handler_toolbar_new_item (uih, "/Common/item1", "item 1", NULL, -1,
					   GNOME_UI_HANDLER_PIXMAP_XPM_DATA, bolt_xpm,
					   0, 0, NULL, NULL);
}

static void
destroy_view_menus (view_data_t *view_data)
{
	GnomeView *view = view_data->view;
	GnomeUIHandler *uih;

	uih = gnome_view_get_ui_handler (view);

	gnome_ui_handler_unset_container (uih);
}

static void
view_activate_cb (GnomeView *view, gboolean activate, gpointer data)
{
	view_data_t *view_data = (view_data_t *) data;

	/*
	 * If we have been activated, create our menus.  If we have
	 * been deactivated, destroy them.
	 */
	if (activate)
		create_view_menus (view_data);
	else
		destroy_view_menus (view_data);

	/*
	 * Notify the ViewFrame that we accept to be activated or
	 * deactivated (we are an acquiescent GnomeView, yes we are).
	 */
	gnome_view_activate_notify (view, activate);
}

/*
 * GNOME::PersistStream
 *
 * These two functions implement the GNOME::PersistStream load and
 * save methods which allow data to be loaded into and out of the
 * BonoboObject.
 */
static int
stream_read (GNOME_Stream stream, bonobo_object_data_t *bonobo_object_data)
{
	GNOME_Stream_iobuf *buffer;
	CORBA_long bytes_read;
	CORBA_Environment ev;

	CORBA_exception_init (&ev);

	do {

#define READ_CHUNK_SIZE 65536
		bytes_read = GNOME_Stream_read (stream, READ_CHUNK_SIZE,
						&buffer, &ev);

		bonobo_object_data->text = g_realloc (bonobo_object_data->text,
						      bonobo_object_data->text_len
						      + buffer->_length);

		memcpy (bonobo_object_data->text
			+ bonobo_object_data->text_len,
			buffer->_buffer, buffer->_length);

		bonobo_object_data->text_len += buffer->_length;

		CORBA_free (buffer);
	} while (bytes_read > 0);

	CORBA_exception_free (&ev);

	if (bytes_read < 0)
		return -1;

	return 0;
} /* stream_read */

/*
 * This function implements the GNOME::PersistStream:load method.
 */
static int
pstream_load (GnomePersistStream *ps, GNOME_Stream stream,
	      void *data)
{
	bonobo_object_data_t *bonobo_object_data = data;

	/*
	 * 1. Free the old text data and blank the views.
	 */
	blank_all_views (bonobo_object_data);
	free_text (bonobo_object_data);

	/*
	 * 2. Read the new text data.
	 */
	if (stream_read (stream, bonobo_object_data) < 0)
		return -1; /* This will raise an exception. */

	/*
	 * 3. Update the displays.
	 */
	update_all_views (bonobo_object_data);

	return 0;
} /* pstream_load */

/*
 * This function implements the GNOME::PersistStream:save method.
 */
static int
pstream_save (GnomePersistStream *ps, GNOME_Stream stream,
	      void *data)
{
	bonobo_object_data_t *bonobo_object_data = data;
	GNOME_Stream_iobuf *buffer;
	size_t pos;
	CORBA_Environment ev;

	CORBA_exception_init (&ev);
	
	/*
	 * Write the text data into the stream.
	 *
	 * FIXME: Do we really _have_ to double-buffer the text?
	 */
	buffer = GNOME_Stream_iobuf__alloc ();

	data = CORBA_sequence_CORBA_octet_allocbuf (bonobo_object_data->text_len);
	memcpy (data, bonobo_object_data->text, bonobo_object_data->text_len);

	buffer->_buffer = data;
	buffer->_length = bonobo_object_data->text_len;

	pos = 0;
	while (pos < bonobo_object_data->text_len) {
		CORBA_long bytes_read;

		bytes_read = GNOME_Stream_write (stream, buffer, &ev);

		if (ev._major != CORBA_NO_EXCEPTION) {
			CORBA_free (buffer);
			CORBA_free (data);
			CORBA_exception_free (&ev);
			return -1;
		}

		pos += bytes_read;
	}

	CORBA_free (buffer);
	CORBA_free (data);
	CORBA_exception_free (&ev);
	
	return 0;
} /* pstream_save */

/*
 * GNOME::ProgressiveDataSink
 *
 * These functions implement the ProgressiveDataSink interface
 * methods, which are used to send a slow stream of data to the widget
 * and have it update its views progressively.
 */

static int
progressive_start (GnomeProgressiveDataSink *psink, void *data)
{
	bonobo_object_data_t * bonobo_object_data =
		(bonobo_object_data_t *) data;
	GList *l;

	bonobo_object_data->progressive_data = TRUE;
	
	blank_all_views (bonobo_object_data);
	free_text (bonobo_object_data);

#ifndef NO_PROGRESS_METER
	for (l = bonobo_object_data->views; l; l = l->next)
	{
		view_data_t *view_data = l->data;

		gtk_progress_bar_update (
			GTK_PROGRESS_BAR (view_data->progress), 0.0);
		gtk_widget_show (view_data->progress);
	}
#endif /* ! NO_PROGRESS_METER */	

	return 0;
} /* progressive_start */

static int
progressive_end (GnomeProgressiveDataSink *psink, void *data)
{
	bonobo_object_data_t * bonobo_object_data =
		(bonobo_object_data_t *) data;
	GList *l;

	bonobo_object_data->progressive_data = FALSE;

#ifndef NO_PROGRESS_METER
	for (l = bonobo_object_data->views; l; l = l->next)
	{
		view_data_t *view_data = l->data;

		gtk_widget_hide (view_data->progress);
	}
#endif /* ! NO_PROGRESS_METER */

	return 0;
} /* progressive_end */

static int
progressive_set_size (GnomeProgressiveDataSink *psink,
		      CORBA_long size, void *data)
{
	bonobo_object_data_t * bonobo_object_data =
		(bonobo_object_data_t *) data;

	bonobo_object_data->total_size = size;

	return 0;
} /* progressive_set_size */

static int
progressive_add_data (GnomeProgressiveDataSink *psink,
		      const GNOME_ProgressiveDataSink_iobuf *buffer,
		      void *data)
{
	bonobo_object_data_t *bonobo_object_data =
		(bonobo_object_data_t *) data;

	progressive_update (bonobo_object_data, (char *) buffer->_buffer,
			    (size_t) buffer->_length);

	return 0;
} /* progressive_add_data */

/*
 * BonoboObject construction functions.
 */
static GnomeView *
view_factory (GnomeEmbeddable *bonobo_object,
	      const GNOME_ViewFrame view_frame,
	      void *data)
{
        GnomeView *view;
	GnomeUIHandler *uih;
	bonobo_object_data_t *bonobo_object_data = data;
	view_data_t *view_data;

	/*
	 * Create the private view data and the GtkText widget.
	 */
	view_data = g_new0 (view_data_t, 1);
	view_data->bonobo_object_data = bonobo_object_data;

	view_data->text = gtk_text_new (NULL, NULL);

	/*
	 * This table will contain the GtkText widget and, below it,
	 * a progress meter which will appear while progressive data
	 * is being loaded into the BonoboObject.
	 */
	view_data->table = gtk_table_new (2, 1, FALSE);

	/*
	 * Put any existing text into this new view.
	 */
	gtk_text_insert (GTK_TEXT (view_data->text),
			 NULL, NULL, NULL,
			 bonobo_object_data->text,
			 bonobo_object_data->text_len);
	view_data->text_len = bonobo_object_data->text_len;

	/*
	 * Make the widget editable and catch the relevant change
	 * signals so that we can update the other views.
	 */
	gtk_text_set_editable (GTK_TEXT (view_data->text), TRUE);
	gtk_signal_connect (GTK_OBJECT (view_data->text), "insert_text",
			    GTK_SIGNAL_FUNC (text_inserted_cb), view_data);
	gtk_signal_connect (GTK_OBJECT (view_data->text), "delete_text",
			    GTK_SIGNAL_FUNC (text_deleted_cb), view_data);


	/*
	 * Stuff it into a scrolled window.
	 */
	view_data->sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (view_data->sw),
					GTK_POLICY_AUTOMATIC,
					GTK_POLICY_AUTOMATIC);

	gtk_container_add (GTK_CONTAINER (view_data->sw), view_data->text);

	gtk_table_attach (GTK_TABLE (view_data->table), view_data->sw,
			  0, 1, 0, 1,
			  (GTK_EXPAND | GTK_FILL), (GTK_EXPAND | GTK_FILL),
			  0, 0);

	/*
	 * Now create the GtkProgress bar which will be displayed when
	 * data is being loaded into the BonoboObject with the
	 * GNOME::ProgressiveDataSink interface.
	 */
	view_data->progress = gtk_progress_bar_new ();
	gtk_table_attach (GTK_TABLE (view_data->table), view_data->progress,
			  0, 1, 1, 2,
			  (GTK_EXPAND | GTK_FILL), 0,
			  0, 0);

        gtk_widget_show_all (view_data->table);

	if (! bonobo_object_data->progressive_data)
		gtk_widget_hide (view_data->progress);

	/*
	 * Now create the GnomeView object.
	 */
        view = gnome_view_new (view_data->table);
	view_data->view = view;

	uih = gnome_ui_handler_new ();
	gnome_view_set_ui_handler (view, uih);

	gnome_view_set_view_frame (view, view_frame);

	gtk_signal_connect (GTK_OBJECT (view), "destroy",
			    GTK_SIGNAL_FUNC (view_destroy_cb), view_data);

	gtk_signal_connect (GTK_OBJECT (view), "system_exception",
			    GTK_SIGNAL_FUNC (view_system_exception_cb), view_data);

	gtk_signal_connect (GTK_OBJECT (view), "view_activate",
			    GTK_SIGNAL_FUNC (view_activate_cb), view_data);

	bonobo_object_data->views = g_list_prepend (bonobo_object_data->views,
						    view_data);
	
        return view;
} /* view_factory */

static GnomeObject *
embeddable_factory (GnomeEmbeddableFactory *this,
		    void *data)
{
	GnomeEmbeddable *bonobo_object;
	GnomePersistStream *stream;
	GnomeProgressiveDataSink *psink;
	bonobo_object_data_t *bonobo_object_data;

	bonobo_object_data = g_new0 (bonobo_object_data_t, 1);
	if (!bonobo_object_data)
		return NULL;

	bonobo_object_data->text = NULL;
	bonobo_object_data->text_len = 0;

	/*
	 * Creates the BonoboObject server
	 */
	bonobo_object = gnome_embeddable_new (view_factory,
					      bonobo_object_data);
	if (bonobo_object == NULL) {
		g_free (bonobo_object_data);
		return NULL;
	}
	running_objects++;

	gtk_signal_connect (GTK_OBJECT (bonobo_object), "destroy",
			    GTK_SIGNAL_FUNC (bonobo_object_destroy_cb),
			    bonobo_object_data);

	gtk_signal_connect (GTK_OBJECT (bonobo_object), "system_exception",
			    GTK_SIGNAL_FUNC (embeddable_system_exception_cb),
			    bonobo_object_data);

	bonobo_object_data->bonobo_object = bonobo_object;

	/*
	 * Register the GNOME::PersistStream interface.
	 */
	stream = gnome_persist_stream_new (pstream_load,
					   pstream_save,
					   bonobo_object_data);
	if (stream == NULL) {
		gtk_object_unref (GTK_OBJECT (bonobo_object));
		g_free (bonobo_object_data);
		return NULL;
	}

	gnome_object_add_interface (GNOME_OBJECT (bonobo_object),
				    GNOME_OBJECT (stream));


	/*
	 * Register the GNOME::ProgressiveDataSink interface.
	 */
	psink = gnome_progressive_data_sink_new (progressive_start,
						 progressive_end,
						 progressive_add_data,
						 progressive_set_size,
						 bonobo_object_data);
	if (psink == NULL) {
		gtk_object_unref (GTK_OBJECT (bonobo_object));
		g_free (bonobo_object_data);
		return NULL;
	}

	gnome_object_add_interface (GNOME_OBJECT (bonobo_object),
				    GNOME_OBJECT (psink));

	return GNOME_OBJECT (bonobo_object);
} /* embeddable_factory */

static void
init_bonobo_text_plain_factory (void)
{
	factory = gnome_embeddable_factory_new (
		"embeddable-factory:text-plain",
		embeddable_factory, NULL);
} /* init_bonobo_text_plain_factory */

static void
init_server_factory (int argc, char **argv)
{
	CORBA_ORB orb;
	CORBA_Environment ev;

	CORBA_exception_init (&ev);
	
	gnome_CORBA_init_with_popt_table (
		"bonobo-text-plain", VERSION,
		&argc, argv, NULL, 0, NULL, GNORBA_INIT_SERVER_FUNC, &ev);

	orb = gnome_CORBA_ORB ();
	if (bonobo_init (orb, NULL, NULL) == FALSE)
		g_error (_("I could not initialize Bonobo"));

	CORBA_exception_free (&ev);
} /* init_server_factory */

int
main (int argc, char **argv)
{
	init_server_factory (argc, argv);
	init_bonobo_text_plain_factory ();

	bonobo_main ();

	return 0;
} /* main */
