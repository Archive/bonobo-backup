/*
 * An embeddable "mines" component.
 *
 * This looks extremely useless, but so do you :-)
 *
 * Author:
 *   Michael Meeks <michael@imaginator.com>
 *
 */

#include <config.h>
#include <gnome.h>
#include <bonobo/gnome-bonobo.h>

#include "minefield.h"

typedef struct {
	GnomeEmbeddable     *embeddable;

	MineField           *data;
} embeddable_data_t;

/*
 * The per-view data.
 */
typedef struct {
	GnomeView	    *view;
	embeddable_data_t   *embeddable_data;
	
	GtkMineFieldView    *mfield;
} view_data_t;

/*
 * This callback is invoked when the GnomeEmbeddable object
 * encounters a fatal CORBA exception.
 */
static void
embeddable_system_exception_cb (GnomeEmbeddable *embeddable, CORBA_Object corba_object,
				CORBA_Environment *ev, gpointer data)
{
	gnome_object_destroy (GNOME_OBJECT (embeddable));
}

/*
 * The view encounters a fatal corba exception.
 */
static void
view_system_exception_cb (GnomeView *view, CORBA_Object corba_object,
			  CORBA_Environment *ev, gpointer data)
{
	gnome_object_destroy (GNOME_OBJECT (view));
}

static void
queue_redraw (GnomeView *view, void *data)
{
	view_data_t *view_data;

	view_data = gtk_object_get_data (GTK_OBJECT (view), "view_data");
	gtk_widget_queue_draw (GTK_WIDGET (view_data->mfield));
}

/*
 * This function updates all of an embeddable's views to reflect the
 * image data stored in the embeddable.
 */
static void
embeddable_update_all_views (embeddable_data_t *embeddable_data)
{
	GnomeEmbeddable *embeddable;

	embeddable = embeddable_data->embeddable;

	gnome_embeddable_foreach_view (embeddable, queue_redraw, NULL);
}

/*
 * This callback is envoked when the view is destroyed.  We use it to
 * free up our ancillary view-centric data structures.
 */
static void
view_destroy_cb (GnomeView *view, gpointer data)
{
	view_data_t *view_data = (view_data_t *) data;

	gtk_widget_destroy (GTK_WIDGET (view_data->mfield));
	g_free (view_data);
}

static void
view_new_game_ui_cb  (GnomeUIHandler *uih, void *user_data,
		      const char *path)
{
	view_data_t *view_data = (view_data_t *) user_data;

	minefield_restart (view_data->embeddable_data->data);
	embeddable_update_all_views (view_data->embeddable_data);
}

static void
view_new_game_cb (GnomeView *view, const char *verb_name, void *user_data)
{
	view_data_t *view_data = (view_data_t *) user_data;

	minefield_restart (view_data->embeddable_data->data);
	embeddable_update_all_views (view_data->embeddable_data);
}

/*
 * When one of our views is activated, we merge our menus
 * in with our container's menus.
 */
static void
view_create_menus (view_data_t *view_data)
{
	GNOME_UIHandler remote_uih;
	GnomeView *view = view_data->view;
	GnomeUIHandler *uih;

	/*
	 * Grab our GnomeUIHandler object.
	 */
	uih = gnome_view_get_ui_handler (view);

	/*
	 * Get our container's UIHandler server.
	 */
	remote_uih = gnome_view_get_remote_ui_handler (view);

	/*
	 * We have to deal gracefully with containers
	 * which don't have a UIHandler running.
	 */
	if (remote_uih == CORBA_OBJECT_NIL)
		return;

	/*
	 * Give our GnomeUIHandler object a reference to the
	 * container's UIhandler server.
	 */
	gnome_ui_handler_set_container (uih, remote_uih);

	/*
	 * Create our menu entries.
	 */
	gnome_ui_handler_menu_new_subtree (uih, "/Game",
					   L_("_Game"),
					   _("Change the game"),
					   1,
					   GNOME_UI_HANDLER_PIXMAP_NONE, NULL,
					   0, (GdkModifierType)0);

	gnome_ui_handler_menu_new_item    (uih,  "/Game/NewGame",
					   L_("_New game"), _("Start a new game"), -1,
					   GNOME_UI_HANDLER_PIXMAP_NONE, NULL, 0,
					   (GdkModifierType)0, view_new_game_ui_cb, (gpointer)view_data);
}

/*
 * When this view is deactivated, we must remove our menu items.
 */
static void
view_remove_menus (view_data_t *view_data)
{
	GnomeView *view = view_data->view;
	GnomeUIHandler *uih;

	uih = gnome_view_get_ui_handler (view);

	gnome_ui_handler_unset_container (uih);
}

/*
 * Clean up our supplementary GnomeEmbeddable data sturctures.
 */
static void
embeddable_destroy_cb (GnomeEmbeddable *embeddable, gpointer data)
{
	embeddable_data_t *embeddable_data = (embeddable_data_t *) data;

	if (embeddable_data->data)
		minefield_destroy (embeddable_data->data);
	embeddable_data->data = NULL;
	
	g_free (embeddable_data); 
}

static void
view_activate_cb (GnomeView *view, gboolean activate, gpointer data)
{
	view_data_t *view_data = (view_data_t *) data;

	/*
	 * The ViewFrame has just asked the View (that's us) to be
	 * activated or deactivated.  We must reply to the ViewFrame
	 * and say whether or not we want our activation state to
	 * change.  We are an acquiescent GnomeView, so we just agree
	 * with whatever the ViewFrame told us.  Most components
	 * should behave this way.
	 */
	gnome_view_activate_notify (view, activate);

	/*
	 * If we were just activated, we merge in our menu entries.
	 * If we were just deactivated, we remove them.
	 */
	if (activate)
		view_create_menus (view_data);
	else
		view_remove_menus (view_data);
}

static void update_views (GtkWidget *widget, view_data_t *view_data)
{
	/* FIXME: all but current view ! */
	embeddable_update_all_views (view_data->embeddable_data);
}

static GnomeView *
view_factory (GnomeEmbeddable *embeddable,
	      const GNOME_ViewFrame view_frame,
	      void *data)
{
	embeddable_data_t *embeddable_data = (embeddable_data_t *) data;
	view_data_t       *view_data;
	GnomeUIHandler    *uih;
	GnomeView         *view;
	GtkWidget         *vbox;

	/*
	 * Create the private view data.
	 */
	view_data = g_new0 (view_data_t, 1);
	view_data->embeddable_data = embeddable_data;

	vbox = gtk_vbox_new (TRUE, 0);
	view_data->mfield = (GtkMineFieldView *)gtk_minefield_new_view (embeddable_data->data);
	gtk_signal_connect(GTK_OBJECT (view_data->mfield), "marks_changed",
			   GTK_SIGNAL_FUNC (update_views), view_data);
	gtk_signal_connect(GTK_OBJECT (view_data->mfield), "explode",
			   GTK_SIGNAL_FUNC (update_views), view_data);
	gtk_signal_connect(GTK_OBJECT (view_data->mfield), "win",
			   GTK_SIGNAL_FUNC (update_views), view_data);
	gtk_signal_connect(GTK_OBJECT (view_data->mfield), "look",
			   GTK_SIGNAL_FUNC (update_views), view_data);
	gtk_signal_connect(GTK_OBJECT (view_data->mfield), "unlook",
			   GTK_SIGNAL_FUNC (update_views), view_data);
	

	gtk_box_pack_start (GTK_BOX (vbox), GTK_WIDGET (view_data->mfield),
			    TRUE, TRUE, 0);
	gtk_widget_show_all (vbox);

	/*
	 * Create the GnomeView object.
	 */
	view = gnome_view_new (vbox);
	view_data->view = view;
	gtk_object_set_data (GTK_OBJECT (view), "view_data", view_data);

	/*
	 * Create the GnomeUIHandler for this view.  It will be used
	 * to merge menu and toolbar items when the view is activated.
	 */
	uih = gnome_ui_handler_new ();
	gnome_view_set_ui_handler (view, uih);

	/*
	 * Register callbacks to handle the verbs
	 */
	gnome_view_register_verb (view, "NewGame", view_new_game_cb, view_data);

	/*
	 * When our container wants to activate a given view of this
	 * component, we will get the "view_activate" signal.
	 */
	gtk_signal_connect (GTK_OBJECT (view), "view_activate",
			    GTK_SIGNAL_FUNC (view_activate_cb), view_data);

	/*
	 * The "system_exception" signal is raised when the GnomeView
	 * encounters a fatal CORBA exception.
	 */
	gtk_signal_connect (GTK_OBJECT (view), "system_exception",
			    GTK_SIGNAL_FUNC (view_system_exception_cb), view_data);

	/*
	 * We'll need to be able to cleanup when this view gets
	 * destroyed.
	 */
	gtk_signal_connect (GTK_OBJECT (view), "destroy",
			    GTK_SIGNAL_FUNC (view_destroy_cb), view_data);

	return view;
}

static GnomeObject *
embeddable_factory (GnomeEmbeddableFactory *this,
		    void *data)
{
	GnomeEmbeddable *embeddable;
	embeddable_data_t *embeddable_data;

	/*
	 * Create a data structure in which we can store
	 * Embeddable-object-specific data about this document.
	 */
	embeddable_data = g_new0 (embeddable_data_t, 1);
	if (embeddable_data == NULL)
		return NULL;

	/*
	 * Create the GnomeEmbeddable object.
	 */
	embeddable = gnome_embeddable_new (view_factory, embeddable_data);

	if (embeddable == NULL) {
		g_free (embeddable_data);
		return NULL;
	}

	embeddable_data->embeddable = embeddable;
	embeddable_data->data = minefield_new ();
	minefield_set_size  (embeddable_data->data, 16, 16);
	minefield_set_mines (embeddable_data->data, 40);
	minefield_restart   (embeddable_data->data);

	/*
	 * Add some verbs to the embeddable.
	 *
	 * Verbs are simple non-paramameterized actions which the
	 * component can perform.  The GnomeEmbeddable must maintain a
	 * list of the verbs which a component supports, and the
	 * component author must register callbacks for each of his
	 * verbs with the GnomeView.
	 *
	 * The container application will then have the programmatic
	 * ability to execute the verbs on the component.  It will
	 * also provide a simple mechanism whereby the user can
	 * right-click on the component to create a popup menu
	 * listing the available verbs.
	 *
	 * We provide one simple verb whose job it is to clear the
	 * window.
	 */
	gnome_embeddable_add_verb (embeddable,
				   "NewGame",
				   _("_New Game"),
				   _("Start a new game"));

	/*
	 * If the Embeddable encounters a fatal CORBA exception, it
	 * will emit a "system_exception" signal, notifying us that
	 * the object is defunct.  Our callback --
	 * embeddable_system_exception_cb() -- destroys the defunct
	 * GnomeEmbeddable object.
	 */
	gtk_signal_connect (GTK_OBJECT (embeddable), "system_exception",
			    GTK_SIGNAL_FUNC (embeddable_system_exception_cb),
			    embeddable_data);

	/*
	 * Catch the destroy signal so that we can free up resources.
	 * When an Embeddable is destroyed, its views will
	 * automatically be destroyed.
	 */
	gtk_signal_connect (GTK_OBJECT (embeddable), "destroy",
			    GTK_SIGNAL_FUNC (embeddable_destroy_cb),
			    embeddable_data);

	return GNOME_OBJECT (embeddable);
}

static void
init_simple_mines_factory (void)
{
	GnomeEmbeddableFactory *factory;

	factory = gnome_embeddable_factory_new (
		"embeddable-factory:application-x-mines",
		embeddable_factory, NULL);
}

static void
init_server_factory (int argc, char **argv)
{
	CORBA_Environment ev;
	CORBA_ORB orb;

        bindtextdomain (PACKAGE, GNOMELOCALEDIR);
	textdomain (PACKAGE);

	CORBA_exception_init (&ev);

	gnome_CORBA_init_with_popt_table (
		"application-x-mines", VERSION,
		&argc, argv, NULL, 0, NULL, GNORBA_INIT_SERVER_FUNC, &ev);

	CORBA_exception_free (&ev);

	orb = gnome_CORBA_ORB ();
	if (bonobo_init (orb, NULL, NULL) == FALSE)
		g_error (_("Could not initialize Bonobo!"));
}
 
int
main (int argc, char **argv)
{
	/*
	 * Setup the factory.
	 */
	init_server_factory (argc, argv);
	init_simple_mines_factory ();

	/*
	 * Start processing.
	 */
	bonobo_main ();

	return 0;
}
